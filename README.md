# Gaia-X Open-Source Software Community

1. Group information

| **Group Name**                 | Gaia-X Open-Source Software (OSS) Community                  |
| ------------------------------ | ------------------------------------------------------------ |
| **Brief Description of Group** | The Gaia-X Open-Source Software (OSS) Community aims to engage and grow the Gaia-X developer community through outreach, onboarding, software development and the organization of the Gaia-X hackathon event. |

2. Group roles and participants

| Role                  | Name                                  | Organization                                              |
| --------------------- | ------------------------------------- | --------------------------------------------------------- |
| Lead                  | Kai Meinke                            | deltaDAO AG                                               |
| Gaia-X Representative | Pierre Gronlier                       | Gaia-X AISBL                                              |

3. Getting started

Join the Gaia-X OSS community meetings and the Gaia-X OSS community mailing list and Slack channel. We'll take you from there.

| Tooling                              |                                                                                                                                        |
| ------------------------------------ | -------------------------------------------------------------------------------------------------------------------------------------- |
| Mailing List                         | oss-community@list.gaia-x.eu , Sign up here: https://list.gaia-x.eu/postorius/lists/                                                   |
| Slack Channel                        | [Slack Channel](https://join.slack.com/t/gaia-xworkspace/shared_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o_sA)                            |
| Meeting Link (Thursdays at 9:00 CET) | [Teams Link](https://teams.microsoft.com/l/meetup-join/19%3ameeting_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)                                                  |
| Matrix Hackathon Chat:               | https://matrix.to/#/#gaia-x-hackathon:matrix.org                                                                                       |
| Matrix Group Chat:                   | https://matrix.to/#/#gaiax-community:matrix.org                                                                                        |
| Place of Documentation (GitLab)      | https://gitlab.com/gaia-x/gaia-x-community                                                                                             |

## Contributing

We are open for contributions from all Gaia-X Open-Source Software Community members and those who want to onboard to the community and contribute to the Gaia-X codebase. 

## Project status
active
