
# Gaia-X OSS Community Call

Next call: Thursday, January 25, 2024, 09:00 - 09:45 CET, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)



## Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Updates from the lab, lighthouses, projects and developers


## Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


## Meeting notes

### General meeting notes

#### Acceptance of agenda

yes



#### Number of participants

41



#### Acceptance of last meeting notes

yes

[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/1ca55e7569a158a8a5dbd91302fd986637153374/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-12-21.md%20](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/1ca55e7569a158a8a5dbd91302fd986637153374/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-12-21.md%20)



### Introduction of new participants

Participant name, institution, position, project



### Interesting sessions, events, and news

recent:

   * Gaia-X EU Parliament Reception 2024-01-22
   * GXFS Tech Workshop #6, Frankfurt
       * [https://eclipse.dev/xfsc/train/train/](https://eclipse.dev/xfsc/train/train/)
       * Exercises: [https://gitlab.eclipse.org/eclipse/xfsc/workshop/gxfs-tech-workshop-6-train/exercises](https://gitlab.eclipse.org/eclipse/xfsc/workshop/gxfs-tech-workshop-6-train/exercises)
   * Gaia-X Brochure w. Roadmap available, [https://gaia-x.eu/wp-content/uploads/2024/01/Gaia-X-Brochure\_2024\_Online\_Spread.pdf](https://gaia-x.eu/wp-content/uploads/2024/01/Gaia-X-Brochure\_2024\_Online\_Spread.pdf)
   * IPCEI-CIS launch
       * 109 projects associated with it
       * 7.8B(!) EUR overall and indirect investment volume
   * SIMPL launch, led by Eviden
       * 41M EUR tender by EC, PoC until mid 2024, MVP until end of 2024
       * Builds Open Source Dataspace Middleware on behalf of the EU Commission
           * Integrate existing Open Source initiatives -> OSS Dataspace Middleware Distribution
           * Goal is to make dataspaces SIMPLe to use
           * Map to DSSC building blocks [insert link here]
       * Consists of Simpl-Open, Simpl-Labs (2025, test and playground), Simpl-Live (procurement, health, language, ...) , starting with Simpl-Open
       * Link: [https://digital-strategy.ec.europa.eu/en/policies/simpl](https://digital-strategy.ec.europa.eu/en/policies/simpl)


next:

   * Event name: Cross Cloud Spaces Workshop 2024
       * Date: 26 February 2024,
       * Location  Barcelona
       * Event link: [https://web-eur.cvent.com/event/ba2eba73-ee72-4a1e-8ff8-cddb962fa05a/summary?locale=en](https://web-eur.cvent.com/event/ba2eba73-ee72-4a1e-8ff8-cddb962fa05a/summary?locale=en)
   * Data Spaces Symposium and Market-X
       * March 12-13, 2024, Frankfurt, Germany
       * [https://gaia-x.eu/market-x-2024-agenda/](https://gaia-x.eu/market-x-2024-agenda/)
       * There will be some fees, Gaia-X members will likely to attend for free, to be confirmed
   * Hannover Fair 2024
       * April 22-26, 2024, Hannover, Germany
       * [https://www.hannovermesse.de/de/](https://www.hannovermesse.de/de/)
   * Sovereign Cloud Stack Summit 2024
       * [https://scs.community/summit2024/](https://scs.community/summit2024/)
       * Tue, May 14, Berlin (mostly German language content, strategic)
       * Co-located with OpenInfra Days (technical) on May 15 (mostly English)




### Updates and questions from lab, lighthouses, projects, and developers

   * Discussion regarding [https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/issues/58](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/issues/58): How to enable domain specific attributes for meta data in our process chain with compliance service and federated catalogue.
       * @Lena Sauermann will prepare a powerpoint presentation to explain the problem and the current state of discussion.
       * Pierre highlighted that solution 2 is going to be implemented with the CES
       * CES will be opened for other valid credentials and perform a signature verification
       * There will be seperate endpoints for the different types of credentials \& filters on the APIs
       * Decentralization of the backend, i.e. with DOME, being checked to move away from the centralized version. Onboarding requirements being checked to be as open as possible.
       * Example for merging credential contents in JSONLD: [http://tinyurl.com/5f94wnun](http://tinyurl.com/5f94wnun)
   * JWS signature implementation in Gaia-X credentials, blocking XFSC catalogue interoperability
       * [https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/issues/53#note\_1738416999](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/issues/53#note\_1738416999)
       * Signer Implemenation showing the difference: [https://github.com/FabianScheidt/verifiable-credential-signer/blob/8fe810282ee250a58b07363f81821aa31ccee118/src/sign.ts#L95]([]https://github.com/FabianScheidt/verifiable-credential-signer/blob/8fe810282ee250a58b07363f81821aa31ccee118/src/sign.ts#L95[])
       * Will be taken care of in the next Loire release -> breaking change
       * Current signature process: [https://docs.gaia-x.eu/technical-committee/identity-credential-access-management/22.10/credential\_format/#proofs-signatures](https://docs.gaia-x.eu/technical-committee/identity-credential-access-management/22.10/credential\_format/#proofs-signatures)
   * Next major Pontus-X developer network upgrade in development for Q1
       * Includes confidential computation and private transactions, based on Intel SGX
       * Includes native EURO payments for transaction fees in the federation




### AOB

next meeting led by Gaia-X AISBL team, Kai is blocked next Thursday.



## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
