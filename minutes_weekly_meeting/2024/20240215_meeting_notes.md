
   * Gaia-X OSS Community Call - February 15th
Next call: Thursday, February 22, 2024, 09:00 - 09:45 CET, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024?ref\_type=heads](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024?ref\_type=heads)



## Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Updates from the lab, lighthouses, projects and developers


## Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


## Meeting notes

### General meeting notes

#### Acceptance of agenda

yes



#### Number of participants

55



#### Acceptance of last meeting notes

yes

[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240208\_meeting\_notes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240208\_meeting\_notes.md)



### Introduction of new participants

   * participant, affiliation, project


### Interesting sessions, events, and news

recent: 

   * New guidelines for IPCEI-CIS released  (German), Proposal Deadline is March 24
       * [https://www.bmwk.de/Redaktion/DE/Downloads/F/foerderrichtlinie-cloud-und-edge-infrastruktur-und-services-ipcei-cis.pdf?\_\_blob=publicationFile\&v=2](https://www.bmwk.de/Redaktion/DE/Downloads/F/foerderrichtlinie-cloud-und-edge-infrastruktur-und-services-ipcei-cis.pdf?\_\_blob=publicationFile\&v=2)
       * The main topics of the second funding round are: 
       * ✔ Mobility 
       * ✔ Energy efficiency 
       * ✔ Industry 4.0 
       * ✔ Resilience and resilience of digital infrastructure 
       * ✔ Health  
       * ✔ Robotics and automation


next:

   * Event name: Cross Cloud Spaces Workshop 2024
       * Date: 26 February 2024,
       * Location  Barcelona
       * Event link: [https://web-eur.cvent.com/event/ba2eba73-ee72-4a1e-8ff8-cddb962fa05a/summary?locale=en](https://web-eur.cvent.com/event/ba2eba73-ee72-4a1e-8ff8-cddb962fa05a/summary?locale=en)
   * Pontus-X Community Call
       * February 29, 13:30 CET - 14:30 CET, online event
       * Initiate voting for new federators. Introduction of a new validator and its impact on the ecosystem.
       * Monthly Update: Reviewing the progress of the Pontus-X Ecosystem, powered by Gaia-X.
       * Feature Presentation: Insight into the Pontus-X Network developments with Oasis Protocol.
       * Partner Showcase: Celebrating success stories, networking of data space initiatives and exploring new use cases.
       * Open Forum: A platform for community involvement and contributions.
       * Pontus-X Newsletter at [https://newsletter.pontus-x.eu/]([]https://newsletter.pontus-x.eu/[]) for regular information about our ecosystem, powered by Gaia-X.
   * Tech Deep Dive - Open to anyone
       * Registry decentralization by Arthur from the Lab
       * Thursday, February 29th at 15:00 CET
       * Invitation to follow
   * Data Spaces Symposium and Market-X
       * March 12-14, 2024, Darmstadt, Germany
       * [https://gaia-x.eu/market-x-2024/](https://gaia-x.eu/market-x-2024/)
       * [https://www.data-spaces-symposium.eu/program](https://www.data-spaces-symposium.eu/program)
       * There will be fees, Gaia-X members will be able to attend the Market-X day, March 12, for free
           * Tickets are now available, vouchers on the members platform
           * Two other days 110 euros / day
       * Tech stage on 12th March Morning, agenda published (Gaia-X 101, IPFS, Policy reasoning \& trusted computing)
   * Hannover Fair 2024
       * April 22-26, 2024, Hannover, Germany
       * [https://www.hannovermesse.de/de/](https://www.hannovermesse.de/de/)
   * Sovereign Cloud Stack Summit 2024
       * [https://scs.community/summit2024/](https://scs.community/summit2024/)
       * Tue, May 14, Berlin (mostly German language content, strategic)
       * Co-located with OpenInfra Days (technical) on May 15 (mostly English)
   * Gaia-X Tech-X
       * May 16-17 tbc, Gaia-X Hub Luxembourg
   * Cloud Expo Frankfurt 
       * May 23-24, Frankfurt, Germany
       * Gaia-X will be there with the German Gaia-X Hub
       * [https://www.cloudexpoeurope.de/](https://www.cloudexpoeurope.de/)




### Updates and questions from lab, lighthouses, projects, and developers

   * Moving Gaia-X Technical Specifications and new working group at the Eclipse Foundation
       * 3 fast tracks
       * Eclipse Trust Protocol Specification Group
   * Presentation in the Gaia-X Members Platform


### AOB

-



## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)

















