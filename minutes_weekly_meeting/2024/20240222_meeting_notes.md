
   * Gaia-X OSS Community Call - February 22
Next call: Thursday, February 29, 2024, 09:00 - 09:45 CET, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024?ref\_type=heads](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024?ref\_type=heads)



## Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Updates from the lab, lighthouses, projects and developers


## Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


## Meeting notes

### General meeting notes

#### Acceptance of agenda

yes/no



#### Number of participants

58



#### Acceptance of last meeting notes

yes

[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240215\_meeting\_notes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240215\_meeting\_notes.md)



### Introduction of new participants

   * participant, affiliation, project


### Interesting sessions, events, and news

previous events:

   * New Gaia-X Endorsement Programme launched
       * [https://gaia-x.eu/who-we-are/endorsement-programme/](https://gaia-x.eu/who-we-are/endorsement-programme/)
       * Features four categories
           * Endorsement Letter
           * Qualified Project
           * Lighthouse Project
           * Lighthouse Data Space
       * Strategic response to the evolving landscape of data spaces initiatives, providing a more inclusive framework for projects with different levels of maturity to actively contribute to and benefit from the Gaia-X community.
       * Each level is designed to reflect Gaia-X adoption and compliance level, as well as the project maturity in accordance with the development stages defined by the Data Spaces Support Centre in the Data Spaces Glossary. 
       * Gaia-X endorsed projects are closely integrated into the Gaia-X requirements process, participating in the elicitation, evaluation and prioritisation of requirements (functional and non-functional, business, technical, legal, etc.) and therefore take a vital part in shaping the Gaia-X development roadmap.


next:

   * Event name: Cross Cloud Spaces Workshop 2024
       * Date: 26 February 2024,
       * Location  Barcelona
       * Event link: [https://eur.cvent.me/RyDbl](https://eur.cvent.me/RyDbl)
       * Talks from Lauresha Memeti (GXFS/XFSC), Pierre Gronlier (Gaia-X), and Alexander Diab (SCS)
       * Still a few seats, but also possibility to register for online attendance
   * Pontus-X Community Call
       * February 29, 13:30 CET - 14:30 CET, online event
       * Initiate voting for new federators. Introduction of a new validator and its impact on the ecosystem.
       * Monthly Update: Reviewing the progress of the Pontus-X Ecosystem, powered by Gaia-X.
       * Feature Presentation: Insight into the Pontus-X Network developments with Oasis Protocol.
       * Partner Showcase: Celebrating success stories, networking of data space initiatives and exploring new use cases.
       * Open Forum: A platform for community involvement and contributions.
       * Pontus-X Newsletter at [https://newsletter.pontus-x.eu/]([]https://newsletter.pontus-x.eu/[]) for regular information about our ecosystem, powered by Gaia-X.
   * Tech Deep Dive - Open to anyone
       * Registry decentralization by Arthur from the Lab
       * Thursday, February 29th at 15:00 CET
       * [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_ZjgzZGYyZDAtNDUxMy00YjM5LWE4Y2UtNjEyNWE1YjkxMzM5%40thread.v2/0?context=%7b%22Tid%22%3a%225845e7c0-703b-46a4-88c5-02cbecfbb49f%22%2c%22Oid%22%3a%22ce231bc1-4696-4c00-b98f-9b19b7d27e2c%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_ZjgzZGYyZDAtNDUxMy00YjM5LWE4Y2UtNjEyNWE1YjkxMzM5%40thread.v2/0?context=%7b%22Tid%22%3a%225845e7c0-703b-46a4-88c5-02cbecfbb49f%22%2c%22Oid%22%3a%22ce231bc1-4696-4c00-b98f-9b19b7d27e2c%22%7d)
   * Data Spaces Symposium and Market-X
       * March 12-14, 2024, Darmstadt, Germany
       * [https://gaia-x.eu/market-x-2024/](https://gaia-x.eu/market-x-2024/)
       * [https://www.data-spaces-symposium.eu/program](https://www.data-spaces-symposium.eu/program)
       * There will be fees, Gaia-X members will be able to attend the Market-X day, March 12, for free
           * Tickets are now available, vouchers on the members platform
           * Two other days 110 euros / day
       * Tech stage on 12th March Morning, agenda published (Gaia-X 101, IPFS, Policy reasoning \& trusted computing)
   * Hannover Fair 2024
       * April 22-26, 2024, Hannover, Germany
       * [https://www.hannovermesse.de/de/](https://www.hannovermesse.de/de/)
   * Sovereign Cloud Stack Summit 2024
       * [https://scs.community/summit2024/](https://scs.community/summit2024/)
       * Tue, May 14, Berlin (mostly German language content, strategic)
       * Co-located with OpenInfra Days (technical) on May 15 (mostly English)
   * Cloud Expo Frankfurt 
       * May 22-23, Frankfurt, Germany
       * Gaia-X will be there with the German Gaia-X Hub
       * [https://www.cloudexpoeurope.de/](https://www.cloudexpoeurope.de/)
   * Gaia-X Tech-X
       * May 23-24, European Convention Center Luxembourg, hosted by Gaia-X Hub Luxembourg
       * CFP opens Monday, 4 March, presentations \& hacks
   * XFSC Tech Workshop #7 
       * Location : Hürth, Germany (close to Cologne)
       * Date: 12 June 2024
       * Registration: [https://www.gxfs.eu/xfsc-tech-workshop-7/](https://www.gxfs.eu/xfsc-tech-workshop-7/) 
       * Agenda coming – showcase the latest advancements in XFSC Specification Phase 2  ([https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads)](https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads)) 


### Updates from the Community, Developers, Lighthouses and Lab

   * Pontus-X launches new developer network on the roadmap towards production in 2024
       * Introduction by Albert Peci
       * Dedicated ParaTime Running on Oasis Network
       * Fully compatible with the current GEN-X Network
       * Nodes running in a TEE
       * Full support for Parallel Runtimes so different networks can be launched in parallel with different Runtimes (EVM, confidential EVM, WASM)
       * [https://docs.oasis.io/node/run-your-node/prerequisites/set-up-trusted-execution-environment-tee/](https://docs.oasis.io/node/run-your-node/prerequisites/set-up-trusted-execution-environment-tee/)
   * Flex4Res manufacturing and industry launched, using GXDCH and the Pontus-X ecosystem for cross-domain interoperability
       * [https://www.linkedin.com/posts/flex4res-project\_flex4res-gaiax-activity-7166331843020070912-mF0x?utm\_source=share\&utm\_medium=member\_desktop](https://www.linkedin.com/posts/flex4res-project\_flex4res-gaiax-activity-7166331843020070912-mF0x?utm\_source=share\&utm\_medium=member\_desktop)
       * [https://flex4res.pontus-x.eu/](https://flex4res.pontus-x.eu/)
   * Questions:
       * Integrations of JsonWebSignature2020 seems to be merged to develop ([https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/merge\_requests/218)](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/merge\_requests/218)). Is that already deployed to [https://compliance.lab.gaia-x.eu/development](https://compliance.lab.gaia-x.eu/development) or [https://compliance.lab.gaia-x.eu/main?](https://compliance.lab.gaia-x.eu/main?)
       * Issue [https://gitlab.com/gaia-x/lab/compliance/gx-registry/-/issues/36](https://gitlab.com/gaia-x/lab/compliance/gx-registry/-/issues/36): We would appreciate some feedback if renaming of the Shapes defined in the registry ([https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#)is](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#)is) considered. Example: rename of gx:DataResource to gx:DataResourceShape (like it is already in place for ParticipantShape and others).
           * Response from the lab: probably not on Tagus. Quite a large change on what's supposed to be a maintenance branch, could have impacts, hard to test. For Loire, shapes/schemas/ontology comes from the SvcCh WG, and generated via LinkML. Need to check whether that could happen too using this tool.
AOB

-



## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
