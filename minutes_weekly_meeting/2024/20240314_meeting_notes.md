
   * Gaia-X OSS Community Call - March 14
Next call: Thursday, March 21, 2024, 09:00 - 09:45 CET, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240307\_meeting\_notes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240307\_meeting\_notes.md)





Moderation: Lauresha Memeti 

Minutes of meeting : Vincent Kelleher  



## Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Updates from the lab, lighthouses, projects and developers


## Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


## Meeting notes

### General meeting notes

#### Acceptance of agenda

yes



#### Number of participants

43



#### Acceptance of last meeting notes

yes

[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240307\_meeting\_notes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240307\_meeting\_notes.md)



### Introduction of new participants

   * participant, affiliation, project
       * Pascal Guérin, Cloud Technology \& Architecture Consultant at NTT Data
       * Joshua Cornejo, working on a DCAT \& ODRL platform for Digital Rights Management
       * Georgios Triantafyllidis, PhD candidate at NTNU working on a project for Circular Cities


### Interesting sessions, events, and news

next:



   * Cross Cloud Spaces Paris Workshop 2024
       * March 19, 2024, Paris, France
       * [https://www.eclipse-foundation.events/event/crosscloudparis2024/summary](https://www.eclipse-foundation.events/event/crosscloudparis2024/summary)
       * Free in person event to discuss open source projects in the domain - limited to 50 seats
       * The location is 5mn walking distance from KubeCon Europe
   * Hannover Fair 2024
       * April 22-26, 2024, Hannover, Germany
       * [https://www.hannovermesse.de/de/](https://www.hannovermesse.de/de/)
   * Sovereign Cloud Stack Summit 2024
       * [https://scs.community/summit2024/](https://scs.community/summit2024/)
       * Tue, May 14, Berlin (mostly German language content, strategic)
       * Co-located with OpenInfra Days (technical) on May 15 (mostly English)
   * Cloud Expo Frankfurt 
       * May 22-23, Frankfurt, Germany
       * Gaia-X will be there with the German Gaia-X Hub
       * [https://www.cloudexpoeurope.de/](https://www.cloudexpoeurope.de/)
   * Gaia-X Tech-X and Gaia-X Hackathon No. 7
       * May 23-24, European Convention Center Luxembourg, hosted by Gaia-X Hub Luxembourg
       * CFP opens Monday, 4 March, presentations \& hacks
       * More informations about this on the Wiki page here 👉 [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/tech-x-2024-and-hackathon-7/-/wikis/home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/tech-x-2024-and-hackathon-7/-/wikis/home)
   * XFSC Tech Workshop #7 
       * Location : Hürth, Germany (close to Cologne)
       * Date: 12 June 2024
       * Registration: [https://www.gxfs.eu/xfsc-tech-workshop-7/](https://www.gxfs.eu/xfsc-tech-workshop-7/) 
       * Agenda coming – showcase the latest advancements in XFSC Specification Phase 2  ([https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads)](https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads)) 


### Updates from the Community, Developers, Lighthouses and Lab



   * Gaia-X Lab Team is now contributing to the LinkML project in the context of the Service Characteristics Working Group ontology to add new features and fixes.
       * [https://github.com/linkml/linkml/pull/1964](https://github.com/linkml/linkml/pull/1964)
       * [https://github.com/linkml/linkml/pull/1601](https://github.com/linkml/linkml/pull/1601)


## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)





