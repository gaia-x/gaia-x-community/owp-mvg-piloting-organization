
   *  Gaia-X OSS Community Call - March 21
Next call: Thursday, March 28, 2024, 09:00 - 09:45 CET, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240321\_meeting\_notes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240321\_meeting\_notes.md)





Moderation: Kai  Meinke

Minutes of meeting : collective 



## Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Verifiable Credential Signing Solution Proposal by Gaia-X CTO Team
   1. Updates from the lab, lighthouses, projects and developers
   1. Effects of the T-Systems Certificate expiration two weeks ago?


## Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


## Meeting notes

### General meeting notes

#### Acceptance of agenda

yes



#### Number of participants

72



#### Acceptance of last meeting notes

yes

[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240321\_meeting\_notes.md?ref\_type=heads](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240321\_meeting\_notes.md?ref\_type=heads)



### Introduction of new participants

   * participant, affiliation, project
       * Detlef Hühnlein, ecsec, developer of XFSC notarization API


### Interesting sessions, events, and news

- New Gaia-X Lighthouse Data Spaces have been announced, 

- *The key difference is that lighthouse data spaces are fully operational from a business standpoint, using an accredited Gaia-X clearing houses and implementing data space services on an industry scale..”*

- [https://gaia-x.eu/who-we-are/lighthouse-data-spaces/](https://gaia-x.eu/who-we-are/lighthouse-data-spaces/)



- Gaia-X officially recognised Aruba SpA and T-Systems International (+Aire networks on its way for signing the GXDCH Agreement) as Gaia-X ACCREDITED Digital Clearing Houses. No difference in status between the already known running clearing house nodes.

- [https://gaia-x.eu/news-press/market-x-conference-expo-wraps-up-successful-2nd-edition-showcasing-the-real-world-impact-of-gaia-x-up-and-running-projects/](https://gaia-x.eu/news-press/market-x-conference-expo-wraps-up-successful-2nd-edition-showcasing-the-real-world-impact-of-gaia-x-up-and-running-projects/)



next:

   * Tech deep dive
       * March 28th, hosted by Ewann from the Lab team
       * [https://gaia-x.eu/event/tech-deep-dive-2/](https://gaia-x.eu/event/tech-deep-dive-2/)
       * Trusted execution for the Gaia-X components
   * Hannover Fair 2024
       * April 22-26, 2024, Hannover, Germany
       * [https://www.hannovermesse.de/de/](https://www.hannovermesse.de/de/)
       * If you want to register for free tickets, in relation to the German Gaia-X Hub, [https://www.hannovermesse.de/de/applikation/registrierung/direkteinstieg-tickets-ausweise?code=rXzKm](https://www.hannovermesse.de/de/applikation/registrierung/direkteinstieg-tickets-ausweise?code=rXzKm)
   * Sovereign Cloud Stack Summit 2024
       * [https://scs.community/summit2024/](https://scs.community/summit2024/)
       * Tue, May 14, Berlin (mostly German language content, strategic)
       * Co-located with OpenInfra Days (technical) on May 15 (mostly English), CfP closing Mar 28: [https://oideurope2024.openinfra.dev/?\_ga=2.81567767.1664958746.1708893806-1894249939.1705129736#registration=1](https://oideurope2024.openinfra.dev/?\_ga=2.81567767.1664958746.1708893806-1894249939.1705129736#registration=1)
   * Cloud Expo Frankfurt 
       * May 22-23, Frankfurt, Germany
       * Gaia-X will be there with the German Gaia-X Hub
       * [https://www.cloudexpoeurope.de/](https://www.cloudexpoeurope.de/)
   * Gaia-X Tech-X and Gaia-X Hackathon No. 7
       * May 23-24, European Convention Center Luxembourg, hosted by Gaia-X Hub Luxembourg
       * CFP opens Thursday, 14 March, presentations \& hacks
           * Please submit proposal by Thursday, 18 April, info on the wiki page below.
       * More informations about this on the Wiki page here 👉 [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/tech-x-2024-and-hackathon-7/-/wikis/home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/tech-x-2024-and-hackathon-7/-/wikis/home)
   * XFSC Tech Workshop #7 
       * Location : Hürth, Germany (close to Cologne)
       * Date: 12 June 2024
       * Registration: [https://www.gxfs.eu/xfsc-tech-workshop-7/](https://www.gxfs.eu/xfsc-tech-workshop-7/) 
       * Agenda coming – showcase the latest advancements in XFSC Specification Phase 2  ([https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads)](https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads)) 


### Demonstration GXFS-FR ICP Issuer Credential Protocol

   * Yves Marie Pondaven / Jeremy Hirth-Daumas / Valerie Bruna
   * uses OpenID Connect for Verifiable Credential Issuance Draft 13, OIDC4VP Draft 20, PEX 2.0
   * Includes StatusList2021Entry (Bitstring StatusList soon)
   * Issuer supports RSA signing algorithms (PS256) used in GXDCH
   * Communicates with Back Office Gaia-X
   * Tested with Altme/Talao Wallet, in test with walt.id and Sphereon wallets with the current exchange and issuance protocol drafts (see above)
   * Entry point and links to code base: [https://elearning.gxfs.fr/](https://elearning.gxfs.fr/) , [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario)


### Verifiable Credential Signing Solution Proposal

   * Presented by Vincent Kelleher <vincent.kelleher@gaia-x.eu> on behalf of the Gaia-X CTO Team
   * The current Json Web Signature 2020 signature method is deprecated
   * Issues have started to appear with the use of the above
   * This presentation will explain the current signature method, expose available standards for signing verifiable credentials and propose the use of one of those standards (VC-JWT)


### Updates from the Community, Developers, Lighthouses and Lab



   * Gaia-X Lab Team released a library to verify DIDs : [https://www.npmjs.com/package/@gaia-x/did-verifier](https://www.npmjs.com/package/@gaia-x/did-verifier)


## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)





