# Gaia-X OSS Community Call - April 4

Next call: Thursday, April 11, 2024, 09:00 - 09:45 CEST, https://teams.microsoft.com/l/meetup-join/19%3ameeting_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d

Last meeting notes always to be found here: https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes_weekly_meeting/2024/20240328_meeting_notes?ref_type=heads

- Moderation: Ewann Gavard - Gaia-X AISBL
- Minutes of meeting : Ewann Gavard + collective 

## Agenda

* Competition and antitrust guidelines
* Acceptance of last meeting notes
* Introduction of new and existing participants
* Interesting sessions, events, and news
* Request to provide input to an overview of existing OSS components and repositories (not done)
* Demonstration of ODRL Policy usage by GXFS-FR
* Updates from the lab, lighthouses, projects and developers


## Competition and antitrust guidelines
- no price-fixing
- no market or customer allocation
- no output restrictions
- no agreement on or exchange of competitively sensitive business information
- consequences of competition law infringement


## Meeting notes
### General meeting notes
#### Acceptance of agenda: **yes**

#### Number of participants: **52**

#### Acceptance of last meeting notes: **yes**

https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes_weekly_meeting/2024/20240328_meeting_notes?ref_type=heads

#### Introduction of new participants:
- Antonio Sanchez Yébenes - Indesia (Spanish Gaia-X Hub)

#### Interesting sessions, events, and news

##### Hannover Fair 2024

April 22-26, 2024, Hannover, Germany

https://www.hannovermesse.de/de/

If you want to register for free tickets, in relation to the German Gaia-X Hub, https://www.hannovermesse.de/de/applikation/registrierung/direkteinstieg-tickets-ausweise?code=rXzKm

##### Sovereign Cloud Stack Summit 2024

https://scs.community/summit2024/

Tue, May 14, Berlin, Villa Elisabeth, (mostly German language content, strategic), expect 200+ participants

Co-located with OpenInfra Days (technical) on May 15 (mostly English), CfP closed, many proposals received: https://oideurope2024.openinfra.dev/?_ga=2.81567767.1664958746.1708893806-1894249939.1705129736#registration=1

##### Cloud Expo Frankfurt 

May 22-23, Frankfurt, Germany

Gaia-X will be there with the German Gaia-X Hub

https://www.cloudexpoeurope.de/

##### Gaia-X Tech-X and Gaia-X Hackathon No. 7

May 23-24, European Convention Center Luxembourg, hosted by Gaia-X Hub Luxembourg

CFP opens Thursday, 14 March, presentations & hacks

Please submit proposal by Thursday, 18 April, info on the wiki page below.

More informations about this on the Wiki page here 👉 https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/tech-x-2024-and-hackathon-7/-/wikis/home

##### XFSC Tech Workshop #7 

Location : Hürth, Germany (close to Cologne)

Date: 12 June 2024

Registration: https://www.gxfs.eu/xfsc-tech-workshop-7/ 

Agenda coming – showcase the latest advancements in XFSC Specification Phase 2  (https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref_type=heads) 

#### Demonstration of ODRL Policy usage by GXFS-FR 
Slides https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes_weekly_meeting/2024/presentations/2024-03-28_-_OSS_Authorization_Flow_in_Aster-X.pdf?ref_type=heads

Presentation by Guillaume HEBERT and Hoan HOANG

Consumption of a DataProduct with rights constraints expressed in ODRL

ODRL rules on DataProduct and DataContract negotiated between a DataProvider and a DataConsumer can be translate to a Amazon S3 Policy to be understandable by "traditional Authorization" system.


### Updates from the Community, Developers, Lighthouses and Lab

Questions regarding developments on blockchain and smart contracts

=> LAB: Not a subject for the lab right now, but feedbacks and comments are more than welcome in the working groups to enlighten their decision-making


## Useful resources for newcomers

- Introduction to community: https://gitlab.com/gaia-x/gaia-x-community/open-source-community

- Gaia-X OSS community mailing list: https://list.gaia-x.eu/postorius/lists/

- Gaia-X OSS community repository: https://gitlab.com/gaia-x/gaia-x-community/open-source-community

- Gaia-X Slack: https://join.slack.com/t/gaia-xworkspace/shared_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o_sA

- Gaia-X OSS community Matrix Chat: https://matrix.to/#/#gaiax-community:matrix.org

- Gaia-X Framework: https://docs.gaia-x.eu/framework/

- Gaia-X Digital Clearing Houses: http://docs.gaia-x.eu/framework/?tab=clearing-house

- Gaia-X Publications: https://gaia-x.eu/mediatech/publications/

- Gaia-X Wizard: https://wizard.lab.gaia-x.eu/

- Gaia-X Members Platform: https://membersplatform.gaia-x.eu/

- C4 Software architecture: https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref_type=heads




