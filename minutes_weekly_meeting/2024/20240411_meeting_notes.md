# Gaia-X OSS Community Call - April 11

Next call: Thursday, April 18, 2024, 09:00 - 09:45 CEST, https://teams.microsoft.com/l/meetup-join/19%3ameeting_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d
Last meeting notes always to be found here: https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes_weekly_meeting/2024/20240404_meeting_notes.md?ref_type=heads

Moderation: Ewann
Minutes of meeting : collective 

## Agenda

- Competition and antitrust guidelines
- Acceptance of last meeting notes
- Introduction of new and existing participants
- Interesting sessions, events, and news
- Updates from the lab, lighthouses, projects and developers


### Competition and antitrust guidelines
- no price-fixing
- no market or customer allocation
- no output restrictions
- no agreement on or exchange of competitively sensitive business information
- consequences of competition law infringement


### Meeting notes
General meeting notes

####  Acceptance of agenda **yes**

####  Number of participants : **34**

####  Acceptance of last meeting notes: **yes**

https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes_weekly_meeting/2024/20240404_meeting_notes.md?ref_type=heads

#### Introduction of new participants

- Aleksandar Kelecevic - T-Systems


####  Interesting sessions, events, and news
- Hannover Fair 2024

April 22-26, 2024, Hannover, Germany https://www.hannovermesse.de/de/

If you want to register for free tickets, in relation to the German Gaia-X Hub, https://www.hannovermesse.de/de/applikation/registrierung/direkteinstieg-tickets-ausweise?code=rXzKm

- Sovereign Cloud Stack Summit 2024

https://scs.community/summit2024/

Tue, May 14, Berlin, Villa Elisabeth, (mostly German language content, strategic), expect 200+ participants

Co-located with OpenInfra Days (technical) on May 15 (mostly English), Good program, will be published later this week. Still sponsorships slots for the OpenInfraDays available. (Contact mailto:garloff@osb-alliance.com).

- Cloud Expo Frankfurt 

May 22-23, Frankfurt, Germany

Gaia-X will be there with the German Gaia-X Hub

https://www.cloudexpoeurope.de/

- Gaia-X Tech-X and Gaia-X Hackathon No. 7

May 23-24, European Convention Center Luxembourg, hosted by Gaia-X Hub Luxembourg

CFP opens Thursday, 14 March, presentations & hacks

Please submit proposal by Thursday, 18 April, info on the wiki page below.

More informations about this on the Wiki page here 👉 https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/tech-x-2024-and-hackathon-7/-/wikis/home or on the website https://gaia-x.eu/tech-x-presentations-2024/ & https://gaia-x.eu/tech-x-hackathon/

Registration open: https://gaia-x-tech.site.digitevent.com/page/informations-en/

- XFSC Tech Workshop #7 

Location : Hürth, Germany (close to Cologne)

Date: 12 June 2024

Registration: https://www.gxfs.eu/xfsc-tech-workshop-7/ 

Agenda coming – showcase the latest advancements in XFSC Specification Phase 2  (https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref_type=heads) 


#### Updates from the Community, Developers, Lighthouses and Lab

- Notary: OpenCorporateAPI via schema:taxID queries, available on v2/main/development branches thanks to Aleksandar Kelecevic. Soon deployed on the Gaia-X Lab. Feature announcing it's presence (https://registrationnumber.notary.lab.gaia-x.eu/development/)

- VC-JWT proposal Gitlab issue is still open to comments and questions 👉 https://gitlab.com/gaia-x/lab/gxdch/-/issues/19

- Working on a more "centralized" technical documentation on the website with the comms team
    - Repositories explanation, link to GXDCH installation info...
- Question regarding remote teams for the Hackathon #7:
    - Need to ask the Gaia-X management team, will provide the anwser publicly


#### Useful resources for newcomers

Introduction to community: https://gitlab.com/gaia-x/gaia-x-community/open-source-community

Gaia-X OSS community mailing list: https://list.gaia-x.eu/postorius/lists/

Gaia-X OSS community repository: https://gitlab.com/gaia-x/gaia-x-community/open-source-community

Gaia-X Slack: https://join.slack.com/t/gaia-xworkspace/shared_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o_sA

Gaia-X OSS community Matrix Chat: https://matrix.to/#/#gaiax-community:matrix.org

Gaia-X Framework: https://docs.gaia-x.eu/framework/

Gaia-X Digital Clearing Houses: http://docs.gaia-x.eu/framework/?tab=clearing-house

Gaia-X Publications: https://gaia-x.eu/mediatech/publications/

Gaia-X Wizard: https://wizard.lab.gaia-x.eu/

Gaia-X Members Platform: https://membersplatform.gaia-x.eu/

Software architecture: https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref_type=heads




