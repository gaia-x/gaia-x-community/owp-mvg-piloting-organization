
# Gaia-X OSS Community Call - April 18

Next call: Thursday, April 11th, 2024, 09:00 - 09:45 CEST, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240411\_meeting\_notes.md?ref\_type=heads](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240411\_meeting\_notes.md?ref\_type=heads)



Moderation: Vincent Kelleher

Minutes of meeting : collective 



## Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Updates from the lab, lighthouses, projects and developers


## Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


## Meeting notes

### General meeting notes

#### Acceptance of agenda

[yes/~~no~~]



#### Number of participants

44



#### Acceptance of last meeting notes

[yes/~~no~~]

[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240411\_meeting\_notes.md?ref\_type=heads](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240411\_meeting\_notes.md?ref\_type=heads)



### Introduction of new participants

   * Name, position, company


### Interesting sessions, events, and news

   * ✨ Gaia-X Academy is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
next:

   * Hannover Fair 2024
       * April 22-26, 2024, Hannover, Germany
       * [https://www.hannovermesse.de/de/](https://www.hannovermesse.de/de/)
       * If you want to register for free tickets, in relation to the German Gaia-X Hub, [https://www.hannovermesse.de/de/applikation/registrierung/direkteinstieg-tickets-ausweise?code=rXzKm](https://www.hannovermesse.de/de/applikation/registrierung/direkteinstieg-tickets-ausweise?code=rXzKm)
   * Sovereign Cloud Stack Summit 2024
       * [https://scs.community/summit2024/](https://scs.community/summit2024/)
       * Tue, May 14, Berlin, Villa Elisabeth, (mostly German language content, strategic), expect 200+ participants
       * Co-located with OpenInfra Days (technical) on May 15 (mostly English), Good program, will be published later this week. Still sponsorships slots for the OpenInfraDays available. (Contact [mailto:garloff@osb-alliance.com)](mailto:garloff@osb-alliance.com)).
   * Cloud Expo Frankfurt 
       * May 22-23, Frankfurt, Germany
       * Gaia-X will be there with the German Gaia-X Hub
       * [https://www.cloudexpoeurope.de/](https://www.cloudexpoeurope.de/)
   * Gaia-X Tech-X and Gaia-X Hackathon No. 7
       * May 23-24, European Convention Center Luxembourg, hosted by Gaia-X Hub Luxembourg
       * CFP opens Thursday, 14 March, presentations \& hacks
           * Please submit proposal by **Thursday, 18 April**, info on the wiki page below.
       * More informations about this on the Wiki page here 👉 [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/tech-x-2024-and-hackathon-7/-/wikis/home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/tech-x-2024-and-hackathon-7/-/wikis/home) or on the website [https://gaia-x.eu/tech-x-presentations-2024/](https://gaia-x.eu/tech-x-presentations-2024/) \& [https://gaia-x.eu/tech-x-hackathon/](https://gaia-x.eu/tech-x-hackathon/)
       * Registration open: [https://gaia-x-tech.site.digitevent.com/page/informations-en/](https://gaia-x-tech.site.digitevent.com/page/informations-en/)
   * XFSC Tech Workshop #7 
       * Location : Hürth, Germany (close to Cologne)
       * Date: 12 June 2024
       * Registration: [https://www.gxfs.eu/xfsc-tech-workshop-7/](https://www.gxfs.eu/xfsc-tech-workshop-7/) 
       * Agenda coming – showcase the latest advancements in XFSC Specification Phase 2  ([https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads)](https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads)) 


### Updates from the Community, Developers, Lighthouses and Lab

   * VC-JWT proposal Gitlab issue is still open to comments and questions 👉 [https://gitlab.com/gaia-x/lab/gxdch/-/issues/19](https://gitlab.com/gaia-x/lab/gxdch/-/issues/19)
   * The Service Characteristic Working Group ontology ([https://gitlab.com/gaia-x/technical-committee/service-characteristics)](https://gitlab.com/gaia-x/technical-committee/service-characteristics)) has been generated and broadcasted through IPFS for the first time as JSON-LD schemas and Shacl shapes 🎉


## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)





