
# Gaia-X OSS Community Call - May 16

Next call: Thursday, May, 23th 2024, 09:00 - 09:45 CEST, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 



Moderation: Kai Meinke, deltaDAO

Minutes of meeting : collective 



### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

yes

[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024)



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Updates from the lab, lighthouses, projects and developers


Acceptance of agenda

yes



Number of participants

40



### Introduction of new participants

   * Name, position, company


### Interesting sessions, events, and news

   * ✨ **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * **Cloud Expo Frankfurt**
       * **May 22-23**, Frankfurt, Germany
       * Gaia-X will be there with the German Gaia-X Hub
       * [https://www.cloudexpoeurope.de/](https://www.cloudexpoeurope.de/)
   * **Gaia-X Tech-X and Gaia-X Hackathon No. 7**
       * **May 23-24**, European Convention Center Luxembourg, hosted by Gaia-X Hub Luxembourg
       * **Registrations open:** [https://gaia-x-tech.site.digitevent.com/page/informations-en/](https://gaia-x-tech.site.digitevent.com/page/informations-en/)
       * **Agenda**: [https://gaia-x.eu/tech-x-agenda-2024/](https://gaia-x.eu/tech-x-agenda-2024/)
       * **Hackathon tracks**: [https://gaia-x.eu/tech-x-hackathon-2024/](https://gaia-x.eu/tech-x-hackathon-2024/)
   * **Pontus-X Ecosystem Call**
       * May 30, online, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_MzQ5M2IzNmQtMTgxYi00MjIzLWFiMDYtYTg5OWQyNDZiOGY5%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%2282638514-a1f4-4d88-be55-23e168a746da%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_MzQ5M2IzNmQtMTgxYi00MjIzLWFiMDYtYTg5OWQyNDZiOGY5%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%2282638514-a1f4-4d88-be55-23e168a746da%22%7d)
       * Agenda: Ecosystem Updates, Project Introductions, Moving towards Production
   * **XFSC Tech Workshop #7**
       * Location : Hürth, Germany (close to Cologne)
       * Date: 12 June 2024
       * Registration: [https://www.gxfs.eu/xfsc-tech-workshop-7/](https://www.gxfs.eu/xfsc-tech-workshop-7/)
       * Agenda coming – showcase the latest advancements in XFSC Specification Phase 2 ([https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads))](https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads)))
### 

### Updates from the Community, Developers, Lighthouses and Lab

   * VC-JWT as default format for the upcoming Loire release 👉 [https://gitlab.com/gaia-x/lab/gxdch/-/issues/19](https://gitlab.com/gaia-x/lab/gxdch/-/issues/19) \& [https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/merge\_requests/39](https://gitlab.com/gaia-x/technical-committee/federation-services/icam/-/merge\_requests/39)
   * CI and Exchange drafts, draft 13 on the road to become a stable draft. draft 12-13 in final selection. VP draft 20 currently in the focus.
   * Small feat on Compliance V1: Cert validity check \& prevent issuing creds valid after certificate end date


## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)

