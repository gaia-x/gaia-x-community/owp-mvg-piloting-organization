
# Gaia-X OSS Community Call - May 30

Next call: Thursday, June 6, 2024, 09:00 - 09:45 CEST, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 



Moderation: Kai Meinke, deltaDAO.

Minutes of meeting : collective 



### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

yes/no

[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024)



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Updates from the lab, lighthouses, projects and developers


Acceptance of agenda

yes



Number of participants

41



### Introduction of new participants

   * Name, position, company


### Interesting sessions, events, and news

   * ✨ **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * **Pierre Gronlier new Chief Innovation Officer of Gaia-X AISBL: **[https://gaia-x.eu/news-press/pierre-gronlier-appointed-as-new-gaia-xs-new-chief-innovation-officer/](**https://gaia-x.eu/news-press/pierre-gronlier-appointed-as-new-gaia-xs-new-chief-innovation-officer/**)
       * CTO job offering [https://gaia-x.eu/job-openings/chief-technology-officer/](https://gaia-x.eu/job-openings/chief-technology-officer/)
   * **Sprin-D Funke "Spark" EUDI Wallet Prototypes Stage 1** Qualifications:
       * Funding of 300k EUR, [https://www.sprind.org/de/challenges/eudi-wallet-prototypes/](https://www.sprind.org/de/challenges/eudi-wallet-prototypes/)
           * Sphereon Wallet for All
           * Heidi: Humanzentrierte EID Infrastruktur
           * Animo Easy PID
           * Governikus eID client Wallet-Evolution
           * TICE Wallet
           * AUTHADA eEWA – easy EUDI-Wallet App
   * **XFSC Tech Workshop #7**
       * Location : Hürth, Germany (close to Cologne)
       * Date: 12 June 2024
       * Registration: [https://www.gxfs.eu/xfsc-tech-workshop-7/](https://www.gxfs.eu/xfsc-tech-workshop-7/)
       * Agenda coming – showcase the latest advancements in XFSC Specification Phase 2 ([https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads))](https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads)))
### 

### Updates from the Community, Developers, Lighthouses and Lab

   * Pontus-X ([https://docs.pontus-x.eu/](https://docs.pontus-x.eu/), [https://www.pontus-x.eu/)](https://www.pontus-x.eu/)) announced roadmap to production
   * New privacy-enabled Pontus-X developer (DEV) and test networks (STAGE) online, based on Oasis Sapphire (, to meet requirements on privacy and domain specific customization
   * All network nodes running in TEEs ([https://docs.oasis.io/node/run-your-node/prerequisites/set-up-trusted-execution-environment-tee)](https://docs.oasis.io/node/run-your-node/prerequisites/set-up-trusted-execution-environment-tee)) with remote code attestation, support for off-chain compution and Compute-to-Data
   * EUROe ([https://www.euroe.com/)](https://www.euroe.com/)) and Ocean Enterprise ([https://www.oceanenterprise.io/)](https://www.oceanenterprise.io/)) supported
   * Roadmap details
       * Migration to Oasis Sapphire in Q2/24
       * Growth Phase in Q3/24
       * MVP Launch in Q4/24
       * Further Gaia-X Framework and GXDCH support - "powered by Gaia-X"
       * SSI integration with GXDCH ([http://docs.gaia-x.eu/framework/?tab=clearing-house)](http://docs.gaia-x.eu/framework/?tab=clearing-house))
       * Migration of use cases and re-config of existing infrastructure in May/June with Nautilus [https://nautilus.delta-dao.com/](https://nautilus.delta-dao.com/)
   * An Architecture Decision Record has been raised about changes to the Gaia-X Notary 👉 [https://gitlab.com/gaia-x/lab/gxdch/-/merge\_requests/9](https://gitlab.com/gaia-x/lab/gxdch/-/merge\_requests/9)
   * New Gaia-X membership portal: you can get your membership Verifiable Credential as a JSON-LD and using OIDC4VCI (using draft 11): 
       * [https://wizard.lab.gaia-x.eu/development/membershipCredential](https://wizard.lab.gaia-x.eu/development/membershipCredential)
       * Plans to prototype with: [https://github.com/GAIA-X4PLC-AAD/ssi-to-oidc-bridge](https://github.com/GAIA-X4PLC-AAD/ssi-to-oidc-bridge)
   * Tech-X presentation recordings are uploaded on the Gaia-X Youtube: [https://www.youtube.com/@gaia-x5188](https://www.youtube.com/@gaia-x5188)
       * Slides also available on the website: [https://gaia-x.eu/tech-x-agenda-2024/](https://gaia-x.eu/tech-x-agenda-2024/)
       * Report on the hackathon being prepared
   * Update of the Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/) (with publication of the Architecture document 24.04 and PRLD 24.04-prerelease)
   * Notebook Examples for the conformity criteria: [https://github.com/ticapix/gaia-x-notebooks/tree/main](https://github.com/ticapix/gaia-x-notebooks/tree/main)


Next week:

   * Update from the SSI developers and from the Gaia-X Hackathon No. 7


## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)



