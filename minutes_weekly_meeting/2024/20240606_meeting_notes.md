
# Gaia-X OSS Community Call - June 6

Next call: Thursday, June 6, 2024, 09:00 - 09:45 CEST, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 



Moderation: Kai Meinke, deltaDAO.

Minutes of meeting : collective 



### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

yes

[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024)



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. New initiative introduced by Pierre Gronlier
   1. Updates from the lab, lighthouses, projects and developers


Acceptance of agenda

yes



Number of participants

46



### Introduction of new participants

   * Name, position, company


### Interesting sessions, events, and news

   * ✨ **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * **Gaia-X Extraordinary General Assembly (EGA)** on June 24, 2024, 13:00 - 15:00 CET
       * If you are a Gaia-X Member take care to prepare for the event and attend the EGA.
   * **XFSC Tech Workshop #7**
       * Location : Hürth, Germany (close to Cologne)
       * Date: 12 June 2024
       * Registration: [https://www.gxfs.eu/xfsc-tech-workshop-7/](https://www.gxfs.eu/xfsc-tech-workshop-7/)
       * Agenda coming – showcase the latest advancements in XFSC Specification Phase 2 ([https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads))](https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads)))
   * **Gaia-X Summit 24**
       * Location: Helsinki, Finland
       * Date: 14, 15 November 2024
       * [https://gaia-x.eu/summit-2024/](https://gaia-x.eu/summit-2024/)
### 

### Updates from the Community, Developers, Lighthouses and Lab

   * eIDAS Testbed to be found here: [https://www.eid.as/](https://www.eid.as/)
   * New Gaia-X membership portal: you can get your membership Verifiable Credential as a JSON-LD and using OIDC4VCI (using draft 11): [https://wizard.lab.gaia-x.eu/development/membershipCredential](https://wizard.lab.gaia-x.eu/development/membershipCredential)
   * Update of the Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/) (with publication of the Architecture document 24.04 and PRLD 24.04-prerelease)
   * Notebook Examples for the conformity criteria: [https://github.com/ticapix/gaia-x-notebooks/tree/main](https://github.com/ticapix/gaia-x-notebooks/tree/main)


**Room for an Update from the SSI developers and from the Gaia-X Hackathon No. 7**

Hackathon #7 report [https://gaia-x.eu/wp-content/uploads/2024/05/Hackathon\_Report.pdf](https://gaia-x.eu/wp-content/uploads/2024/05/Hackathon\_Report.pdf)



Introduction of a new initiative by Pierre.



   * Early feedback and expectations are most welcome [https://gitlab.com/gaia-x/lab/lsd-connector](https://gitlab.com/gaia-x/lab/lsd-connector)
   * Idea is to start a new Gaia-X Light \& Simple connector, based on the IDS Data Space Protocol
   * Development has not started yet
   * Including the Gaia-X Trust Framework Principles and Credentials
   * Readme can be shared by Pierre: 
   * Reduction of Dependencies to easy the deployment
   * The goal is not to develop from scratch, but start from the existing libraries.
   * What does the community think about it? Dataspace Lab France and Health Space in Luxemburg.
       * Painpoints of the EDC are touched by this.
       * Why not contribute something simpler to EDC? Has not possible as extension, it requires work at the core.
       * Where is the problem exactly? What is the missing bit that leads to the need of a core change? Lack of ODRL resolver.
       * What would be the cost of building something new? Is it more efficient than re-engineering? Since many lighthouses failed to scale EDC usage the benefit is quite relevant.
       * We did not see a IDS-based connector yet that supports the framework. 
       * We should have comparison of the available stacks and challenges. The community can engage in a comparison table, but this is not a Gaia-X AISBL activity. 
       * There is the assumption that all the components that validate policies do exist somewhere. Building an enforcer for ODRL that is fully compliant is extremely complex. Dropping complexity is very important.
           * One challenge is also implementation efficiency (which could be a sore point with graph databases)
       * There are some ODRL reasoning tools where developments can start, also part of the GIT repo.
       * One idea of the Data Space Working group was to create such a starter kit, maybe this can be merged together.
       * The project does not need to live in a Gaia-X repository
       * Some prototyping has already started, a few missing parts have been identified.
       * deltaDAO and Pontus-X ([https://pontus-x.eu)](https://pontus-x.eu)) are also interested in the results, also in the option for an EDC module to make it more interoperable with the Ocean Provider. ODRL reasoning is currently not supported, but these items have been placed on the Ocean Enterprise roadmap to enhance interoperability between the different technology stacks. 
       * Remark: Just consider the perspective of a data space participant and his/her expectiations on the EDC.. The example would be that there is a data service / compute service they want to offer which has its own interfaces etc.. the effort to map this into the EDC corset may be simply too expensive with regard to needed programming resources etc...  (in a way this addressed the idea of a very lightweight simple connector which I really appreciate).
       *  What would be a reasonable timeline for this? The hope would be to have something before summer 24.
       * Do you have some component diagrams for a few use cases, describing where and how this fits in?
       * One should consider it is not all about the technical layer, but often on the governance layer


## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)