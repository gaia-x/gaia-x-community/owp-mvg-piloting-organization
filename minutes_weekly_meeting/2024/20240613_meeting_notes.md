
# Gaia-X OSS Community Call - June 13

Next call: Thursday, June 20, 2024, 09:00 - 09:45 CEST, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 



Moderation: Vincent Kelleher, Gaia-X AISBL

Minutes of meeting : collective 



### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

yes

[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024)



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Updates from the lab, lighthouses, projects and developers


Acceptance of agenda

yes



Number of participants

50



### Introduction of new participants

   * Verena Parzer, Researcher at the Austrian Institute of Technology


### Interesting sessions, events, and news

   * ✨ **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * **Gaia-X Extraordinary General Assembly (EGA)** on June 24, 2024, 13:00 - 15:00 CET
       * If you are a Gaia-X Member take care to prepare for the event and attend the EGA.
   * **XFSC Tech Workshop #7**
       * Location : Hürth, Germany (close to Cologne), Date: 12 June 2024
       * Showcase the latest advancements in XFSC Specification Phase 2 ([https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads))](https://gitlab.eclipse.org/eclipse/xfsc/xfsc-spec-2/-/blob/main/Spec2Overview.md?ref\_type=heads)))
       * Repo of the TWSH7 : [https://gitlab.eclipse.org/eclipse/xfsc/workshop/xfsc-tech-workshop-7](https://gitlab.eclipse.org/eclipse/xfsc/workshop/xfsc-tech-workshop-7) 
   * **Gaia-X Summit 24**
       * Location: Helsinki, Finland
       * Date: 14, 15 November 2024
       * [https://gaia-x.eu/summit-2024/](https://gaia-x.eu/summit-2024/)
### 

### Updates from the Community, Developers, Lighthouses and Lab

   * Signing Algorithm
       * We have noticed that the gx-wizard ([https://wizard.lab.gaia-x.eu/)](https://wizard.lab.gaia-x.eu/)) now produces Verifiable Credentials that have a proof with a JWS-Header **alg=PS512**. In the past the JWS-Header was (at least in our cases) **alg=PS256**. 
       * Exampe:
       * **{"crit":["b64"],"b64":false,"alg":"PS512"}**
       * We also noticed (thanks Fabian) that there was a change recently that is computing the algorithm depending on the bitlength of the key’s modulus here: [https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/-/blob/development/web-crypto/cryptokey.ts?ref\_type=heads\&blame=0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/-/blob/development/web-crypto/cryptokey.ts?ref\_type=heads\&blame=0)
       *  
       * The result in our case is that **PS512** was computed from the gx-wizard and set to the JWS header.
       * But: The JWS-Specification does **not mention** **PS512**: [https://www.w3.org/community/reports/credentials/CG-FINAL-lds-jws2020-20220721/#jose-conformance](https://www.w3.org/community/reports/credentials/CG-FINAL-lds-jws2020-20220721/#jose-conformance)
       * 

       * In my opinion PS256 is correct because of “*2048+*” in the specification: if the length exceeds 2048 bits (= 256 bytes) then PS256 is to be set.
   * I am currently not able to verify the signature of an LRN coming from the notary ([https://registration.lab.gaia-x.eu/v1-staging/docs/)](https://registration.lab.gaia-x.eu/v1-staging/docs/)) within the catalogue. 
       * Is it planned to integrate the signing tool that is used by the other gx components?


## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)





