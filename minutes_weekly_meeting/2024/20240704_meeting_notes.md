
# Gaia-X OSS Community Call - July 4

Next call: Thursday, July 11, 2024, 09:00 - 09:45 CEST, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 



Moderation: Kai Meinke

Minutes of meeting : collective



### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

yes



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Demonstration GXFS-FR SSI Components
   1. Updates from the lab, lighthouses, projects and developers


Acceptance of agenda

yes



Number of participants

52



### Introduction of new participants

   * Arno Scheidereiter, neusta Aerospace, Aerospace Domain Gaia-X Hub Germany, COOPERANTS Gaia-X Lighthouse, Pontus-X Operating Company
   * 



### Interesting sessions, events, and news

   * ✨ **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * **Gaia-X Members Webinar**: July 9, 09:00 - 10:30
       * Data Spaces: How to maintain interoperability and protect your investment
       * latest updates and developments from the DSBC and success stories
   * **Gaia-X Members Webinar**, July 12, 12:00 - 12:45
       * Data Spaces: How to maintain interoperability and protect your investment
   * **Gaia-X Summit 24**
       * Location: Helsinki, Finland
       * Date: 14, 15 November 2024
       * [https://gaia-x.eu/summit-2024/](https://gaia-x.eu/summit-2024/)


### Demonstration of GXFS-FR SSI Components

   * Presentation of GXFS-FR Components used i.e. in Aster-X
   * Using OIDC4VP Draft 20 for Credential Exchange
   * OIDC4VCI Import Work in Progress (Employee to Company Import)
   * [https://webcatalog.aster-x.demo23.gxfs.fr/catalog](https://webcatalog.aster-x.demo23.gxfs.fr/catalog)
   * Advancements with the Corporate Wallet and SSI components. A corporate wallet to manage VC owned by a legal entity and to manage employees and available with their wallets.
   * Presentation by Yves Marie Pondaven (Docaposte) and Team, Alexandre Nicaise, Erwan Goudard, Valerie Bruna, Damien Cochard
   * Repositories:
       * Compliance: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/aster-x-compliance](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/aster-x-compliance)
       * Smart Contracts: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/smart-contracts/specifications](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/smart-contracts/specifications)
       * Issuer: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer)
       * Corporate Wallet: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/wallet-corporate](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/wallet-corporate)
       * Verifier: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/aster-x-verifier](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/aster-x-verifier)
       * Payment Gateway: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/payment-gateway](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/payment-gateway)
       * IdP Bridge: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/idp-bridge](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/idp-bridge)


### Updates from the Community, Developers, Lighthouses and Lab

   * New Gaia-X Magazine out: [https://gaia-x.eu/media/the-gaia-x-magazine/](https://gaia-x.eu/media/the-gaia-x-magazine/)


## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)