
# Gaia-X OSS Community Call - July 11

Next call: Thursday, July 18, 2024, 09:00 - 09:45 CEST, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 



Moderation: Kai Meinke

Minutes of meeting : collective



### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

yes



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Demonstration GXFS-FR SSI Components - Part 2
   1. Demonstration TUM+msg SSI-OIDC Bridge Provider
   1. Updates from the lab, lighthouses, projects and developers


Acceptance of agenda

yes



Number of participants

70



### Introduction of new participants

   * 



### Interesting sessions, events, and news

   * ✨ **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * **Gaia-X Members Webinar**, July 12, 12:00 - 12:45
       * Data Spaces: How to maintain interoperability and protect your investment
       * Feature of interoperable lighthouse projects COOPERANTS, EuProGigant, Gaia-X 4 Future Mobility sharing a common ecosystem, powered by Gaia-X and GXDCH
   * **Gaia-X Summit 24**
       * Location: Helsinki, Finland
       * Date: 14, 15 November 2024
       * [https://gaia-x.eu/summit-2024/](https://gaia-x.eu/summit-2024/)


### Demonstration of GXFS-FR SSI Components - Part 2

   * Presentation of GXFS-FR Components and development status
   * Smart Contract Manager + 
   * Presentation by Yves Marie Pondaven (Docaposte) and Team, Alexandre Nicaise, Erwan Goudard, Valerie Bruna, Damien Cochard, Moez Somai, Atoine Hattabe, Benoit Tabutiaux
   * Description: ... to be filled by the team
   * Questions:
       * How are ODRL Statements and Smart Contract linked?
       * Is the code OSS and are the Smart Contracts available as OSS?
       * Is the payment channel off-chain? --> Oracle and Docaposte payment solution
       * Did you look at the DSP data space protocol? 
       * Does it use the same negotiation mechanisms?
       * Where are the ODRL policies possible described?
       * What about logging services the exchange of files? (to run evidence whether data has been transmitted, received and rules and terms of use (data usage policies) have been respected or not within the Gaia-X ecosystem)  This is being written by which component?
       * Who deploys the smart contracts?
       * When is the project ending? In about two days.
       * Private chain currently being used, feeless.
       * Where can we see the specifications?
       * Is it a centralized Oracle that is being authorized to send data or can this mechanism be decentralized?
   * Repositories:
       * Overall: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario)
       * Compliance: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/aster-x-compliance](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/aster-x-compliance)
       * Smart Contracts: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/smart-contracts/specifications](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/smart-contracts/specifications)
       * Issuer: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer)
       * Corporate Wallet: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/wallet-corporate](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/wallet-corporate)
       * Verifier: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/aster-x-verifier](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/aster-x-verifier)
       * Payment Gateway: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/payment-gateway](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/payment-gateway)
       * IdP Bridge: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/idp-bridge](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/idp-bridge)


### msg Demonstration of SSI-OIDC-Bridge

   * Introduction by Felix Hoops (TUM)
   * Description: We have built a simple to deploy and configure solution to add SSI sign-ins to any OIDC-supporting service application. The main draw of this solution is that the bridge is a containerized service using open standards with just one configuration file. This makes it usable without special technical knowledge for SSI being required. A test instance is currently run by msg to enable signing in to the Gaia-X Portal found here: [https://portal.gxfs.gx4fm.org/](https://portal.gxfs.gx4fm.org/)
   * Repositories: [https://github.com/GAIA-X4PLC-AAD/ssi-to-oidc-bridge](https://github.com/GAIA-X4PLC-AAD/ssi-to-oidc-bridge)
   * For testing the bridge login: Use the Altme wallet, an open-source SSI + Web3 wallet
   * Questions: 
       * The verification follows DIF Presentation Exchange , which version? Also is it compatible with multiple wallets (Service provider unaware of holder's wallet) ?
           * We use OID4VP Draft 18 (current implementor's draft) that mandates PEX v2 ([https://openid.net/specs/openid-4-verifiable-presentations-1\_0-ID2.html#name-dif-presentation-exchange-2)](https://openid.net/specs/openid-4-verifiable-presentations-1\_0-ID2.html#name-dif-presentation-exchange-2))
           * Any wallet that adheres to the OID4VP standard is compatible. The only limit is that we currently only support JSON-LD VCs in the flow (jwt format support planned in near future) and the DID method support is limited to common methods (e.g., did:web, did:key, did:pkh, ...). 
       * The problem I see with OIDC4VC is that the VC format/schema is not the one that the Dataspace Ecosystem required (might need conversion). I might contact you later after reading the link, as I have interest in this theme.
           * As far as I am aware, the OpenID-related standards work with any W3C VC and even Hyperledger Anoncreds. So your VCs are probably compatible. In practice, the bridge just supports JSON-LD VCs, with JWT-VCs planned.
           * Your VC schema does not matter to the bridge. The bridge is novel in the sense that it does not check the schema (contexts are not necessarily safe from manipulation in JSON-LD). We instead just require you to write a policy that contains issuers you trust and fields you require. Optionally, you can also add constraints on specific field content.
       * Presentation Definition Generator can you bit elaborate on it ( Are you using any library or generating exchange query using some UIs)
           * We use OID4VP Draft 18 (current implementor's draft) that mandates PEX v2 ([https://openid.net/specs/openid-4-verifiable-presentations-1\_0-ID2.html#name-dif-presentation-exchange-2)](https://openid.net/specs/openid-4-verifiable-presentations-1\_0-ID2.html#name-dif-presentation-exchange-2)). The security-critical validation in the Bridge happens based on a) general VC signature verification and b) our custom login policy format. But we still want to use PEX in the OID4VP exchange to make it more user-friendly. If we kept the presentation definition empty, the wallet could not already suggest fitting VCs to the user.
           * Our custom Login Policy also contains all the content that would be required for a Presentation Definition. So in the interest of having our simple one-file configuration, we wrote a custom transformer that can build a valid Presentation Definition from a Login Policy, which now happens on the fly.


### Updates from the Community, Developers, Lighthouses and Lab

   * Creation of an ecosystem community, starting with ecosystem configuration files
       * List of Trust Anchors via dns records
       * [https://gitlab.com/ecosystem-community/dns-records](https://gitlab.com/ecosystem-community/dns-records)


## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)