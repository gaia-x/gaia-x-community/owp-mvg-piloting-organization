
# Gaia-X OSS Community Call - July 25

Next call: Thursday, August 1, 2024, 09:00 - 09:45 CEST, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 



Moderation: Kai Meinke

Minutes of meeting : collective



### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240718\_meeting\_notes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240718\_meeting\_notes.md)



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Updates from the lab, lighthouses, projects and developers


Acceptance of agenda

yes



Number of participants

35



### Introduction of new participants





### Interesting sessions, events, and news

   * ✨ **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * **Gaia-X Summit 24**
       * Location: Helsinki, Finland
       * Date: 14, 15 November 2024
       * [https://gaia-x.eu/summit-2024/](https://gaia-x.eu/summit-2024/)
   * **Nexusforum Summit 24**
       * Location: Brussels, Belgium
       * Date: 19, 20 September 2024
       * [https://nexusforum.eu/](https://nexusforum.eu/)




### Updates from the Community, Developers, Lighthouses and Lab

**Claim Compliance Provider (CCP) by Robert Schubert  (msg)**

**In general**: The Claim Compliance Provider is a component that 

   * signs claims, 
   * extracts gaia-x claims from domain specific claims
   * sends the gaia-x claims in a Verifiable Presentation to the Compliance Service
   * invokes the federated catalogue with both gaia-x and domain-specific claims (currently /verfification endpoint is implemented.
   * returns up to 3 VPs:
       * #1: Containing the ServiceOffering(s)
       * #2: Containing the Resource specific claims
       * #3: Containing the Compliance Credential from the Compliance Service.
It is built modular so single implementations like the signing component can be easily exchanged by adding another implementation (which implements a certain interface) and adjusting the app configuration.

**Artifacts**:

   * Source Code: [https://github.com/GAIA-X4PLC-AAD/claim-compliance-provider/](https://github.com/GAIA-X4PLC-AAD/claim-compliance-provider/)
   * Architecture overview: [https://github.com/GAIA-X4PLC-AAD/claim-compliance-provider/blob/main/docs/ARCHITECTURE.md](https://github.com/GAIA-X4PLC-AAD/claim-compliance-provider/blob/main/docs/ARCHITECTURE.md)
   * Running application / OpenAPI: [https://claim-compliance-provider.gxfs.gx4fm.org/docs/](https://claim-compliance-provider.gxfs.gx4fm.org/docs/)
   * Example gaia-x claims: [https://github.com/GAIA-X4PLC-AAD/gaia-x-compliant-claims-example](https://github.com/GAIA-X4PLC-AAD/gaia-x-compliant-claims-example)
   * Corrected gaia-x Tagus vocabulary
       * [https://github.com/GAIA-X4PLC-AAD/ontology-management-base/tree/main/gx](https://github.com/GAIA-X4PLC-AAD/ontology-management-base/tree/main/gx)
       * This vocabulary is imported into the [http://fc-server.gxfs.gx4fm.org/](http://fc-server.gxfs.gx4fm.org/) which makes it possible to have both gaia-x compliance and catalogue compliance (both verifySemantics and verifySchema are set to TRUE). 
   * An integration of the CCP into the PLC-AAD demo EDCs running here: 
       * [https://edcdb-pr.gxfs.gx4fm.org/](https://edcdb-pr.gxfs.gx4fm.org/), 
       * [https://edcdb-co.gxfs.gx4fm.org/](https://edcdb-co.gxfs.gx4fm.org/)
Have a look and feel free to contribute :)



🧪 Lab Team is currently working on the Loire Gaia-X labelling process combining the new ontology context and shapes, VC-JWT, Verifiable Credential Data Model v2.0 and others.



## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)