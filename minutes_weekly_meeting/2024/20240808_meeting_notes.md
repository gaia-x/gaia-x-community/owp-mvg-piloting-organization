
# Gaia-X OSS Community Call - August 8

Next call: Thursday, August 15, 2024, 09:00 - 09:45 CEST, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 



Moderation: Kai Meinke

Minutes of meeting : collective



### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240801\_meeting\_notes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240801\_meeting\_notes.md)



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Updates from the lab, lighthouses, projects and developers


Acceptance of agenda

yes



Number of participants

39



### Introduction of new participants

- Name, Institution, Gaia-X related project



### Interesting sessions, events, and news

   * Community invited to propose content for the DSSC Toolbox: [https://dssc.eu/space/News/blog/533430342/The+DSSC+Toolbox+and+its+validation+scheme](https://dssc.eu/space/News/blog/533430342/The+DSSC+Toolbox+and+its+validation+scheme)
   * ✨ **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * **Tech Deep dive**
       * Online
       * Date: Beginning of September
       * Topic: Gaia-X Ontology, toolchain around it and progress made on the 24.04 releases
   * **Nexusforum Summit 24**
       * Location: Brussels, Belgium
       * Date: 19, 20 September 2024
       * [https://nexusforum.eu/](https://nexusforum.eu/)
   * **Gaia-X Summit 24**
       * Location: Helsinki, Finland
       * Date: 14, 15 November 2024
       * [https://gaia-x.eu/summit-2024/](https://gaia-x.eu/summit-2024/)
   * Data Space Implementation Forums
       * Regular Online Meetings for Reference Implementations along the DSSC Building Blocks
       * Registration Link: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)


### Updates from the Community, Developers, Lighthouses and Lab

🧪 Lab Team is currently working on the Loire Gaia-X labelling process combining the new ontology context and shapes, VC-JWT, Verifiable Credential Data Model v2.0 and others.



🧪 New Pontus-X Docs : [https://docs.pontus-x.eu/](https://docs.pontus-x.eu/)

       * General Introduction \& Getting started guides
       * Onboarding refering to Gaia-X Participant Credentials: [https://docs.pontus-x.eu/docs/getting-started/introduction](https://docs.pontus-x.eu/docs/getting-started/introduction) but not mandatory as many participants still struggle with certificate acquisition. 
       * Network configuration \& Technical Architecture
       * Draft on updated Metadata Standard for Ocean Enterprise to be aligned with Gaia-X Loire Release
       * Contribution Guide
   * Onboarding to the Pontus-X Ecosystem becoming decentralized
       * Faucet implemented, based on the Ecosystem Participant Registry [https://www.pontus-x.eu/](https://www.pontus-x.eu/)
       * Registered participants can retrieve fee and payment tokens for the ecosystem
       * New onboarding tutoral in the market is enabled
   * Ongoing Pontus-X Load and Performance Tests
       * Preparing for production usage end of 2024 and scaling the ecosystem.
       * Developer and Staging ecosystem being tested with 5000 tx/h to begin with. No issues have been encountered, very low load on the ecosystem.
       * Load tests to be continued with higher frequencies and more intense load on the connector components.
   * 

   * Question from Robert Schubert
       * sorry but I cannot join this meeting today, but I have a question:
       * Is there a public plan where I can look up the roughly planned release date for Loire?
           * => Not date yet. We're implementing the criteria from the Compliance Document 24.06-prerelease, might be subject to changes. APIs are defined and should not change that much (VC-JWT as input, notary with multiple endpoints etc.)
           * Ongoing tasks: [https://gaia-x.atlassian.net/issues/?jql=project+%3D+%22LAB%22+AND+status+IN+%28%22In+Progress%22%2C+%22To+Do%22%29+ORDER+BY+created+DESC\&atlOrigin=eyJpIjoiOGJiZDI3YjkzNmUzNDE3OThhZjhkYWQwOThlN2E3MTMiLCJwIjoiaiJ9](https://gaia-x.atlassian.net/issues/?jql=project+%3D+%22LAB%22+AND+status+IN+%28%22In+Progress%22%2C+%22To+Do%22%29+ORDER+BY+created+DESC\&atlOrigin=eyJpIjoiOGJiZDI3YjkzNmUzNDE3OThhZjhkYWQwOThlN2E3MTMiLCJwIjoiaiJ9)


## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)
   * Gaia-X 101 Workshop: [https://gitlab.com/gaia-x/lab/workshops/gaia-x-101](https://gitlab.com/gaia-x/lab/workshops/gaia-x-101) \& [https://www.youtube.com/watch?v=xHaBM-T2--k](https://www.youtube.com/watch?v=xHaBM-T2--k)
   * Data Space Implementation Forums: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)