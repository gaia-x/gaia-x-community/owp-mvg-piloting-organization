
#  Gaia-X OSS Community Call - August 22

Next call: Thursday, August 29, 2024, 09:00 - 09:45 CEST, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 



Moderation: Kai Meinke

Minutes of meeting : collective



### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240815\_meeting\_nodes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240815\_meeting\_nodes.md)



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Demonstration - Asset Administration Shell \& Product Carbon Footprint Calculation
   1. Updates from the lab, lighthouses, projects and developers


Acceptance of agenda

yes



Number of participants

47



### Introduction of new participants

- Tom Last,  Health-X, - elbtech



### Interesting sessions, events, and news

   * **Community invited to propose content for the DSSC Toolbox**: [https://dssc.eu/space/News/blog/533430342/The+DSSC+Toolbox+and+its+validation+scheme](https://dssc.eu/space/News/blog/533430342/The+DSSC+Toolbox+and+its+validation+scheme)
   * ✨ **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * **Data Space Implementation Forums**
       * Regular Online Meetings for Reference Implementations along the DSSC Building Blocks
       * Registration Link: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)
   * **Gaia-X Members Webinar**
       * August 22, 1100-1145, virtual
       * Key developments and achievements from the first half of the year
       * [https://gaia-x.eu/event/gaia-x-members-webinar-mid-year-insights-updates-from-our-working-groups-and-sprints/](https://gaia-x.eu/event/gaia-x-members-webinar-mid-year-insights-updates-from-our-working-groups-and-sprints/)
   * **Pontus-X Open Ecosystem Call** 
       * August 29, virtual**, Time?**
       * Ecosystem updates, roadmap, use case spotlight and community exchange
       * [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_MzQ5M2IzNmQtMTgxYi00MjIzLWFiMDYtYTg5OWQyNDZiOGY5%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%2282638514-a1f4-4d88-be55-23e168a746da%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_MzQ5M2IzNmQtMTgxYi00MjIzLWFiMDYtYTg5OWQyNDZiOGY5%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%2282638514-a1f4-4d88-be55-23e168a746da%22%7d)
   * **Tech Deep dive**
       * Online, date to be confirmed
       * Date: Beginning of September
       * Topic: Gaia-X Ontology, toolchain around it and progress made on the 24.04 releases
   * **Technology Talks Austria**
       * September 12-13, Vienna, Austria
       * September 13 Special Session on Manufacturing-X
       * with Thomas Hahn (Siemens), Oscar Lazaro (Innovalia), Jens Gayko (VDI, Germany), John Blankendahl (Brainport Industries), Jean-Pascal Riss (Schneider Electric), Bernhard Peischl (AVL), Sebastian Schneider (DMG Mori), Anja Misselbeck (Catena-X)
       * Link: [https://technologytalks.ait.ac.at/en/konferenzprogramm](https://technologytalks.ait.ac.at/en/konferenzprogramm)
   * **OCX Conference, Eclipse Foundation**
       * Co-located event for data spaces
       * [https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/summary](https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/summary)
   * **FIWARE Global Summit 2024**
       * September 18-19, 2024, Naples, Italy
       * [https://www.fiware.org/global-summit/](https://www.fiware.org/global-summit/)
       * Features Ulrich Ahle, Andrea Battaglia, and X-ecosystem projects
   * **Nexusforum Summit 24**
       * Location: Brussels, Belgium
       * Date: 19, 20 September 2024
       * [https://nexusforum.eu/](https://nexusforum.eu/)
   * **EuProGigant OpenHouse Day**
       * Focus on data spaces and Gaia-X in manufacturing
       * October 7-8, 2024, Vienna, Austria
       * [https://euprogigant.com/en/event/euprogigant-open-house-day-2024/](https://euprogigant.com/en/event/euprogigant-open-house-day-2024/)
       * [https://gaia-x.eu/event/euprogigant-open-house-day-2024-8-october-in-vienna/](https://gaia-x.eu/event/euprogigant-open-house-day-2024-8-october-in-vienna/)
   * **Scaling Industrial Data Ecosystems - United For One Vision**
       * October 8-9, 2024, Berlin, Germany
       * Key event for digital manufacturing ecosystems, Manufacturing-X, IPCEI-Cloud "∞ra"
   * **Gaia-X Hub Germany at Smart Country Convention**
       * October 15-17, Berlin
       * [https://gaia-x.eu/event/meet-the-gaia-x-hub-germany-at-the-smart-country-convention/](https://gaia-x.eu/event/meet-the-gaia-x-hub-germany-at-the-smart-country-convention/)
   * **German Digitial Summit 2024**
       * October 21-22, 2024, Frankfurt, Germany
       * Focus on innovation, sovereignty and internationality
       * [https://www.de.digital/DIGITAL/Navigation/DE/Digital-Gipfel/digital-gipfel.html](https://www.de.digital/DIGITAL/Navigation/DE/Digital-Gipfel/digital-gipfel.html)
   * **Gaia-X Summit 24**
       * Location: Helsinki, Finland
       * Date: 14, 15 November 2024
       * [https://gaia-x.eu/summit-2024/](https://gaia-x.eu/summit-2024/)


### 🛠 Demonstration - AAS and PCF Calculation

Presentation by Fabian Gast, EuProGigant, PTW TU Darmstadt

   * Generation of Product Carbon Footprint (PCF) following IDTA PCF Submodel Template using Compute-to-Data to ensure technical data sovereignty
       * [https://portal.euprogigant.io/asset/did:op:7008fbeaff95defda057f26d1802903287a295d25dbd0481bf0a93540f6f8e9b](https://portal.euprogigant.io/asset/did:op:7008fbeaff95defda057f26d1802903287a295d25dbd0481bf0a93540f6f8e9b)
       * [https://portal.euprogigant.io/asset/did:op:a03c48ae417db04dbc3e3ad0974dc66cd70d9ac38277218846756cfb468866fc](https://portal.euprogigant.io/asset/did:op:a03c48ae417db04dbc3e3ad0974dc66cd70d9ac38277218846756cfb468866fc)
   * Follows OPC UA Companions Specification for CNC-Systems
       * [https://reference.opcfoundation.org/CNC/docs/](https://reference.opcfoundation.org/CNC/docs/)
   * Uses AAS Submodel Template IDTA 02023-0-9 Carbon Footprint
       * [https://industrialdigitaltwin.org/wp-content/uploads/2024/01/IDTA-2023-0-9-\_Submodel\_CarbonFootprint.pdf](https://industrialdigitaltwin.org/wp-content/uploads/2024/01/IDTA-2023-0-9-\_Submodel\_CarbonFootprint.pdf)
   * Cooperation of
       * CliCE-DiPP [https://clice-dipp.de/en/](https://clice-dipp.de/en/)
       * EuProGigant [https://euprogigant.com/](https://euprogigant.com/)
       * ESCOM [https://escom-project.de/en/](https://escom-project.de/en/) 
   * Gaia-X Service Credentials to be adapted to include the relevant information and describe the new services




### Updates from the Community, Developers, Lighthouses and Lab

🧪 Lab Team is currently working on the Loire Gaia-X labelling process combining the new ontology context and shapes, VC-JWT, Verifiable Credential Data Model v2.0 and others.



🧪 Gaia-X Digital Clearing Houses Performance and Test Suite Overview is now available:

   * Features Clearing House Status Tests \& Response Times
       * [https://docs.gaia-x.eu/framework/?tab=clearing-house](https://docs.gaia-x.eu/framework/?tab=clearing-house) 


🔏 deltaDAO AG Gaia-X Digital Clearing Houses (GXDCH) now up and running

   * Accreditation process ongoing, tests successful
       * Notary Service: [https://notary.gxdch.delta-dao.com/v1/docs/]([]https://notary.gxdch.delta-dao.com/v1/docs/[])
       * Registry Service: [https://registry.gxdch.delta-dao.com/v1/docs]([]https://registry.gxdch.delta-dao.com/v1/docs[])
       * Compliance Service: [https://compliance.gxdch.delta-dao.com/v1/docs/]([]https://compliance.gxdch.delta-dao.com/v1/docs/[])
   * Added value services to be defined


🧪 New Pontus-X Ecosystem Landing Page available to make digital service ecosystem implementations easier and the open-source software framework for accessible to newcomers

   * Landing Page [https://pontus-x.eu](https://pontus-x.eu) features:
       * Portals / Participant Agents Overview
       * Overview of Documentation, Resources and Software Components
       * Use Cases
       * Ecosystem Operators
   * Portal / Participant Agent moved to [https://portal.pontus-x.eu](https://portal.pontus-x.eu)


## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)
   * Gaia-X 101 Workshop: [https://gitlab.com/gaia-x/lab/workshops/gaia-x-101](https://gitlab.com/gaia-x/lab/workshops/gaia-x-101) \& [https://www.youtube.com/watch?v=xHaBM-T2--k](https://www.youtube.com/watch?v=xHaBM-T2--k)
   * Data Space Implementation Forums: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)