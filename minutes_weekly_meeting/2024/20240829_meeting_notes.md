
#  Gaia-X OSS Community Call - August 29

Next call: Thursday, September 5, 2024, 09:00 - 09:45 CEST, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 



Moderation: Kai Meinke

Minutes of meeting : collective



### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

Notes: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240822\_meeting\_notes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240822\_meeting\_notes.md)

Acceptance: yes



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. Update on the XFSC, Implementation Phase 2
   1. Updates from the lab, lighthouses, projects and developers


Acceptance of agenda: yes

Number of participants: 45



### Introduction of new participants

- name, affiliation, project



### Interesting sessions, events, and news

   * ✨ **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * **Data Space Implementation Forums**
       * Regular Online Meetings for Reference Implementations along the DSSC Building Blocks
       * Registration Link: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)
   * **Pontus-X Open Ecosystem Call** 
       * August 29, virtual, 13:00 - 14:00
       * Tech update, Product Carbon Footprint (PCF) Calculation to PCF, new participants, decentralized onboarding, event roadmap
       * [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_MzQ5M2IzNmQtMTgxYi00MjIzLWFiMDYtYTg5OWQyNDZiOGY5%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%2282638514-a1f4-4d88-be55-23e168a746da%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_MzQ5M2IzNmQtMTgxYi00MjIzLWFiMDYtYTg5OWQyNDZiOGY5%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%2282638514-a1f4-4d88-be55-23e168a746da%22%7d)
   * **Tech Deep dive**
       * Online, date to be confirmed
       * Date: Third week of September
       * Topic: Gaia-X Ontology, toolchain around it and progress made on the 24.04 releases
       * LinkML, Shacl Shapes, IPFS, DNS pinning
   * **Technology Talks Austria**
       * September 12-13, Vienna, Austria
       * September 13 Special Session on Manufacturing-X
       * with Thomas Hahn (Siemens), Oscar Lazaro (Innovalia), Jens Gayko (VDI, Germany), John Blankendahl (Brainport Industries), Jean-Pascal Riss (Schneider Electric), Bernhard Peischl (AVL), Sebastian Schneider (DMG Mori), Anja Misselbeck (Catena-X)
       * Link: [https://technologytalks.ait.ac.at/en/konferenzprogramm](https://technologytalks.ait.ac.at/en/konferenzprogramm)
   * **FIWARE Global Summit 2024**
       * September 18-19, 2024, Naples, Italy
       * [https://www.fiware.org/global-summit/](https://www.fiware.org/global-summit/)
       * Features Ulrich Ahle, Andrea Battaglia, and X-ecosystem projects
   * **Nexusforum Summit 24**
       * Location: Brussels, Belgium
       * Date: 19, 20 September 2024
       * [https://nexusforum.eu/](https://nexusforum.eu/)
   * **EuProGigant OpenHouse Day**
       * Focus on data spaces and Gaia-X in manufacturing
       * October 7-8, 2024, Vienna, Austria
       * [https://euprogigant.com/en/event/euprogigant-open-house-day-2024/](https://euprogigant.com/en/event/euprogigant-open-house-day-2024/)
       * [https://gaia-x.eu/event/euprogigant-open-house-day-2024-8-october-in-vienna/](https://gaia-x.eu/event/euprogigant-open-house-day-2024-8-october-in-vienna/)
   * **Scaling Industrial Data Ecosystems - United For One Vision**
       * October 8-9, 2024, Berlin, Germany
       * Key event for digital manufacturing ecosystems, Manufacturing-X, IPCEI-Cloud "∞ra"
   * **Gaia-X Hub Germany at Smart Country Convention**
       * October 15-17, Berlin
       * [https://gaia-x.eu/event/meet-the-gaia-x-hub-germany-at-the-smart-country-convention/](https://gaia-x.eu/event/meet-the-gaia-x-hub-germany-at-the-smart-country-convention/)
   * **German Digital Summit 2024**
       * October 21-22, 2024, Frankfurt, Germany
       * Focus on innovation, sovereignty and internationality
       * [https://www.de.digital/DIGITAL/Navigation/DE/Digital-Gipfel/digital-gipfel.html](https://www.de.digital/DIGITAL/Navigation/DE/Digital-Gipfel/digital-gipfel.html)
   * **OCX Conference, Eclipse Foundation**
       * October 22-24
       * Co-located event for data spaces
       * [https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/summary](https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/summary)
   * **Gaia-X Summit 24**
       * Location: Helsinki, Finland
       * Date: 14, 15 November 2024
       * [https://gaia-x.eu/summit-2024/](https://gaia-x.eu/summit-2024/)


### 🛠 Update - Eclipse Cross Federation Service Components

Update by Lauresha Memeti, eco Association

   * XFSC Repository: [https://gitlab.eclipse.org/eclipse/xfsc](https://gitlab.eclipse.org/eclipse/xfsc) 
   * XFSC Implementation phase 2 - QA to be finalised for all components planned for October 2024 
       * Overview of the Components \& QA :
           * Trust Service API (TSA) Extension 1 – finalized
           * TRAIN (Trust Management Infrastructure) – 90% finalized
           * OCM W-STACK – QA ongoing
           * OCM Extension 1 – QA started beginning of August
           * PCM Extension 1 \& PCM Cloud –QA not yet started
           * Notarization API Extension 1 - QA not yet started
       * Everyone is welcome to look at the repositories and open issues / give feedback into the repositories 
           * [https://gitlab.eclipse.org/eclipse/xfsc](https://gitlab.eclipse.org/eclipse/xfsc) 
       * If you don´t have access to the repositories please contact lauresha.memeti@eco.de 




### Updates from the Community, Developers, Lighthouses and Lab

🧪 Lab Team is currently working on the Loire Gaia-X labelling process combining the new ontology context and shapes, VC-JWT, Verifiable Credential Data Model v2.0 and others. 



👷‍♂️ Labelling criteria have started to be documented (work in progress) in the Compliance Engine documentation  [https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md)



🧪 Gaia-X Digital Clearing Houses Performance and Test Suite Overview is now available:

   * Features Clearing House Status Tests \& Response Times
       * [https://docs.gaia-x.eu/framework/?tab=clearing-house](https://docs.gaia-x.eu/framework/?tab=clearing-house) 


🔏 deltaDAO AG Gaia-X Digital Clearing Houses (GXDCH) accredited

   * Fix of the Notary Certificate to be applied next. 


## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)
   * Gaia-X 101 Workshop: [https://gitlab.com/gaia-x/lab/workshops/gaia-x-101](https://gitlab.com/gaia-x/lab/workshops/gaia-x-101) \& [https://www.youtube.com/watch?v=xHaBM-T2--k](https://www.youtube.com/watch?v=xHaBM-T2--k)
   * Data Space Implementation Forums: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)