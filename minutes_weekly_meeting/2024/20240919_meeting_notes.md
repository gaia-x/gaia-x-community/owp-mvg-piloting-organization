# Gaia-X OSS Community Call - September 19

Next call: Thursday, September 26, 2024, 09:00 - 09:45 CEST, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 

Moderation: Kai Meinke

Minutes of meeting : collective

### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

Notes: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240912\_meeting\_notes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240912\_meeting\_notes.md)

Acceptance: yes



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1. NCDS Intro
6. Updates from the lab, lighthouses, projects and developers



Acceptance of agenda: yes

Number of participants: 43



### Introduction of new participants

- name, affiliation, projec

- Charles Verleyen, Astrafy

- Shay Begun, Astrafy

- Robin Andersen, CTO nomadlabs,

- Hermund Dale, Smart Innovation Norway - Setting up the Gaia-X Hub Norway



### Interesting sessions, events, and news

   * ✨ **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * **Data Space Implementation Forums**
       * Regular Online Meetings for Reference Implementations along the DSSC Building Blocks
       * Registration Link: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)
   * **FIWARE Global Summit 2024**
       * September 18-19, 2024, Naples, Italy
       * [https://www.fiware.org/global-summit/](https://www.fiware.org/global-summit/)
       * Features Ulrich Ahle, Andrea Battaglia, and X-ecosystem projects
   * **Nexusforum Summit 24**
       * Location: Brussels, Belgium
       * Date: 19, 20 September 2024
       * [https://nexusforum.eu/](https://nexusforum.eu/)
   * **Trusted Data Transaction - Understand the Impacts of the Future Standardisation**
       * Location: Brussels, Belgium
       * Date: 24 September 2024
       * [https://www.trusted-data-transaction.org/en/event-trusted-data-transaction-sept-24](https://www.trusted-data-transaction.org/en/event-trusted-data-transaction-sept-24)
   * **Gaia-X Hub Germany Roadshow **(in german)
       * September 25, Kempten
       * Digitale Ökosysteme für smarte Regionen“ mit Schwerpunkt Landwirtschaft u. Tourismus
       * [https://gaia-x-hub.de/events/gaia-x-roadshow-kempten/](https://gaia-x-hub.de/events/gaia-x-roadshow-kempten/)
   * **Tech Deep dive (15:00 - 16:00)**
       * Online, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_YWZmZDQ0ODEtODdlNC00ODE2LWI0Y2UtODgwYTI4ZjkzZThh%40thread.v2/0?context=%7b%22Tid%22%3a%225845e7c0-703b-46a4-88c5-02cbecfbb49f%22%2c%22Oid%22%3a%22ce231bc1-4696-4c00-b98f-9b19b7d27e2c%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_YWZmZDQ0ODEtODdlNC00ODE2LWI0Y2UtODgwYTI4ZjkzZThh%40thread.v2/0?context=%7b%22Tid%22%3a%225845e7c0-703b-46a4-88c5-02cbecfbb49f%22%2c%22Oid%22%3a%22ce231bc1-4696-4c00-b98f-9b19b7d27e2c%22%7d)
       * Date: 26th of September
       * Topic: How the Gaia-X ontology is made using LinkML
       * LinkML, Shacl Shapes, IPFS, DNS pinning
   * **Pontus-X Open Community Call**
       * September 26, 13:00 - 14:00 CET
       * Online, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_MzQ5M2IzNmQtMTgxYi00MjIzLWFiMDYtYTg5OWQyNDZiOGY5%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%2282638514-a1f4-4d88-be55-23e168a746da%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_MzQ5M2IzNmQtMTgxYi00MjIzLWFiMDYtYTg5OWQyNDZiOGY5%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%2282638514-a1f4-4d88-be55-23e168a746da%22%7d)
   * **EuProGigant OpenHouse Day**
       * Focus on data spaces and Gaia-X in manufacturing
       * October 7-8, 2024, Vienna, Austria
       * [https://euprogigant.com/en/event/euprogigant-open-house-day-2024/](https://euprogigant.com/en/event/euprogigant-open-house-day-2024/)
       * [https://gaia-x.eu/event/euprogigant-open-house-day-2024-8-october-in-vienna/](https://gaia-x.eu/event/euprogigant-open-house-day-2024-8-october-in-vienna/)
   * **Scaling Industrial Data Ecosystems - United For One Vision**
       * October 8-9, 2024, Berlin, Germany
       * Key event for digital manufacturing ecosystems, Manufacturing-X, IPCEI-Cloud "∞ra"
   * **Gaia-X Hub Germany Roadshow **(in german)
       * October 10, Essen
       * Mit Daten zum Erfolg: Kompetenzmanagement in der digitalen Transformation
       * [https://gaia-x-hub.de/events/gaia-x-roadshow-essen/](https://gaia-x-hub.de/events/gaia-x-roadshow-essen/)
   * **Gaia-X Hub Germany at Smart Country Convention**
       * October 15-17, Berlin
       * [https://gaia-x.eu/event/meet-the-gaia-x-hub-germany-at-the-smart-country-convention/](https://gaia-x.eu/event/meet-the-gaia-x-hub-germany-at-the-smart-country-convention/)
   * **Gaia-X Hub Germany Roadshow **(in german)
       * October 16, Berlin
       * Innovative Ökosysteme und digitale Lösungen für smarte Cities und Regionen (Frühstück auf der SCCON)
       * [https://gaia-x-hub.de/events/gaia-x-roadshow-berlin/](https://gaia-x-hub.de/events/gaia-x-roadshow-berlin/)
   * **German Digital Summit 2024**
       * October 21-22, 2024, Frankfurt, Germany
       * Focus on innovation, sovereignty and internationality
       * [https://www.de.digital/DIGITAL/Navigation/DE/Digital-Gipfel/digital-gipfel.html](https://www.de.digital/DIGITAL/Navigation/DE/Digital-Gipfel/digital-gipfel.html)
   * **OCX Conference, Eclipse Foundation**
       * October 22-24
       * Co-located event for data spaces
       * [https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/summary](https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/summary)
   * **Launch of Gaia-X Hub Norway**
       * October 24, 2024
       * more details to be shared, virtual 
   * **Gaia-X Summit 24**
       * Location: Helsinki, Finland
       * Date: 14, 15 November 2024
       * [https://gaia-x.eu/summit-2024/](https://gaia-x.eu/summit-2024/)
   * **Gaia-X Hub Germany Roadshow **(in german)
       * November 19, Hof
       * Planen, Bauen, Betreiben – mit Daten zum Erfolg in der Baubranche (Anmeldung ca. Ende September möglich)


### Nordic Centre of Data Sovereignty (NCDS) Introduction

   * Hermund Dale, Smart Innovation Norway - Setting up the Gaia-X Hub Norway
   * Applied for funding by Nordic Research Council. Final application in June 2025.
   * Intro to the work package structure


### Updates from the Community, Developers, Lighthouses and Lab

🧪 Lab Team is currently working on the Loire Gaia-X labelling process combining the new ontology context and shapes, VC-JWT, Verifiable Credential Data Model v2.0 and others. 



👷‍♂️ Labelling criteria implementation have started to be documented (work in progress) in the Compliance Engine documentation  [https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md)



**Question on the native identity framework of EDC/Tractus-X:**

   * Is it correct, that the native identity is a SECP256K1 key
   * Assumption based on
       * [https://catenax-ev.github.io/docs/next/standards/CX-0149-Dataspaceidentityandidentification](https://catenax-ev.github.io/docs/next/standards/CX-0149-Dataspaceidentityandidentification)
       * [https://catenax-ev.github.io/docs/next/standards/CX-0049-DIDDocumentSchema](https://catenax-ev.github.io/docs/next/standards/CX-0049-DIDDocumentSchema)
       * [https://catenax-ev.github.io/docs/standards/CX-0006-RegistrationAndInitialOnboarding#42-non-normative-references](https://catenax-ev.github.io/docs/standards/CX-0006-RegistrationAndInitialOnboarding#42-non-normative-references)
       * [https://github.com/search?q=org%3Aeclipse-tractusx+SECP256K1\&type=code](https://github.com/search?q=org%3Aeclipse-tractusx+SECP256K1\&type=code)
       * Mentions JsonWebKey2020, 
       * The encoding format for VCs and Verifiable Presentations (VP) is JWT ES256K/secp256k1.
   * How is that connected to Gaia-X Compliance and RSA Keys and X.509 Certs issued by TSPs? 
   * How are participant identities being recorded for reference in the ecosystem? Is there some kind of PKI / public registry?
   * Is it correct to assume EDC / Catena-X uses SECP256K1 keys as primary source of identity ownership?
   * Can somebody pinpoint an example and spec in the EDC framework?
   * Pontus-X uses SECP256K1 keys natively and RSA/X.509 for Gaia-X Compliance
       * Native Service Credentials signed with SECP256K1
       * Gaia-X Service Credentials signed with RSA, bound to X.509 with reference to native service and components
       * Identity / Key Ownership verified through TSP, according to Gaia-X Framework


**GXDCH Status Page Update:**

   * [https://docs.gaia-x.eu/framework/?tab=clearing-house](https://docs.gaia-x.eu/framework/?tab=clearing-house)


**Gaia-X Hub Germany published an initial Gaia-X Actors Map for the community:**

   * [https://gaia-x-hub.de/gx-community-karte/](https://gaia-x-hub.de/gx-community-karte/)
   * Everybody can submit proposals to the map here


**SIMPL published first guidelines**

   * Onboarding: [https://simpl-programme.ec.europa.eu/book-page/3a-onboarding-new-dataspace-participant-providers-data-application-infrastructure](https://simpl-programme.ec.europa.eu/book-page/3a-onboarding-new-dataspace-participant-providers-data-application-infrastructure)
   * Gaia-X Integration seems to be missing in the process (explicitly)?!
   * [https://simpl-programme.ec.europa.eu/book-page/5-provider-adds-new-resource-data-application-or-infrastructure-dataspaces-catalogue](https://simpl-programme.ec.europa.eu/book-page/5-provider-adds-new-resource-data-application-or-infrastructure-dataspaces-catalogue)


## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: gaia
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)
   * Gaia-X 101 Workshop: [https://gitlab.com/gaia-x/lab/workshops/gaia-x-101](https://gitlab.com/gaia-x/lab/workshops/gaia-x-101) \& [https://www.youtube.com/watch?v=xHaBM-T2--k](https://www.youtube.com/watch?v=xHaBM-T2--k)
   * Data Space Implementation Forums: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)