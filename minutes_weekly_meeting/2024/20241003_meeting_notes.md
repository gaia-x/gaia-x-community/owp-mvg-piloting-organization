
# Gaia-X OSS Community Call - October 3rd

Next call: Thursday, October 10, 2024, 09:00 - 09:45 CEST, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 



Moderation: Vincent Kelleher

Minutes of meeting : collective



### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

Notes:  [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240926\_meeting\_notes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20240926\_meeting\_notes.md)

Acceptance: yes



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
5. Updates from the lab, lighthouses, projects and developers



Number of participants: 31



### Introduction of new participants

- name, affiliation, project



### Interesting sessions, events, and news

   * ✨ **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * **Data Space Implementation Forums**
       * Regular Online Meetings for Reference Implementations along the DSSC Building Blocks
       * Registration Link: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)
   * **EuProGigant OpenHouse Day**
       * Focus on data spaces and Gaia-X in manufacturing
       * October 7-8, 2024, Vienna, Austria
       * [https://euprogigant.com/en/event/euprogigant-open-house-day-2024/](https://euprogigant.com/en/event/euprogigant-open-house-day-2024/)
       * [https://gaia-x.eu/event/euprogigant-open-house-day-2024-8-october-in-vienna/](https://gaia-x.eu/event/euprogigant-open-house-day-2024-8-october-in-vienna/)
   * **Scaling Industrial Data Ecosystems - United For One Vision**
       * October 8-9, 2024, Berlin, Germany
       * Key event for digital manufacturing ecosystems, Manufacturing-X, IPCEI-Cloud "∞ra"
   * **Gaia-X Hub Germany Roadshow **(in german)
       * October 10, Essen
       * Mit Daten zum Erfolg: Kompetenzmanagement in der digitalen Transformation
       * [https://gaia-x-hub.de/events/gaia-x-roadshow-essen/](https://gaia-x-hub.de/events/gaia-x-roadshow-essen/)
   * **Gaia-X Hub Germany at Smart Country Convention**
       * October 15-17, Berlin
       * [https://gaia-x.eu/event/meet-the-gaia-x-hub-germany-at-the-smart-country-convention/](https://gaia-x.eu/event/meet-the-gaia-x-hub-germany-at-the-smart-country-convention/)
   * **Gaia-X Hub Germany Roadshow **(in german)
       * October 16, Berlin
       * Innovative Ökosysteme und digitale Lösungen für smarte Cities und Regionen (Frühstück auf der SCCON)
       * [https://gaia-x-hub.de/events/gaia-x-roadshow-berlin/](https://gaia-x-hub.de/events/gaia-x-roadshow-berlin/)
   * **German Digital Summit 2024**
       * October 21-22, 2024, Frankfurt, Germany
       * Focus on innovation, sovereignty and internationality
       * [https://www.de.digital/DIGITAL/Navigation/DE/Digital-Gipfel/digital-gipfel.html](https://www.de.digital/DIGITAL/Navigation/DE/Digital-Gipfel/digital-gipfel.html)
   * **OCX Conference, Eclipse Foundation**
       * October 22-24
       * Co-located event for data spaces
       * [https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/summary](https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/summary)
   * **Launch of Gaia-X Hub Norway**
       * October 24, 2024
       * more details to be shared, virtual 
   * **Gaia-X Summit 24**
       * Location: Helsinki, Finland
       * Date: 14, 15 November 2024
       * [https://gaia-x.eu/summit-2024/](https://gaia-x.eu/summit-2024/)
   * **Gaia-X Hub Germany Roadshow **(in german)
       * November 19, Hof
       * Planen, Bauen, Betreiben – mit Daten zum Erfolg in der Baubranche (Anmeldung ca. Ende September möglich)
   * FOSDEM 2025
       *  1 \& 2 February Brussels
       * [https://fosdem.org/2025/](https://fosdem.org/2025/)


### Updates from the Community, Developers, Lighthouses and Lab

**Question regarding the Fiware Dataspace Connector**

   * -> Presentation planned for October 10.
   * 

Open question: Has anyone taken the course #8 in the Academy on how to deploy a GXDCH ? Any feedback ?

   * -> [https://academy.gaia-x.eu/courses/course-v1:2+tutorialsDeployGXDCH+tutorialsDeployGXDCH001/course/](https://academy.gaia-x.eu/courses/course-v1:2+tutorialsDeployGXDCH+tutorialsDeployGXDCH001/course/)
   * 

🧪 Lab Team has finished implementing the conformity criteria and is currently testing them before release.



**Question on the native identity framework of EDC/Tractus-X:**

   * Is it correct, that the native identity is a SECP256K1 key?
   * How is that connected to Gaia-X Compliance and RSA Keys and X.509 Certs issued by TSPs? 
   * How are participant identities being recorded for reference in the ecosystem? Is there some kind of PKI / public registry?
   * Is it correct to assume EDC / Catena-X uses SECP256K1 keys as primary source of identity ownership?
   * Can somebody pinpoint an example and spec in the EDC framework?






## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)
   * Gaia-X 101 Workshop: [https://gitlab.com/gaia-x/lab/workshops/gaia-x-101](https://gitlab.com/gaia-x/lab/workshops/gaia-x-101) \& [https://www.youtube.com/watch?v=xHaBM-T2--k](https://www.youtube.com/watch?v=xHaBM-T2--k)
   * Data Space Implementation Forums: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)

















