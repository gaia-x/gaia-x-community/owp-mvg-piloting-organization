
# Gaia-X OSS Community Call - October 10th

Next call: Thursday, October 17th, 2024, 09:00 - 09:45 CEST, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 



Moderation: 

Minutes of meeting : collective



### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

Notes:  [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20241003\_meeting\_notes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20241003\_meeting\_notes.md)

Acceptance: yes



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
   1. Introduction of new and existing participants
   1. Interesting sessions, events, and news
   1.  The FIWARE Data Space Connector and Gaia-X
   1. Update of the ICAM Document 24.07.
6. Updates from the lab, lighthouses, projects and developers



Number of participants: 53



### Introduction of new participants

- name, affiliation, project



### Interesting sessions, events, and news

   * ✨ **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * **Data Space Implementation Forums**
       * Regular Online Meetings for Reference Implementations along the DSSC Building Blocks
       * Registration Link: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)
   * **Gaia-X Hub Germany Roadshow **(in german)
       * October 10, Essen
       * Mit Daten zum Erfolg: Kompetenzmanagement in der digitalen Transformation
       * [https://gaia-x-hub.de/events/gaia-x-roadshow-essen/](https://gaia-x-hub.de/events/gaia-x-roadshow-essen/)
   * **Gaia-X Hub Germany at Smart Country Convention**
       * October 15-17, Berlin
       * [https://gaia-x.eu/event/meet-the-gaia-x-hub-germany-at-the-smart-country-convention/](https://gaia-x.eu/event/meet-the-gaia-x-hub-germany-at-the-smart-country-convention/)
   * **Gaia-X Hub Germany Roadshow **(in german)
       * October 16, Berlin
       * Innovative Ökosysteme und digitale Lösungen für smarte Cities und Regionen (Frühstück auf der SCCON)
       * [https://gaia-x-hub.de/events/gaia-x-roadshow-berlin/](https://gaia-x-hub.de/events/gaia-x-roadshow-berlin/)
   * **German Digital Summit 2024**
       * October 21-22, 2024, Frankfurt, Germany
       * Focus on innovation, sovereignty and internationality
       * [https://www.de.digital/DIGITAL/Navigation/DE/Digital-Gipfel/digital-gipfel.html](https://www.de.digital/DIGITAL/Navigation/DE/Digital-Gipfel/digital-gipfel.html)
   * **OCX Conference, Eclipse Foundation**
       * October 22-24
       * Co-located event for data spaces
       * [https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/summary](https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/summary)
   * **Launch of Gaia-X Hub Norway**
       * October 24, 2024
       * more details to be shared, virtual 
   * **Gaia-X Summit 24**
       * Location: Helsinki, Finland
       * Date: 14, 15 November 2024
       * [https://gaia-x.eu/summit-2024/](https://gaia-x.eu/summit-2024/)
   * **Gaia-X Hub Germany Roadshow **(in german)
       * November 19, Hof
       * Planen, Bauen, Betreiben – mit Daten zum Erfolg in der Baubranche (Anmeldung ca. Ende September möglich)
   * **XFSC Final Tech Workshop**
       * Date: November 28,29
       * Location: Madrid UPM, Spain
       * [https://www.gxfs.eu/xfsc-final-tech-workshop/](https://www.gxfs.eu/xfsc-final-tech-workshop/)
   * **Open Source eXPerience**
       * Dec 4+5, Paris
       * [https://www.opensource-experience.com/en/](https://www.opensource-experience.com/en/)
   * **FOSDEM 2025**
       *  1 \& 2 February, 2025, Brussels
       * [https://fosdem.org/2025/](https://fosdem.org/2025/)


### The FIWARE Data Space Connector and Gaia-X

- Introduction by Gernot Boege gernot.boege@fiware.org

   * Github repo: [https://github.com/FIWARE/data-space-connector](https://github.com/FIWARE/data-space-connector)
       * Local deployment: [https://github.com/FIWARE/data-space-connector/blob/main/doc/deployment-integration/local-deployment/LOCAL.MD](https://github.com/FIWARE/data-space-connector/blob/main/doc/deployment-integration/local-deployment/LOCAL.MD)
   * Webinar with live demo: [https://www.youtube.com/watch?v=hm5qMlhpK0g](https://www.youtube.com/watch?v=hm5qMlhpK0g)
   * DOME marketplace demo: [https://www.youtube.com/watch?v=B7ojYTS5mEs](https://www.youtube.com/watch?v=B7ojYTS5mEs)
   * APISIX: [https://apisix.apache.org/](https://apisix.apache.org/)
       * Plugins: [https://apisix.apache.org/docs/apisix/plugins/http-logger/](https://apisix.apache.org/docs/apisix/plugins/http-logger/)


### Updates from the Community, Developers, Lighthouses and Lab

   * 

**Community Recap: 10 steps to achieve Gaia-X Compliance / Basic Conformity for Participant and Service Credentials and why it matters.**

1. Generate and manage RSA keys

2. Acquire X.509 from Gaia-X accredited TSP and transform to match Gaia-X specs

3. Host X.509 and validate against Gaia-X Registry

4. Generate DID Document for DID:WEB

5. Validate the DID Document

6. Generate Gaia-X Participant, Registration No., Terms and Conditions Credentials utilizing the GXDCH Notary Service

7. Create a Participant Verifiable Presentation and verify against GXDCH Compliance Service

8. Host Gaia-X Participant Credential

9. Manage and update your DID:WEB, Keys and Certificate(s) continuously. 

10. Start building service credentials and explore service composition to increase your trust level for services and applications.

   * 

🧪 Lab Team has finished implementing the conformity criteria and is currently testing them before release.





## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)
   * Gaia-X 101 Workshop: [https://gitlab.com/gaia-x/lab/workshops/gaia-x-101](https://gitlab.com/gaia-x/lab/workshops/gaia-x-101) \& [https://www.youtube.com/watch?v=xHaBM-T2--k](https://www.youtube.com/watch?v=xHaBM-T2--k)
   * Data Space Implementation Forums: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)