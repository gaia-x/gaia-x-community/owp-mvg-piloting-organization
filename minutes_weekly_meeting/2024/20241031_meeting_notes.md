
# Gaia-X OSS Community - October 31st

Next call: Thursday, November 5th, 2024, 09:00 - 09:45 CEST, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 



Moderation: Ewann Gavard

Minutes of meeting : collective



### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

Notes:  [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes_weekly_meeting/2024?ref_type=heads](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes_weekly_meeting/2024?ref_type=heads)

Acceptance: yes



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
3. Introduction of new and existing participants

4. Interesting sessions, events, and news

5. Update on the Loire Release

6. Updates from the lab, lighthouses, projects and developers



Number of participants: 42



### Introduction of new participants

- name, affiliation, project



### Interesting sessions, events, and news

   * ✨ **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * **Data Space Implementation Forums**
       * Regular Online Meetings for Reference Implementations along the DSSC Building Blocks
       * Registration Link: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)
   * **Launch/Introduction of the Danish Gaia-X Hub**
       * November 14, 2024
       * [https://digst.dk/digital-transformation/dansk-data-space-forum/konference-dansk-data-space-forum/](https://digst.dk/digital-transformation/dansk-data-space-forum/konference-dansk-data-space-forum/)
   * **Gaia-X Summit 24**
       * Location: Helsinki, Finland
       * Date: 14, 15 November 2024
       * [https://gaia-x.eu/summit-2024/](https://gaia-x.eu/summit-2024/)
       * Main Stage / Tech Theater / Q\&A sessions
   * **Launch of Gaia-X Hub Norway**
       * more details to be shared, virtual 
   * **Gaia-X Hub Germany Roadshow **(in german)
       * November 19, Hof
       * Planen, Bauen, Betreiben – mit Daten zum Erfolg in der Baubranche (Anmeldung ca. Ende September möglich)
   * **XFSC Final Tech Workshop**
       * Date: November 28,29
       * Location: Av. Complutense, 30, Moncloa – Aravaca, 28040 Madrid, Spain (✨ New location)
       * Registration: [https://www.gxfs.eu/xfsc-final-tech-workshop/](https://www.gxfs.eu/xfsc-final-tech-workshop/)
   * **Open Source eXPerience**
       * Dec 4+5, Paris
       * [https://www.opensource-experience.com/en/](https://www.opensource-experience.com/en/)
   * **FOSDEM 2025**
       *  1 \& 2 February, 2025, Brussels
       * [https://fosdem.org/2025/](https://fosdem.org/2025/)


### Update on the Loire Release

   * Based on Compliance 24.06 and Architecture Document 24.04 and ICAM Document 24.07.
   * Introduction of new technical requirements
   * [https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/blob/development/docs/labelling-criteria.md?ref\_type=heads)


### Updates from the Community, Developers, Lighthouses and Lab

   * 

**Community Recap: 10 steps to achieve Gaia-X Compliance / Basic Conformity for Participant and Service Credentials and why it matters.**

1. Generate and manage private (RSA) keys

2. Acquire X.509 from Gaia-X accredited TSP and transform to match Gaia-X specs

3. Host X.509 and validate against Gaia-X Registry

4. Generate DID Document for DID:WEB

5. Validate the DID Document

6. Generate Gaia-X Participant, Registration No., Terms and Conditions Credentials utilizing the GXDCH Notary Service

7. Create a Participant Verifiable Presentation and verify against GXDCH Compliance Service

8. Host Gaia-X Participant Credential

9. Manage and update your DID:WEB, Keys and Certificate(s) continuously. 

10. Start building service credentials and explore service composition to increase your trust level for services and applications.



Extra lap(s) for service providers:

11. Plan or launch service (depends on how deterministic your system is)

12. Gather the necessary information for your service

13. Gather or describe credentials from dependencies and aggregations

14. Compile all Verifiable Credentials into a Service VP

15. Expose for potential users and DD

16. Maintain!



Small update from Pontus-X here: In the catalogue item view, you will now find a service credential generation support: [https://portal.pontus-x.eu/asset/did:op:be4b6c502d9e9126a39f16ae24fd6612091e987e03bfdd99b16a5924aad81d77](https://portal.pontus-x.eu/asset/did:op:be4b6c502d9e9126a39f16ae24fd6612091e987e03bfdd99b16a5924aad81d77) that allows preparation of a service credential based on a running service.



Lab: Fix on EV SSL on Tagus. Some root CA certificates are marked as non-EV

   * 



🤝 An Gaia-X OpenID for Verifiable Credentials profile is being built with a proof-of-concept implementation to allow secure VC/VP exchanges.





## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)
   * Gaia-X 101 Workshop: [https://gitlab.com/gaia-x/lab/workshops/gaia-x-101](https://gitlab.com/gaia-x/lab/workshops/gaia-x-101) \& [https://www.youtube.com/watch?v=xHaBM-T2--k](https://www.youtube.com/watch?v=xHaBM-T2--k)
   * Data Space Implementation Forums: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)























