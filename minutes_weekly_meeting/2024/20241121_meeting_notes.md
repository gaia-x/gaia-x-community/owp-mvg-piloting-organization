
# Gaia-X OSS Community - November 21 th

Next call: Thursday, November 28th, 2024, 09:00 - 09:45 CET, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 



Moderation: Kai Meinke

Minutes of meeting : collective



### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

Notes:  [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20241107\_meeting\_notes.md]([]https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20241[]107[]\_meeting\_notes.md[])

Acceptance: yes



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
3. Introduction of new and existing participants

4. Interesting sessions, events, and news

5. Gaia-X OID4VC Demonstration

6. Updates from the lab, lighthouses, projects and developers



Number of participants: 51



### Introduction of new participants

- Nicolo Brandizzi, Fraunhofer IAIS



### Interesting sessions, events, and news

   * **VDMA Impulse Study on Tokenization in the Data Economy and Industry 4.0 released.**
       * With 3,600 members, the VDMA is the largest network organization and an important voice for the machinery and equipment manufacturing industry in Germany and Europe.
       * Henrik Schunk, Chairman of the Board of Directors of Schunk SE \& Co. KG and Chairman of the Board of Trustees of the IMPULS Foundation: “In the future, we will trade with industrial data and generate countless added values. Tokens, blockchain and smart contracts are indispensable technological prerequisites for the scaling data economy.”
       * Press Release: [https://www.vdma.org/viewer/-/v2article/render/133076295](https://www.vdma.org/viewer/-/v2article/render/133076295)
       * The Study (German): [https://www.vdma.org/documents/34570/4802302/24-11-20+TIM+Studie.pdf/156e09df-3851-7f35-76fd-e583020e5bba?t=1732090580644](https://www.vdma.org/documents/34570/4802302/24-11-20+TIM+Studie.pdf/156e09df-3851-7f35-76fd-e583020e5bba?t=1732090580644)
       * Features Gaia-X Lighthouse Projects EuProGigant and COOPERANTS, based on Gaia-X, Pontus-X and Ocean Enterprise
       * Main drivers are efficiency, interoperability, transparency, scalability, reducing cost of coordination and incentive systems for participants


   * **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * **Data Space Implementation Forums**
       * Regular Online Meetings for Reference Implementations along the DSSC Building Blocks
       * Registration Link: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)
   * **Launch of Gaia-X Hub Norway**
       * more details to be shared, virtual 
   * **German Gaia-X Projects Expo, Berlin**
       * November 26-27
       * 11 German BMWK-funded projects will showcase their Gaia-X implementation projects and Gaia-X compliance across domains
       * List of projects: [https://www.bundesnetzagentur.de/SharedDocs/Downloads/DE/Sachgebiete/Digitales/GAIAX/Gewinnerskizzen.pdf?\_\_blob=publicationFile\&v=1](https://www.bundesnetzagentur.de/SharedDocs/Downloads/DE/Sachgebiete/Digitales/GAIAX/Gewinnerskizzen.pdf?\_\_blob=publicationFile\&v=1)
       * Autowerkstatt 4.0 (Automotive), COOPERANTS (Aviation and Space), EuroDat (Data Space Intermediary), Health-X (Health), iECO (Construction), Marispace-X (Martitime), MERLOT (Education), OpenGPT-X (AI), Possible, Team-X (Health), Tellus (Infrastructure).
       * **Please be aware that the Gaia-X Lab Compliance Service is NOT a Gaia-X compliant issuer of Compliance Credentials and Registration Number Credentials (Notary Service)! See: **[https://docs.gaia-x.eu/framework/?tab=clearing-house](**https://docs.gaia-x.eu/framework/?tab=clearing-house**)
   * **XFSC Final Tech Workshop**
       * Date: November 28,29
       * Location: Av. Complutense, 30, Moncloa – Aravaca, 28040 Madrid, Spain (✨ New location)
       * Registration: [https://www.gxfs.eu/xfsc-final-tech-workshop/](https://www.gxfs.eu/xfsc-final-tech-workshop/)
   * **Gaia-X Spain Summit and Hackathon**
       * Date: December 2-3 (Hackathon) 3-4 (Summit)
       * Location: Matadero, Plaza de Legazpi, 8, Madrid (Spain).
       * Registration:[https://www.gaiax-spain.com/primera-cumbre-de-espacios-de-datos-gaia-x-espana/](https://www.gaiax-spain.com/primera-cumbre-de-espacios-de-datos-gaia-x-espana/)
   * **Open Source eXPerience**
       * Dec 4+5, Paris
       * [https://www.opensource-experience.com/en/](https://www.opensource-experience.com/en/)
   * **FOSDEM 2025**
       *  1 \& 2 February, 2025, Brussels
       * [https://fosdem.org/2025/](https://fosdem.org/2025/)


### Gaia-X OID4VC Demonstration

Proof-of-concept implementation of the OID4VC (OID4VCI \& OID4VP) protocols in a Gaia-X-like setting.



⚠️ **Not for production use **⚠️



Source code : [https://gitlab.com/gaia-x/gaia-x-community/openid-for-verifiable-credentials/](https://gitlab.com/gaia-x/gaia-x-community/openid-for-verifiable-credentials/)

Presentation : [https://gaia-x.eu/summit-2024/wp-content/uploads/2024/11/Summit-2024-PPT\_TechTheatre\_Master\_Day1.pdf](https://gaia-x.eu/summit-2024/wp-content/uploads/2024/11/Summit-2024-PPT\_TechTheatre\_Master\_Day1.pdf) (page 54)



Youtube video : Coming soon...

Opening Ceremony: [https://www.youtube.com/watch?v=1asg5f7qf6w](https://www.youtube.com/watch?v=1asg5f7qf6w)



### Updates from the Community, Developers, Lighthouses and Lab

   * 

**Community Recap: 10 steps to achieve Gaia-X Compliance / Basic Conformity for Participant and Service Credentials and why it matters.**

1. Generate and manage private (RSA) keys

2. Acquire X.509 from Gaia-X accredited TSP and transform to match Gaia-X specs

3. Host X.509 and validate against Gaia-X Registry

4. Generate DID Document for DID:WEB

5. Validate the DID Document

6. Generate Gaia-X Participant, Registration No., Terms and Conditions Credentials utilizing the GXDCH Notary Service

7. Create a Participant Verifiable Presentation and verify against GXDCH Compliance Service

8. Host Gaia-X Participant Credential

9. Manage and update your DID:WEB, Keys and Certificate(s) continuously. 

10. Start building service credentials and explore service composition to increase your trust level for services and applications.



Extra lap(s) for service providers:

11. Plan or launch service (depends on how deterministic your system is)

12. Gather the necessary information for your service

13. Gather or describe credentials from dependencies and aggregations

14. Compile all Verifiable Credentials into a Service VP

15. Expose for potential users and DD

16. Maintain!



## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)
   * Gaia-X 101 Workshop: [https://gitlab.com/gaia-x/lab/workshops/gaia-x-101](https://gitlab.com/gaia-x/lab/workshops/gaia-x-101) \& [https://www.youtube.com/watch?v=xHaBM-T2--k](https://www.youtube.com/watch?v=xHaBM-T2--k)
   * Data Space Implementation Forums: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)