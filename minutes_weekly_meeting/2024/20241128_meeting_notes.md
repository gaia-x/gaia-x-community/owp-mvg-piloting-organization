
# Gaia-X OSS Community - November 28 th

Next call: Thursday, December 5, 2024, 09:00 - 09:45 CET, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 



Moderation: Kai Meinke

Minutes of meeting : collective



### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

Notes:  [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20241121\_meeting\_notes.md]([]https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20241[]121[]\_meeting\_notes.md[])

Acceptance: yes



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
3. Introduction of new and existing participants

4. Interesting sessions, events, and news

5. Updates from the lab, lighthouses, projects and developers

6. Discussion regarding policy reasoning during contracts



Number of participants: 34



### Introduction of new participants

- name, institution



### Interesting sessions, events, and news

   * **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * **Gaia-X Summit24 **videos are online, including the tech theater: [https://gaia-x.eu/summit-2024/videos/](https://gaia-x.eu/summit-2024/videos/)
   * **Data Space Implementation Forums**
       * Regular Online Meetings for Reference Implementations along the DSSC Building Blocks
       * Registration Link: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)
   * **XFSC Final Tech Workshop**
       * Date: November 28,29
       * Location: Av. Complutense, 30, Moncloa – Aravaca, 28040 Madrid, Spain (✨ New location)
       * Registration: [https://www.gxfs.eu/xfsc-final-tech-workshop/](https://www.gxfs.eu/xfsc-final-tech-workshop/)
   * **VDMA Impulse Study on Tokenization in the Data Economy and Industry 4.0 release**
       * Date: November 28, 10:00-12:00
       * Release Meeting Registration: [https://www.vdma.org/events-trade-fairs/-/event/view/104262](https://www.vdma.org/events-trade-fairs/-/event/view/104262)
       * Press Release: [https://www.vdma.org/viewer/-/v2article/render/133076295](https://www.vdma.org/viewer/-/v2article/render/133076295)
       * The Study (German): [https://www.vdma.org/documents/34570/4802302/24-11-20+TIM+Studie.pdf/156e09df-3851-7f35-76fd-e583020e5bba?t=1732090580644](https://www.vdma.org/documents/34570/4802302/24-11-20+TIM+Studie.pdf/156e09df-3851-7f35-76fd-e583020e5bba?t=1732090580644)
       * Features Gaia-X Lighthouse Projects EuProGigant and COOPERANTS, based on Gaia-X, Pontus-X and Ocean Enterprise
       * Main drivers are efficiency, interoperability, transparency, scalability, reducing cost of coordination and incentive systems for participants
   * **Pontus-X Open Community Call**
       * Date: November 28, 13:00 - 14:00
       * Free to attend, just reach out to us.
       * Updates on Tech, Releases, new Gaia-X implementations and demonstration of Gaia-X integrated use cases from lighthouse projects / manufacturing / aerospace.
   * **Gaia-X Spain Summit and Hackathon**
       * Date: December 2-3 (Hackathon) 3-4 (Summit)
       * Location: Matadero, Plaza de Legazpi, 8, Madrid (Spain).
       * Registration:[https://www.gaiax-spain.com/primera-cumbre-de-espacios-de-datos-gaia-x-espana/](https://www.gaiax-spain.com/primera-cumbre-de-espacios-de-datos-gaia-x-espana/)
   * **Open Source eXPerience**
       * Dec 4+5, Paris
       * [https://www.opensource-experience.com/en/](https://www.opensource-experience.com/en/)
   * **FOSDEM 2025**
       *  1 \& 2 February, 2025, Brussels
       * [https://fosdem.org/2025/](https://fosdem.org/2025/)




### Updates from the Community, Developers, Lighthouses and Lab

tbd: Is the Data Space Protocol (DSP) and the DCP abandoning OIDC as the European Standard for Credential Exchange? [https://github.com/eclipse-dataspace-protocol-base/DataspaceProtocol/pull/63](https://github.com/eclipse-dataspace-protocol-base/DataspaceProtocol/pull/63)

- Will this be breaking interoperability between DSP and Gaia-X, EUDI, EBSI, Dome, Simpl initiatives on the most fundamental level?

- "DCP is *the* protocol for *decentralized identity* in the EDWG. DSP has *no concept of human interactions* and they do not enter into the specification." OIDC is currently blocked from DCP/DSP integration here as it seems.

   * 

**Questions from Robert Schubert regarding Loire**

**1. Registry**

How to find a running registry having already the 2404 vocabulary (Loire)?

I tried 

- [https://registry.gaia-x.eu/v1/docs](https://registry.gaia-x.eu/v1/docs) --> has 2210 as expected

- [https://registry.gaia-x.eu/v2/docs](https://registry.gaia-x.eu/v2/docs) --> HTTP 503 **=> Load balancer not updated yet as Loire is not General Availability (GXDCH providers are testing/deploying)**

   * - [https://registry.lab.gaia-x.eu/v2/docs/](https://registry.lab.gaia-x.eu/v2/docs/) --> HTTP 503 **=> Ontology was recently released, use version 2406 **
   * - [https://registry.lab.gaia-x.eu/v2/owl/2406](https://registry.lab.gaia-x.eu/v2/owl/2406)
   * - [https://registry.lab.gaia-x.eu/v2/shapes/2406](https://registry.lab.gaia-x.eu/v2/shapes/2406)
   * - [https://registry.lab.gaia-x.eu/v2/schemas/2406](https://registry.lab.gaia-x.eu/v2/schemas/2406)
TBD by the lab: replace the example in the Swagger with 2406: [https://gitlab.com/gaia-x/lab/compliance/gx-registry/-/issues/59](https://gitlab.com/gaia-x/lab/compliance/gx-registry/-/issues/59)



Alternatives: (not yet done, but will be added to [https://registry.lab.gaia-x.eu/v2/api/trusted-issuers](https://registry.lab.gaia-x.eu/v2/api/trusted-issuers) soon)

- Registry Service: [https://www.delta-dao.com/registry/v2/](https://www.delta-dao.com/registry/v2/)

- Compliance Service: [https://www.delta-dao.com/compliance/v2/](https://www.delta-dao.com/compliance/v2/)

- Notary Service: [https://www.delta-dao.com/notary/v2/](https://www.delta-dao.com/notary/v2/)

   * [https://gx-compliance.gxdch.dih.telekom.com/v2/docs](https://gx-compliance.gxdch.dih.telekom.com/v2/docs)
   * [https://gx-registry.gxdch.dih.telekom.com/v2/docs](https://gx-registry.gxdch.dih.telekom.com/v2/docs)
   * [https://gx-notary.gxdch.dih.telekom.com/v2/docs](https://gx-notary.gxdch.dih.telekom.com/v2/docs)


Arsys Test Environment

   * [https://compliance.arlabdevelopments.com/v2/docs](https://compliance.arlabdevelopments.com/v2/docs)
   * [https://registry.arlabdevelopments.com/v2/docs](https://registry.arlabdevelopments.com/v2/docs)
   * [https://registrationnumber.notary.arlabdevelopments.com/v2/docs/](https://registrationnumber.notary.arlabdevelopments.com/v2/docs/)




**2. Compliance**

For compliance service I found the one matching to 2404:

[https://compliance.lab.gaia-x.eu/v2/docs](https://compliance.lab.gaia-x.eu/v2/docs)

But:

- [https://compliance.gaia-x.eu/v2/docs](https://compliance.gaia-x.eu/v2/docs) --> 404  **=> Load balancer not updated yet as Loire is not General Availability (GXDCH providers are testing/deploying)**

- [https://compliance.gaia-x.eu/v1/docs](https://compliance.gaia-x.eu/v1/docs) --> has 2210 

Did I understand it right that the new compliance service has multiple endpoints which expect the JWT as *request body* with request-header "application/vp+ld+json+jwt"?

Which library did you use to create / sign the JWT with encapsulated VCs with ID field containing another JWT?

{

      "@context": "[https://www.w3.org/ns/credentials/v2](https://www.w3.org/ns/credentials/v2)",

      "id": "data:application/vc+ld+json+jwt;eyJhbGc.....zxZuMA...

      "type": "EnvelopedVerifiableCredential"

    },

**=> Indeed, we switch to VC-JWT (**[https://docs.gaia-x.eu/technical-committee/identity-credential-access-management/24.07/credential\_format/#verifiable-credential)](**https://docs.gaia-x.eu/technical-committee/identity-credential-access-management/24.07/credential\_format/#verifiable-credential)**)** **

**=>** **The lab uses Jose internally to create JWT from credentials. No weird changes to perform when signing. Just send the VC to Jose :)**

=> [https://vc-jwt.io](https://vc-jwt.io) can help with this but it's a **non-official Gaia-X tool** which **isn't maintained by the official Lab Team**



**3. Wizard**

Will the wizard be extended to produce 

- LegalPerson (2404) rather than LegalParticipant (2210) when selecting "main (Loire)"?

- VPs as JWT

**=> We mentioned that we won't support Loire with the Wizard. The team is quite small and the Wizard is a huge work to maintain. Community is welcome to take over and/or provide new tools. **



**4. Compliance credentials in Loire**

In Tagus we used to use DID (which are resolvable) as ID field in the VCs/VPs. The fact that they are resolvable was mandatory for the compliance credential since the compliant VC is only referenced by its ID.

In the examples of the compliance server payload ([https://compliance.lab.gaia-x.eu/v2/docs)](https://compliance.lab.gaia-x.eu/v2/docs)) I can see that IDs are something like this:

  "@id": "ex:DevelopmentCycleSecurity\_44",

  wich is OK for demo purposes.

How will compliance credentials will look like in Loire? I was not able to execute the request in OpenApi since there is a signature falidation failure

**=> The signature verification failure is due to an issue in Swagger's example JWT payload. The JWT string should be posted raw without quotes. This is a limitation of Nest JS Swagger at the moment so you should remove the quotes from the example payload for the time being.**





Question to Ewann: Will the really useful gaia-x-101-workshop be updated to Loire?

**=> Not now. Quite a bit of work to create a Loire VP. **

   * 

**Community Recap: 10 steps to achieve Gaia-X Compliance / Basic Conformity for Participant and Service Credentials and why it matters.**

1. Generate and manage private (RSA) keys

2. Acquire X.509 from Gaia-X accredited TSP and transform to match Gaia-X specs

3. Host X.509 and validate against Gaia-X Registry

4. Generate DID Document for DID:WEB

5. Validate the DID Document

6. Generate Gaia-X Participant, Registration No., Terms and Conditions Credentials utilizing the GXDCH Notary Service

7. Create a Participant Verifiable Presentation and verify against GXDCH Compliance Service

8. Host Gaia-X Participant Credential

9. Manage and update your DID:WEB, Keys and Certificate(s) continuously. 

10. Start building service credentials and explore service composition to increase your trust level for services and applications.



Extra lap(s) for service providers:

11. Plan or launch service (depends on how deterministic your system is)

12. Gather the necessary information for your service

13. Gather or describe credentials from dependencies and aggregations

14. Compile all Verifiable Credentials into a Service VP

15. Expose for potential users and DD

16. Maintain!



## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)
   * Gaia-X 101 Workshop: [https://gitlab.com/gaia-x/lab/workshops/gaia-x-101](https://gitlab.com/gaia-x/lab/workshops/gaia-x-101)  --> to be updated to Loire? Would be really useful!
   * Developers page on the Gaia-X Website: [https://gaia-x.eu/developers/](https://gaia-x.eu/developers/) Including Gaia-X 101 course
   * Data Space Implementation Forums: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)