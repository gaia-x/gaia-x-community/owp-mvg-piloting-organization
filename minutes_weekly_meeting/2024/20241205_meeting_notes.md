
# Gaia-X OSS Community - December 5

Next call: Thursday, December 12, 2024, 09:00 - 09:45 CET, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_NGIwNjRiYzYtZDY4MC00Mzk4LWIyYWMtNjkwODU2ZmIzMzA4%40thread.v2/0?context=%7b%22Tid%22%3a%22ca16cd36-046b-4f65-89cf-6448895c6937%22%2c%22Oid%22%3a%22f90ecb4e-61b0-4bfb-a2e1-2da5a49cf500%22%7d)

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/2024) 

Moderation: Kai Meinke

Minutes of meeting : collective

### Competition and antitrust guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


### Acceptance of last meeting notes

Notes:  [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20241128\_meeting\_notes.md]([]https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2024/20241[]128[]\_meeting\_notes.md[])

Acceptance: yes



### Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
3. Introduction of new and existing participants

4. Interesting sessions, events, and news

5. Updates from the lab, lighthouses, projects and developers

6. Discussion regarding policy reasoning during contracts



Number of participants: 28



### Introduction of new participants

- name, institution



### Interesting sessions, events, and news

   * **Gaia-X Academy** is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * **Gaia-X Summit24 **videos are online, including the tech theater: [https://gaia-x.eu/summit-2024/videos/](https://gaia-x.eu/summit-2024/videos/)
   * **Open Source eXPerience**
       * Dec 4+5, Paris
       * [https://www.opensource-experience.com/en/](https://www.opensource-experience.com/en/)
   * **Simpl Community Day, on-site and remote attendance**
       * January 30, 2025
       * Brussels
       * [https://simpl-programme.ec.europa.eu/event/simpl-annual-event](https://simpl-programme.ec.europa.eu/event/simpl-annual-event)
       * [https://simplannualevent.uevent.eu/](https://simplannualevent.uevent.eu/)
       * MVP showcase expected
   * **FOSDEM 2025**
       *  1 \& 2 February, 2025, Brussels
       * [https://fosdem.org/2025/](https://fosdem.org/2025/)




### Updates from the Community, Developers, Lighthouses and Lab

**DSP and DCP and OpenID?**

tbd: Will the DSP \& DCP support OpenID for Credential Issuance and Exchange?

   * Discussion in the EDSWG ongoing


**Launch of the cross-data space Dataset Search Engine "daseen" by German BMDV** last week:

   * PR (DE): [https://mission-ki.de/de/bessere-vernetzung-des-datenoekosystems-mission-ki-entwickelt-neuartige-datensatz-suchmaschine](https://mission-ki.de/de/bessere-vernetzung-des-datenoekosystems-mission-ki-entwickelt-neuartige-datensatz-suchmaschine)
   * PR (EN): [https://mission-ki.de/en/enhancing-data-ecosystem-connectivity-mission-ki-develops-innovative-dataset-search-engine](https://mission-ki.de/en/enhancing-data-ecosystem-connectivity-mission-ki-develops-innovative-dataset-search-engine)
   * Link: [https://daseen.de/](https://daseen.de/)
   * Aims to enable data search for AI training and smart services across data spaces and national access points. deltaDAO and beebucket will enable the first connection to Gaia-X Ecosystems through Pontus-X taking Gaia-X Credentials into account.
   * The search engine also allows data quality metrics - extended data profiles (EDPS) to be generated on public open data AND on private data through Compute-to-Data (launch end of December/January 25, depending on testing). 
   * Metadata and license information is automatically processed and terms and conditions analyzed by AI to assess the legal status and availability for different processing operations.
   * 

**XFSC Final Tech Workshop**

   * Specification Phase 2 results have been presented last week
   * Project ending now in December 2024
   * Catalogue and Workflow presentations have been shared.
   * Where can the reference implementation of the catalogue be found?
   * Slides to be shared with the Community once available. The XFSC team will publish them in the near future, most probably here: [https://gitlab.eclipse.org/eclipse/xfsc/workshop](https://gitlab.eclipse.org/eclipse/xfsc/workshop)


**Community Recap: 10 steps to create Gaia-X compliant Participant and Service Credentials and why it matters.**

1. Generate and manage private (RSA) keys

2. Acquire X.509 from Gaia-X accredited TSP and transform to match Gaia-X specs

3. Host X.509 and validate against Gaia-X Registry

4. Generate DID Document for DID:WEB

5. Validate the DID Document

6. Generate Gaia-X Participant, Registration No., Terms and Conditions Credentials utilizing the GXDCH Notary Service

7. Create a Participant Verifiable Presentation and verify against GXDCH Compliance Service

8. Host Gaia-X Participant Credential

9. Manage and update your DID:WEB, Keys and Certificate(s) continuously. 

10. Start building service credentials and explore service composition to increase your trust level for services and applications.



Extra lap(s) for service providers:

11. Plan or launch service (depends on how deterministic your system is)

12. Gather the necessary information for your service

13. Gather or describe credentials from dependencies and aggregations

14. Compile all Verifiable Credentials into a Service VP

15. Expose for potential users and DD

16. Maintain!



## Useful resources for newcomers

   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)
   * Gaia-X 101 Workshop: [https://gitlab.com/gaia-x/lab/workshops/gaia-x-101](https://gitlab.com/gaia-x/lab/workshops/gaia-x-101)  --> to be updated to Loire? Would be really useful!
   * Developers page on the Gaia-X Website: [https://gaia-x.eu/developers/](https://gaia-x.eu/developers/) Including Gaia-X 101 course
   * Data Space Implementation Forums: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)