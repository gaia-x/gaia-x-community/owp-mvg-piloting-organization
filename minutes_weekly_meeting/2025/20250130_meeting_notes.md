
# Gaia-X OSS Community - January 30

Next call: Thursday, February 6, 2025, 09:00 - 09:45 CET, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_MzdkNDQ4ZTktOWY1NC00MzFhLWEwZDItYjhiY2I0N2VlOGYy%40thread.v2/0?context=%7b%22Tid%22%3a%225845e7c0-703b-46a4-88c5-02cbecfbb49f%22%2c%22Oid%22%3a%22af8c7a56-05ca-473d-8b91-c15d12668067%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_MzdkNDQ4ZTktOWY1NC00MzFhLWEwZDItYjhiY2I0N2VlOGYy%40thread.v2/0?context=%7b%22Tid%22%3a%225845e7c0-703b-46a4-88c5-02cbecfbb49f%22%2c%22Oid%22%3a%22af8c7a56-05ca-473d-8b91-c15d12668067%22%7d)



Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/)



Moderation: Ewann

Minutes of meeting : collective



## Competition and Antitrust Guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


## Acceptance of last meeting notes

Notes: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2025/20250123\_meeting\_notes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2025/20250123\_meeting\_notes.md)

Acceptance: yes



## Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
3. Introduction of new and existing participants

4. Interesting sessions, events, and news

5. Updates from the lab, lighthouses, projects and developers



Number of participants: 25



### Introduction of new participants

   * name, institution


## Interesting sessions, events, and news

   * **Simpl Community Day, on-site and remote attendance**
       * January 30, 2025, Brussels, registration and selection of workshop required
       * [https://simpl-programme.ec.europa.eu/event/simpl-annual-event](https://simpl-programme.ec.europa.eu/event/simpl-annual-event)
       * [https://simplannualevent.uevent.eu/](https://simplannualevent.uevent.eu/)
       * MVP showcase expected
   * **FOSDEM 2025**
       * 1 \\& 2 February, 2025, Brussels
       * [https://fosdem.org/2025/](https://fosdem.org/2025/)
   * **Data Sharing Festival**
       * February 4-5, The Hague, Netherlands
       * [https://data-sharing-festival.net/program](https://data-sharing-festival.net/program)
       * Gaia-X Speakers day 1 (Ulrich Ahle, Christoph Strnadl, Ewann Gavard)
   * **Gaia-X DSBC Workshop**
       * February 5-6, , The Hague, Netherlands
       * Focus on Lighthouses, Roadmap and Alignment 2025+
   * **Pontus-X Initial Production Go-Live**
       * February 24, virtual
       * There will be a virtual Summit around the Go-Live event.
       * [https://pontus-x.eu](https://pontus-x.eu)
   * **Data Spaces Symposium 2025**
       * March 11-12, Warsaw, Poland
       * [https://www.data-spaces-symposium.eu/](https://www.data-spaces-symposium.eu/)
   * **Hannover Fair 2025**
       * March 31 - April 4, Hannover, Germany
       * [https://www.hannovermesse.de/en/](https://www.hannovermesse.de/en/)






## Updates from the Community, Lighthouses and Lab

🎯 A general Gaia-X roadmap is being designed with input from each Gaia-X team (CTO, COO, CEO, ...) and will be published soon. This exercise will be conducted in an outside-in fashion, that is, we will listen carefully to the **input of all our external stakeholders** -- and that very much includes this **OSS community**. Haven't decided yet how to exactly implement the project (structure, etc.) but maybe we can use this forum for collecting your input.



👷‍♂️ At the moment, the Lab Team is mostly working on house-keeping and maintenance tasks on multiple repositories. Also contribution are being done to Open Source projects the Gaia-X ecosystem uses to enable new features in a near future.

Lab: Batch of fixes on Compliance engine (Invalid Data URI separator, bump to last MIME type for Data URI, Issuer validation fix)



📚 Two new courses have been published on the Gaia-X Academy : 

    - 11 - Gaia-X Compliance Overview : What is the compliance ? (Business \& legal explanation)

    - 12 - Gaia-X Compliance Technical Focus: How is the compliance checked ?



📚 Gaia-X Docs updated [https://docs.gaia-x.eu/](https://docs.gaia-x.eu/) with new v1+v2 list of GXDCH



Arthur \& Ewann: Now is a amazing moment to give feedback on the community requirements and needs for software, documentation and any other things that can help the adoption as we are building the roadmap and these feedback will help prioritize topics



[Feel free to add your contributions here]





## Useful resources for newcomers

   * Gaia-X Academy is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * How to become a Gaia-X Conformant Service
       * [https://gaia-x.eu/wp-content/uploads/2024/12/How-to-become-a-Gaia-X-Conformant-Service\_Tagus-Release.pdf](https://gaia-x.eu/wp-content/uploads/2024/12/How-to-become-a-Gaia-X-Conformant-Service\_Tagus-Release.pdf)
   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Digital Clearing Houses: [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)
   * Gaia-X 101 Workshop: [https://gitlab.com/gaia-x/lab/workshops/gaia-x-101](https://gitlab.com/gaia-x/lab/workshops/gaia-x-101) --> to be updated to Loire? Would be really useful!
   * Developers page on the Gaia-X Website: [https://gaia-x.eu/developers/](https://gaia-x.eu/developers/) Including Gaia-X 101 course
   * Data Space Implementation Forums: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)

