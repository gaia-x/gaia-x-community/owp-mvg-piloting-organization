
# Gaia-X OSS Community - February 20

Next call: Thursday, February 27, 2025, 09:00 - 09:45 CET, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_MzdkNDQ4ZTktOWY1NC00MzFhLWEwZDItYjhiY2I0N2VlOGYy%40thread.v2/0?context=%7b%22Tid%22%3a%225845e7c0-703b-46a4-88c5-02cbecfbb49f%22%2c%22Oid%22%3a%22af8c7a56-05ca-473d-8b91-c15d12668067%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_MzdkNDQ4ZTktOWY1NC00MzFhLWEwZDItYjhiY2I0N2VlOGYy%40thread.v2/0?context=%7b%22Tid%22%3a%225845e7c0-703b-46a4-88c5-02cbecfbb49f%22%2c%22Oid%22%3a%22af8c7a56-05ca-473d-8b91-c15d12668067%22%7d) 

Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/)

Moderation: Kai Meinke

Minutes of meeting : collective.

## Competition and Antitrust Guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


## Acceptance of last meeting notes

Notes: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2025/20250213\_meeting\_notes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2025/20250213\_meeting\_notes.md)

Acceptance: yes

## Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
3. Introduction of new and existing participants

4. Gaia-X Evangelists News (Christoph)

5. Interesting sessions, events, and news

6. Updates from the lab, lighthouses, projects and developers

Number of participants: 29

### Introduction of new participants

   * name, project, affil.

## Interesting sessions, events, and news

   * **New: Press Review February 2024**
       * [https://gaia-x.eu/wp-content/uploads/2025/02/11-17-February-2025.pdf](https://gaia-x.eu/wp-content/uploads/2025/02/11-17-February-2025.pdf)
   * **New: Gaia-X Members Webinar**
       * February 20, virtual, 11:00 - 12:00 CET
       * [https://gaia-x.eu/event/members-webinar/](https://gaia-x.eu/event/members-webinar/)
   * **New: Gaia-X Smart Cities Ecosystem Quarterly Meeting**
       * February 20, virtual, 13:30 - 15:00 CET
       * [https://gaia-x.eu/event/gaia-x-smart-cities-ecosystem-quarterly-meeting/](https://gaia-x.eu/event/gaia-x-smart-cities-ecosystem-quarterly-meeting/)
   * **New: DSSC Insight Series: Road to the Data Spaces Symposium 2025**
       * February 21, virtual, 13:00 - 14:00 CET
       * [https://gaia-x.eu/event/dssc-insight-series-road-to-the-data-spaces-symposium-2025/](https://gaia-x.eu/event/dssc-insight-series-road-to-the-data-spaces-symposium-2025/)
   * **Pontus-X Go-Live \& Virtual Summit**
       * February 24-27, virtual
       * Virtual Summit around the Go-Live event to be announced, open for all community members
       * [https://pontus-x.eu](https://pontus-x.eu)
   * **2nd Japan Future Data Meeting**
       * February 27, On-site only, Japanese Language only
       * Session expected to be published in Youtube. (1st Meeting 2024 is available on YT)
       * Event by Japan IPA \& DSA
       * [https://www.ipa.go.jp/event/2024/data20250227.html](https://www.ipa.go.jp/event/2024/data20250227.html)
   * **Data Spaces Symposium 2025**
       * March 11-12, Warsaw, Poland
       * [https://gaia-x.eu/event/data-spaces-symposium-2025-share-data-unlock-value-boost-impact/](https://gaia-x.eu/event/data-spaces-symposium-2025-share-data-unlock-value-boost-impact/)
       * [https://www.data-spaces-symposium.eu/](https://www.data-spaces-symposium.eu/)
   * **New: European Parliament Reception**
       * March 18, 2025, Brussels, Belgium
       * [https://gaia-x.eu/event/gaia-x-european-parliament-reception-3/](https://gaia-x.eu/event/gaia-x-european-parliament-reception-3/)
   * **Hannover Fair 2025**
       * March 31 - April 4, Hannover, Germany
       * [https://www.hannovermesse.de/en/](https://www.hannovermesse.de/en/)
   * **EU-Japan Digital Week 2025**
       * March 31 - April 7, Tokyo, Japan
       * [https://inpacehub.eu/eu-japan-digitalweek-2025/](https://inpacehub.eu/eu-japan-digitalweek-2025/)
   * **Market-X \& Tech-X, 13-14 May 2025**
       * Valencia, Spain
       * Combined event
       * 2 days of business and tech talks
       * Hackathon #8 will take place there
   * **Extended Semantic Web Conference (ESWC 2025)**
       * June 1 - 5 in Portorož, Slovenia
       *  Workshop on Semantics in Dataspaces
       * Workshop on ODRL and Policy for Access and Usage Control

## Updates from the Community, Lighthouses and Lab

Next week demonstration by Tom Last on a Credential Wizard and Signing Tool for Gaia-X Credentials.

Also planned for next week: Initial Production Launch of Pontus-X Ecosystem, based on Gaia-X, Ocean Enterprise and OASIS Sapphire with COOPERANTS Gaia-X Lighthouse Project and Gaia-X participants from space and aerospace ecosystem. See: [https://portal.main.pontus-x.eu/](https://portal.main.pontus-x.eu/)

   * Participation in the ecosystem is open for all Gaia-X participants
   * Operation is the ecosystem is open for all Gaia-X participants


Fix on the notary v2 for EC keys support \& up-to-date MIME types for VC-JWT

Two new GXDCH to be announced publicly: Pfalzkom \& CISPE

## Useful resources for newcomers

   * Gaia-X Academy is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * How to become a Gaia-X Conformant Service
       * [https://gaia-x.eu/wp-content/uploads/2024/12/How-to-become-a-Gaia-X-Conformant-Service\_Tagus-Release.pdf](https://gaia-x.eu/wp-content/uploads/2024/12/How-to-become-a-Gaia-X-Conformant-Service\_Tagus-Release.pdf)
   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X RSS feeds [https://gaia-x.eu/rss-feeds/](https://gaia-x.eu/rss-feeds/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Digital Clearing Houses: [https://docs.gaia-x.eu/#/gxdch](https://docs.gaia-x.eu/#/gxdch)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)
   * Gaia-X 101 Workshop: [https://gitlab.com/gaia-x/lab/workshops/gaia-x-101](https://gitlab.com/gaia-x/lab/workshops/gaia-x-101) --> to be updated to Loire? Would be really useful!
   * Developers page on the Gaia-X Website: [https://gaia-x.eu/developers/](https://gaia-x.eu/developers/) Including Gaia-X 101 course
   * Data Space Implementation Forums: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)