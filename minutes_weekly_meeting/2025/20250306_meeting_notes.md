
# Gaia-X OSS Community - March 6

Next call: Thursday, March 13, 2025, 09:00 - 09:45 CET, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_MzdkNDQ4ZTktOWY1NC00MzFhLWEwZDItYjhiY2I0N2VlOGYy%40thread.v2/0?context=%7b%22Tid%22%3a%225845e7c0-703b-46a4-88c5-02cbecfbb49f%22%2c%22Oid%22%3a%22af8c7a56-05ca-473d-8b91-c15d12668067%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_MzdkNDQ4ZTktOWY1NC00MzFhLWEwZDItYjhiY2I0N2VlOGYy%40thread.v2/0?context=%7b%22Tid%22%3a%225845e7c0-703b-46a4-88c5-02cbecfbb49f%22%2c%22Oid%22%3a%22af8c7a56-05ca-473d-8b91-c15d12668067%22%7d) 



Last meeting notes always to be found here: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting/)



Moderation: Kai Meinke

Minutes of meeting : collective.



## Competition and Antitrust Guidelines

   1. no price-fixing
   1. no market or customer allocation
   1. no output restrictions
   1. no agreement on or exchange of competitively sensitive business information
   1. consequences of competition law infringement


## Acceptance of last meeting notes

Notes: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2025/20250227\_meeting\_notes.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/2025/20250227\_meeting\_notes.md)

Acceptance: yes



## Agenda

   1. Competition and antitrust guidelines
   1. Acceptance of last meeting notes
3. Introduction of new and existing participants

4. Interesting sessions, events, and news

5. Updates from the lab, lighthouses, projects and developers



Number of participants: 34



### Introduction of new participants

   * name, project, affil.


## Interesting sessions, events, and news

   * **Data Spaces Symposium 2025**
       * March 11-12, Warsaw, Poland
       * [https://gaia-x.eu/event/data-spaces-symposium-2025-share-data-unlock-value-boost-impact/](https://gaia-x.eu/event/data-spaces-symposium-2025-share-data-unlock-value-boost-impact/)
       * [https://www.data-spaces-symposium.eu/](https://www.data-spaces-symposium.eu/)
   * **European Parliament Reception**
       * March 18, 2025, Brussels, Belgium
       * [https://gaia-x.eu/event/gaia-x-european-parliament-reception-3/](https://gaia-x.eu/event/gaia-x-european-parliament-reception-3/)
   * **Hannover Fair 2025**
       * March 31 - April 4, Hannover, Germany
       * [https://www.hannovermesse.de/en/](https://www.hannovermesse.de/en/)
   * **EU-Japan Digital Week 2025**
       * March 31 - April 7, Tokyo, Japan
       * [https://inpacehub.eu/eu-japan-digitalweek-2025/](https://inpacehub.eu/eu-japan-digitalweek-2025/)
   * **Market-X \& Tech-X, 13-14 May 2025**
       * Valencia, Spain
       * Combined event
       * 2 days of business and tech talks
       * Hackathon #8 will take place there
   * **Extended Semantic Web Conference (ESWC 2025)**
       * June 1 - 5 in Portorož, Slovenia
       * Workshop on Semantics in Dataspaces
       * Workshop on ODRL and Policy for Access and Usage Control
   * **Gaia-X Hub Norway Event**
       * November 2025
   * **Gaia-X Summit 2025**
       * Porto, Portugal
       * November 20-21
       * [https://gaia-x.eu/event/gaia-x-summit-2025/](https://gaia-x.eu/event/gaia-x-summit-2025/)




## Pontus-X Initial Production Launch

Initial Production Launch of Pontus-X Ecosystem, based on Gaia-X, Ocean Enterprise and OASIS Sapphire with COOPERANTS Gaia-X Lighthouse Project and Gaia-X participants from space and aerospace ecosystem. See: [https://portal.main.pontus-x.eu/](https://portal.main.pontus-x.eu/)

   * Participation in the ecosystem is open for all Gaia-X participants, Gaia-X Identity required
   * Currently support for Trust Framework 22.10, Full Loire support will follow when the OIDV4VP Implementation has been tested and is mature enough.
   * Operation is the ecosystem is open for all Gaia-X participants
   * Federation of all services enabled to not have a centralized operating company and to support scalability
   * Ramp up phase until Summer 2025 when e-money integration shall be completed
   * Not perfect yet, so we appreciate constructive feedback from the community as always




## Updates from the Community, Lighthouses and Lab



New Gaia-X Digital Clearing Houses have been listed: [https://docs.gaia-x.eu/#/gxdch](https://docs.gaia-x.eu/#/gxdch)



EuroStack is getting more traction and Gaia-X is certainly an initiative at the core of it.

   * [https://euro-stack.eu/](https://euro-stack.eu/)
   * [https://www.euro-stack.info/](https://www.euro-stack.info/)
   * [https://european-alternatives.eu/categories](https://european-alternatives.eu/categories)


EDC Release now based on DCP

[https://github.com/eclipse-edc/MinimumViableDataspace](https://github.com/eclipse-edc/MinimumViableDataspace)

The Decentralized Claims Protocol defines a secure way how to participants in a dataspace can exchange and present credential information. In particular, the DCP specification defines the *Presentation Flow*, which is the process of requesting, presenting and verifying Verifiable Credentials.



Don't trust. Verify.



🎉 The verifiable credential playground (vc-jwt.io) source code is now part of the Gaia-X Community repository and now provides an easy deployment Helm chart. 



[https://gitlab.com/gaia-x/gaia-x-community/vc-jwt.io](https://gitlab.com/gaia-x/gaia-x-community/vc-jwt.io) 



## Useful resources for newcomers

   * Gaia-X Academy is now open for everyone: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
       * First certification courses are available
   * How to become a Gaia-X Conformant Service
       * [https://gaia-x.eu/wp-content/uploads/2024/12/How-to-become-a-Gaia-X-Conformant-Service\_Tagus-Release.pdf](https://gaia-x.eu/wp-content/uploads/2024/12/How-to-become-a-Gaia-X-Conformant-Service\_Tagus-Release.pdf)
   * Introduction to community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X OSS community mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X RSS feeds [https://gaia-x.eu/rss-feeds/](https://gaia-x.eu/rss-feeds/)
   * Gaia-X OSS community repository: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Gaia-X Slack: [https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA](https://join.slack.com/t/gaia-xworkspace/shared\_invite/zt-1cjc6bpqp-KgNBldvOXLIcDqS1I1o\_sA)
   * Gaia-X OSS community Matrix Chat: [https://matrix.to/#/#gaiax-community:matrix.org](https://matrix.to/#/#gaiax-community:matrix.org)
   * Gaia-X Docs: [http://docs.gaia-x.eu/](http://docs.gaia-x.eu/)
   * Gaia-X Digital Clearing Houses: [https://docs.gaia-x.eu/#/gxdch](https://docs.gaia-x.eu/#/gxdch)
   * Gaia-X Lighthouse Projects: [https://gaia-x.eu/who-we-are/lighthouse-projects/](https://gaia-x.eu/who-we-are/lighthouse-projects/)
   * Gaia-X Publications: [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Gaia-X Wizard: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)
   * Gaia-X Academy: [https://academy.gaia-x.eu](https://academy.gaia-x.eu)
   * Software architecture: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/architecture/index.md?ref\_type=heads)
   * Gaia-X 101 Workshop: [https://gitlab.com/gaia-x/lab/workshops/gaia-x-101](https://gitlab.com/gaia-x/lab/workshops/gaia-x-101) --> to be updated to Loire? Would be really useful!
   * Developers page on the Gaia-X Website: [https://gaia-x.eu/developers/](https://gaia-x.eu/developers/) Including Gaia-X 101 course
   * Data Space Implementation Forums: [https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit](https://docs.google.com/forms/d/1XnS4N3hfpEpGHgZB7Wc7IWz9-r8smhNAHdgc\_CP1DkI/edit)











