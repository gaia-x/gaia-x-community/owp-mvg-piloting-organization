MVG/Piloting Call: 2022-07-14

1. Interesting sessions/events/news

Kai Meinke: GXFS Connect
- September 7-8
- https://www.gxfs.eu/gxfs-connect-2022/

Discussion on keeping track of interesting events
- DCP Calendar tracks events but you need a DCP account
-> Not clear who is currently responsible for maintenance and updates
- Gaia-X calendar: https://gaia-x.eu/events/
-> Maintained by Marketing & Communication team
- Idea: Add events to GitLab?
-> Ariane Segelitz-Karsten will add events to Wiki from now on

2. Coordinator of MVG group

Election of the new MVG group leader

Candidate: Kai Meinke
Participants in Call: 19
Every participant has the right to vote.

Cristina Pauna: "We vote the peaceful transition of power of the leadership of the MVG group. Is there anybody in the meeting who obstains from voting?"

Number of people obstaining: 0

Cristina Pauna: "Is anybody against Kai becoming the new lead?"

Number of people against it: 0

Kai Meinke is officially pronounced as the new MVG group lead.
(On vacation now, Marius Feldmann will jump in until he returns)

3. AOB

Mission Document:

- Kai Meinke has been working on a new draft of the mission document
-> https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/tree/main
-> Everyone is invited to work on the document!

Kai Meinke is presenting the new draft

Overall information:

- Based on the structure provided in the DCP
- Template is available in GitLab as soon as merge requests are accepted
- Currently: (Too) many owners, developers and maintainers in the repository
-> Kai Meinke would like to trim this a bit and see, who is really active
-> Needs an upgrade on his own role for this
-> Whoever is responsible: Please check merge requests!

Content of the document (for details please view the link shared above):

1. Brief Description of Group:

- No further discussion or remarks

2. Group roles and participants (Commiting, Reporting, Standby)

- Current roles: Lead, Gaia-X Representative, Commiting
-> Feel free to add yourself to the list
-> Kai Meinke will also add the usual suspects

3. Inputs:

- Description of External Dependencies
-> We can't promise to implement everything from the different working groups
-> However, we should at least consider input from all working groups
-> Lighthouse projects should be included as well (drivers of implementation towards production)
-> Connection of the different kinds of input

Cristina Pauna:
- Will review mission with Pierre Gronlier
-> Consider, if CTO Office should be included as well

Marius Feldmann:
- Add dependencies towards further open-source projects
-> Not as strictly as AISBL and lighthouse projects, but let's try to incorporate and pay attention to them, too
-> Refers for example to communities such as SCS
-> Knowledge transfer (in both directions)

4. Outputs:

Kai Meinke added to the already existing content in this section:

- Onboarding guide to Gaia-X software development
-> Aims to create collection of entry points
-> Present the most important and most relevant projects from the whole software stack as well as the different repos
-> Newcomers are often confused about where development takes place (since we do not have one central repo but several ones with different responsible people)
-> Lighthouse projects could contribute as well, pointers to documentation, who to get in touch with,...

- Onboarding to Gaia-X ecosystem
-> We do have a homepage, but a very brief on-ramp would be helpful
-> Where to start when you want to join the Gaia-X ecosystem
-> Remark Cristina Pauna: Good idea, however: Rather combine both Onboardings

- Demonstrations of available Gaia-X open-source software
-> Option to also invite people from lighthouse projects
-> Show their current status, next steps, current struggles,...
-> Remark Cristina Pauna: Good idea, however: Struggles with "event" as the deliverable (rather presentation)
-> Comment Josep Blanch: Based on last Hackathon: Video recordings could be interesting and are easy to add

- Support Gaia-X by validating technical hypothesis before publication of the specifications 
-> Existed in the old version already, was however related to the Lab
-> Now a bit more open (as the MVG also tests early versions and pilots and not primarily finished products)
-> Also related to Item #1 (Support incremental release of Gaia-X open-source software)

[Discussion on whether this point could be added directly to Item #1 or whether it needs to be its own deliverable]

-> Conclusion: Will be included in Item #1

5. Out of Scope:

- Josep Blanch: Where do our user scenarios come from?
-> Comment Kai Meinke: e.g., Data Space Business Committee, GXFS group, lighthouse projects,... 
-> They already bring the use cases, so we are not responsible for finding new use cases but rather for bringing those into practice
-> Implementation focus

- Kai Meinke:
-> "Handled by..." should be filled in too
-> Information is also helpful for newcomers
-> Need to check other mission documents
-> However: Many of them are not up to date

6. Skills required for the mission fulfilment

- No further discussion or remarks

7. KPIs & CFS:

- No further discussion of remarks

8. Toolings:

Kai Meinke would like to foster the Slack Channel
-> Remark Marius Feldmann: Would Matrix be an alternative option?
-> No further discussion on this yet

Further thoughts and discussions:

Kai Meinke: Is the MVG an OWP or a group?
-> Cristina Pauna: It's an OWP!

Kai Meinke: Call to Action for merge requests
-> Whoever is resonsible, please check merge requests
-> Upgrade Kai Meinke to owner role
-> Direct link to merge requests: https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/merge_requests

Hackathon #5:
Lauresha Memeti:
- GXFS is gladly participating with support from eco as always!

