MVG/Piloting Call: 2022-07-21

Kai Meinke is absent, Marius Feldmann is doing the call.

1. Interesting sessions/events/news

Marius Feldmann: GC Tech-Talks
- Tech-Talk #4 takes place on 2022-07-28, 2-3pm CEST
-> Register here: https://mautic.cloudandheat.com/techtalk-registration
- If you're interested in submitting a topic for a future Tech-Talk: Contact maria.vaquero@cloudandheat.com

2. Hackathon date

September 26-27 is the now confirmed!

Discussion on making Hackathon an on-site event
- Challenge: People need to get to the location, exclusion of people that are unable to join (e.g., due to financial reasons),... 
- Instead keeping a hybrid option in mind (also depending on content)
- eco could provide space, AISBL would support as well

3. Mission document

Marius Feldmann presents current state of mission document:
https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/blob/main/mission%20document/mission%20document.md

Remarks:
- Cristina Pauna: Naming of the Group
-> Will be discussed when Kai Meinke is back
- Marius Feldmann: Communication channels
-> Would prefer Matrix instead of Slack 

Call to Action: Review the document!
- Alignment is crucial
- Foundation for our work in the next months

4. AOB

[nothing]