# Meeting notes Gaia-X OSS community August 25, 2022



## Focus of the weekly

- Mission Document (approved)

- Hackathon #5: **Registration open** [https://online2.superoffice.com/Cust26633/CS/scripts/customer.fcgi?action=formFrame\&formId=F-U06hqXqB)](https://online2.superoffice.com/Cust26633/CS/scripts/customer.fcgi?action=formFrame\&formId=F-U06hqXqB))

- Onboarding Guide



## Proposed Agenda

   * Competition \& Antitrust Guidelines
   * Acceptance of Agenda
   * Introduction of new participants and regular contributors 
   * Interesting sessions, events and news
   * Organizational topics
       * Mission document approved
   * Work on the deliverables
       * Organization of the Gaia-X Hackathon
       * Onboarding Guide to Gaia-X Software and Ecosystem
       * Demonstrations
       * Release of Gaia-X Open-Source Software
   * AOB


## General notes

Participants in today's call: 15

    

Acceptance of Agenda: Agenda accepted.

        

### Introduction of new participants and regular contributors:

No new participants present in call



### Sessions, events and news

Data Space Tools, [https://www.dataintelligence.at/event/spacetools-was-verwende-ich-fuer-meinen-data-space/](https://www.dataintelligence.at/event/spacetools-was-verwende-ich-fuer-meinen-data-space/)



Days of digital technologies in Berlin, [https://projekttraeger.dlr.de/de/infothek/veranstaltungen/tage-der-digitalen-technologien-2022-berlin](https://projekttraeger.dlr.de/de/infothek/veranstaltungen/tage-der-digitalen-technologien-2022-berlin)



Blockchain and Mobility Meetup, [https://www.dutchblockchaincoalition.org/agenda/blockchain-mobility-overview-of-gaiax4futuremobility-moveid-project](https://www.dutchblockchaincoalition.org/agenda/blockchain-mobility-overview-of-gaiax4futuremobility-moveid-project)



GXFS Connect 2022, [https://www.gxfs.eu/de/](https://www.gxfs.eu/de/)



Agriculture Data Space Event, [https://gaia-x.eu/event/agriculture-data-space-event/](https://gaia-x.eu/event/agriculture-data-space-event/)



Gaia-X Summit 2022, [https://gaia-x.eu/event/gaia-x-summit-2022/](https://gaia-x.eu/event/gaia-x-summit-2022/)



### Organizational topics

#### Mission Document

Can be found here: [https://gitlab.com/gaia-x/technical-committee/operational-handbook/-/blob/master/docs/mission\_documents/technical-committee-oss-community.yaml](https://gitlab.com/gaia-x/technical-committee/operational-handbook/-/blob/master/docs/mission\_documents/technical-committee-oss-community.yaml) but not being rendered correctly [https://gaia-x.gitlab.io/technical-committee/operational-handbook/Technical\_Committee/#minimal-viable-gaia-x](https://gaia-x.gitlab.io/technical-committee/operational-handbook/Technical\_Committee/#minimal-viable-gaia-x)



See also DCP: [https://community.gaia-x.eu/f/14633138](https://community.gaia-x.eu/f/14633138)



### Work on Deliverables

#### Hackathon

Registration open: [https://online2.superoffice.com/Cust26633/CS/scripts/customer.fcgi?action=formFrame\&formId=F-U06hqXqB](https://online2.superoffice.com/Cust26633/CS/scripts/customer.fcgi?action=formFrame\&formId=F-U06hqXqB)

**Registration is necessary in order to join!** Why? We need you to agree to the terms and privacy notice etc. Link to access Hackathon will only be shared with registered participants. Joining ad-hoc is _not_ possible. **Please spread the word!**



**Call for proposals!**

Drop a (minimal) proposal if you have any ideas/wishes/... - it does _not_ mean you have to be responsible for its organization etc.



Hackathon will take place **online only** (Hybrid potentially in the future but not this time).



#### Onboarding Guide

Added an issue for the Onboarding Guide: [https://gitlab.com/gaia-x/technical-committee/framework/-/issues/2](https://gitlab.com/gaia-x/technical-committee/framework/-/issues/2) to [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)



Since the Framework Document is being created a large part of the Onboarding Guide will be obsolute and we need to focus on topics not covered in the Framework Document.
