# Meeting notes Gaia-X OSS community September 22, 2022



## Focus of the weekly

   * Hackathon No. 5


## Proposed Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of Agenda

3. Introduction of new participants and regular contributors 

4. Interesting sessions, events and news

5. Organizational topics

6. Work on the Deliverables

   * 1. Organization of the Gaia-X Hackathon No. 5
   * 2. Open questions regarding Gaia-X Hackathon No. 5
7. AOB 



## General Notes

Participants in the call: 20

Agenda: Accepted, no points added.



### Interesting sessions, events and news

   * moveID Kick-Off event, September 20-21
   * Deep Dive on Identity and Trust this Friday from 12:30 - 14:00, Lauresha, [https://teams.microsoft.com/l/meetup-join/19%3ameeting\_MmE1ZTI0YzQtODI1Yy00ZDNmLWJmOTktN2U2OGNiYTIzMmJk%40thread.v2/0?context=%7b%22Tid%22%3a%22442d55f0-10e0-4a5d-9894-57c58a5de2c0%22%2c%22Oid%22%3a%2280a53533-c0cf-4701-a0a2-a37ec1bb4296%22%7d](https://teams.microsoft.com/l/meetup-join/19%3ameeting\_MmE1ZTI0YzQtODI1Yy00ZDNmLWJmOTktN2U2OGNiYTIzMmJk%40thread.v2/0?context=%7b%22Tid%22%3a%22442d55f0-10e0-4a5d-9894-57c58a5de2c0%22%2c%22Oid%22%3a%2280a53533-c0cf-4701-a0a2-a37ec1bb4296%22%7d)
   * Trust AnchorsTech Deep Dive, September 29, [https://gaia-x.eu/event/tech-deep-dive/](https://gaia-x.eu/event/tech-deep-dive/)
   * Astrid Hub Wolfsburg [https://www.futurecongress.digital/](https://www.futurecongress.digital/), October 10
   * Gaia-X Summit 2022, November 17-18, [https://gaia-x.eu/event/gaia-x-summit-2022/](https://gaia-x.eu/event/gaia-x-summit-2022/)
   * Cloud Expo Frankfurt, May 10 - 11, 2023, [https://www.cloudexpoeurope.de/en](https://www.cloudexpoeurope.de/en)


### Work on the Deliverables

**Organization of the Gaia-X Hackathon**



   * **FAQ**
       * Is the event only open for developers? - No! Everyone is invited to join.
       * Which developer skills are required? - None. The Hackathon offers something for anyone.
       * Can I join if I don't know anything about Gaia-X yet? - Yes, we are always happy to help and onboard you to Gaia-X!


       * ...if you have any other questions, please **always reach out!** No stupid questions, no issue is too small - we're always happy to repeat stuff in case you missed anything. :) 
       * You can connect with us via mail, mailing list or matrix/slack channels (Overview of our communication channels: [https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization)](https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization)).


   * **Agenda is now finalized!**
       * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/home#hackathon-agenda](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/home#hackathon-agenda)


**Don't forget to register for the Hackathon!** Registration is **mandatory** in order to receive access to the sessions.
