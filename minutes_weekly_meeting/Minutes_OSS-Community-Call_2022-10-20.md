# Meeting notes Gaia-X OSS community October 20, 2022


## Focus of the weekly

   * Discussion of different smaller topics.


## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. OSS Community Event Calendar

6. Hackathon Results Report

7. Catalogue Federation



## General Notes

**Participants in the call:** 15

**Acceptance of Agenda:** Approved by the audience.

**Acceptance of last week's minutes:** Approved by the audience.

**Link to the minutes:** [https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/tree/main/minutes\_weekly\_meeting)



### Introduction of new participants and regular contributors

   * Bonian Riebe (Health-X, Charité)


**Useful resources for newcomers:** The Gaia-X Framework. [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)



### Interesting sessions, events and news



   * **Survey "Acceptance of Gaia-X"**
       * Call for participants!
       * Open for 3 weeks, will take you approx. 10 minutes
       * In German, but you can simply translate the page and participate that way too :)
       * Link: [https://survey.lamapoll.de/Akzeptanz\_Gaia-X/](https://survey.lamapoll.de/Akzeptanz\_Gaia-X/)


   * **Gaia-X Roadshow powered by eco**
       * Oct 20 in Munich, Nov 30 in Frankfurt, date for Hamburg will follow soon, Viena-Austria 16.03.2022, 
       * Link: [https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/](https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/)


   * **GC Tech-Talk No. 5 (Cloud\&Heat)**
       * Oct 27 (Thu) 2 - 3pm (online)
       * "Share without Sharing - Swarm Learning to analyze any private data from any data owner without sharing data to foster digital sovereignty" (HPE) and "Basic digital service - a utility perspective" (EnBW)
       * Link: [https://mautic.cloudandheat.com/techtalk-registration](https://mautic.cloudandheat.com/techtalk-registration)


   * **Open Source Experience**
       * Nov 8 - 9
       * Link: [https://www.opensource-experience.com/en/](https://www.opensource-experience.com/en/)


   * **Gaia-X Summit 2022**
       * Nov 17 - 18, Online Attendance (unlimited), Physical Attendance (very limited)
       * Link: [https://gaia-x.eu/event/gaia-x-summit-2022/](https://gaia-x.eu/event/gaia-x-summit-2022/)


### OSS Community Event Calendar

   * From now on event calendar will be kept here: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar) (upcoming \& past events)
   * Events will also stay in the pad as long as they're upcoming


### Hackathon Results Report

   * Report for Hackathon No. 5 is published!
   * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/home#results-report](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-5/-/wikis/home#results-report)


### Catalogue Federation

   * Question from Enrique on how the synchronization of catalogues is done between the different Federations, in a manner that avoids the exponential growth of each catalogue
   * Lauresha: Catalogues are synchronized within each Federation, not between each other. Some explanations can be found at [https://www.gxfs.eu/videos/](https://www.gxfs.eu/videos/)
   * Klaus: The topic has been brought up in other forums as well, and the need to specify catalogue synchronization is recognized. In the Gaia-X Framework you can see that with the new release we'll create a new WG that will deal with "Inter-Catalogue Synchronization", "Wallet \& Catalogue Interaction" ([https://docs.gaia-x.eu/framework/)](https://docs.gaia-x.eu/framework/)). The new WG will be created after the Gaia-X Summit



