# Meeting notes Gaia-X OSS community November 03, 2022

## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. AOB


## General Notes

**Participants in the call:** 19

**Acceptance of Agenda:** Approved by the audience.

**Acceptance of last week's minutes:** Approved by the audience.

**Link to the minutes:** [https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/owp-mvg-piloting-organization/-/tree/main/minutes\_weekly\_meeting)



### Introduction of new participants and regular contributors

   * No new participants in the call.


**Useful resources for newcomers:** The Gaia-X Framework. [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)



### Interesting sessions, events and news

**We're also collecting all upcoming events here:** [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar) (Feedback?)

   * Eduard from Sovereign Cloud Stack proposed to rather collect all relevant events in a YAML-file as this can be easily converted to a subscribable ics-file: [https://github.com/SovereignCloudStack/calendar](https://github.com/SovereignCloudStack/calendar)


   * **Gaia-X Roadshow powered by eco**
       * Nov 30 in Frankfurt, date for Hamburg will follow soon, Viena-Austria 16.03.2022, 
       * Link: [https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/](https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/)


   * **Open Source Experience**
       * Nov 8 - 9
       * Link: [https://www.opensource-experience.com/en/](https://www.opensource-experience.com/en/)


   * **Gaia-X Summit 2022**
       * Nov 17 - 18, Online Attendance (unlimited), Physical Attendance (very limited)
       * Link: [https://gaia-x.eu/event/gaia-x-summit-2022/](https://gaia-x.eu/event/gaia-x-summit-2022/)


   * **European Big Data Forum**
       * Nov 21 - 23, Prague (CZ)
       * Link: [https://european-big-data-value-forum.eu/2022-edition/programme/](https://european-big-data-value-forum.eu/2022-edition/programme/)


   * **Event by Gaia-X Finland** 
       * "Gaia-X Finland: Data Sharing for Breakfast" and "Data Spaces Technology Landscape 2023"
       * Beginning of December, Helsinki (FIN)
       * Link: [https://www.gaiax.fi/](https://www.gaiax.fi/)


   * **"Gaia-X like" Public Permissionless Compliance Service Deployment Scenario demonstrated in Production by J.P. Morgan**
       * [https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/](https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/) 
       * [https://www.mas.gov.sg/news/media-releases/2022/first-industry-pilot-for-digital-asset-and-decentralised-finance-goes-live#3](https://www.mas.gov.sg/news/media-releases/2022/first-industry-pilot-for-digital-asset-and-decentralised-finance-goes-live#3)
       * [https://polygonscan.com/address/0xE4Aa1f9065220B506A39ff8FdCa94D48CA20865E](https://polygonscan.com/address/0xE4Aa1f9065220B506A39ff8FdCa94D48CA20865E)
       * [https://twitter.com/TyLobban/status/1587679364141158401?s=20](https://twitter.com/TyLobban/status/1587679364141158401?s=20)


### Demonstrations, latest releases

   * **Please share any updates from what you're working on!** What has been done in the last week? Who is involved? Which challenges occured? Which successes did you have? ... We're curious. :)
       * Eduard: What about having a "sharing session" every x weeks? So communities can prepare on giving an update
   * Lab Update (Cristina)
   * SCS Update (Kurt) [https://scs.community/de/release/2022/09/21/release3/](https://scs.community/de/release/2022/09/21/release3/)
   * Update on the Gaia-X Web3 Ecosystem and Pontus-X next week after new release.



