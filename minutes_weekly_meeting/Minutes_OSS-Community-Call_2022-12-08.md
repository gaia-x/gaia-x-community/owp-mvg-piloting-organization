# Meeting notes Gaia-X OSS community December 8, 2022
## Focus of the weekly

   * Hackathon No. 6 - first steps of planning the event
   * Gaia-X Web3 Ecosystem, Self-Descriptions and System Stress Test


## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. Hackathon No. 6

6. Gaia-X Web3 Ecosystem, SDs and Stresstest

7. AOB



## General Notes

**Participants in the call:** -

**Acceptance of Agenda:** Approved by the audience.

**Acceptance of last week's minutes:** Approved by the audience.

**Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2022-12-01.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2022-12-01.md)

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)



### Introduction of new participants and regular contributors

   * Stephan, DSBC
   * Hartwig, IKS


**Useful resources for newcomers:**

   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)


### Interesting sessions, events and news



   * **How to subscribe to the Gaia-X Tech Newsletter**
       * Link: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)


   * **Gaia-X Publications 22.10 and 22.11 are now available**
       * [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)


   * **Summit 2022 Videos now available**
       * [https://gaia-x.eu/mediatech/videos/](https://gaia-x.eu/mediatech/videos/)


   * **Event by Gaia-X Finland** 
       * "Gaia-X Finland: Data Sharing for Breakfast" and "Data Spaces Technology Landscape 2023"
       * Dec 14, Helsinki (FIN)
       * Link: [https://www.gaiax.fi/](https://www.gaiax.fi/)
       * [https://www.sitra.fi/en/events/data-spaces-technology-landscape-2023/#speakers-of-the-event](https://www.sitra.fi/en/events/data-spaces-technology-landscape-2023/#speakers-of-the-event)


   * Lightning Talk Gaia-X Federation Services on Sovereign Cloud Stack 
       * Thursday, 2023-01-12 at 15:45 CET. Feel free to join us
       * [https://scs.community/contribute](https://scs.community/contribute)


   * **Deepdive Gaia-X Federation Services on Sovereign Cloud Stack**
       * Monday, 2023-01-16 at 15:05 CET. Feel free to join us
       * [https://scs.community/contribute](https://scs.community/contribute)
   * 

### Hackathon No. 6

   * Update on the timing. The original proposal around March 21-24 does not work out well.
   * Best bet for the hackathon No. 6 might be after the Easter Holidays. Prep. for content, workshops and location will be more demanding.
   * Thinking about incl. the clearing house and more technical parts looking at the quickly evolving ecosystem


### Web3 Ecosystem Demo \& Stresstest

Want to start with Self-Descriptions?

   * If you want to get started with your Self-Descriptions as Institution please let us (deltaDAO, Gaia-X Web3 Ecosystem participants) know. We are onboarding and helping all the community members along as we believe we should have valid self-descriptions for all community members in the next months to bring the identity game to the next level and to build on that. 

This is the place of documentation for the Web3 Ecosystem and GEN-X network living on GItLab [https://gitlab.com/web3-ecosystem/gen-x-network](https://gitlab.com/web3-ecosystem/gen-x-network)

New Self-Descriptions are being migrated here and the deltaDAO well-known folder.

[https://gitlab.com/web3-ecosystem/gen-x-network/-/tree/main/static/self-descriptions](https://gitlab.com/web3-ecosystem/gen-x-network/-/tree/main/static/self-descriptions)



**Portals \& Services:**


Pontus-X [https://portal.minimal-gaia-x.eu/](https://portal.minimal-gaia-x.eu/)

EuProGigant [https://euprogigant.portal.minimal-gaia-x.eu/)

FMDM [https://marketplace.future-mobility-alliance.org/](https://marketplace.future-mobility-alliance.org/)

moveID [https://portal.moveid.eu/](https://portal.moveid.eu/)

SBB [https://sbb.portal.minimal-gaia-x.eu/](https://sbb.portal.minimal-gaia-x.eu/)

PEAQ [https://gaia-x.portal.peaq.network/](https://gaia-x.portal.peaq.network/)

UDL [https://udl.portal.minimal-gaia-x.eu/](https://udl.portal.minimal-gaia-x.eu/)

Logging Service / Explorer: [https://logging.genx.minimal-gaia-x.eu/](https://logging.genx.minimal-gaia-x.eu/) 



### AOB

   * The Clearing Houses will be several from centralized to decentralized versions that are being worked on today [https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/](https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/) 