# Meeting notes Gaia-X OSS community December 15, 2022

## Focus of the weekly

   * Hackathon No. 6


## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. Hackathon No. 6

6. AOB



## General Notes

**Participants in the call:** 17

**Acceptance of Agenda:** Approved by the audience.

**Acceptance of last week's minutes:** Approved by the audience.

**Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2022-12-08.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2022-12-08.md)

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)



### Introduction of new participants and regular contributors

   * Stefan Dumss (EuProGigant lighthouse project)
   * Sergiu Gordea (AIT Austrian Institute of Technology - interested in Cultural Heritage)


**Useful resources for newcomers:**

   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)


### Interesting sessions, events and news



   * **Lightning Talk Gaia-X Federation Services on Sovereign Cloud Stack**
       * Thursday, 2023-01-12 at 15:45 CET. Feel free to join us
       * [https://scs.community/contribute](https://scs.community/contribute)


   * **Deepdive Gaia-X Federation Services on Sovereign Cloud Stack**
       * Monday, 2023-01-16 at 15:05 CET. Feel free to join us
       * [https://scs.community/contribute](https://scs.community/contribute)


   * **Gaia-X Dataspace Landscape 2023 by Sitra and Gaia-X Hub Finland**
       * Recording available here: Starts at 14:02: [https://youtu.be/CRp\_E0KbvQI?t=836](https://youtu.be/CRp\_E0KbvQI?t=836) 


   * **Data Spaces Discovery Days Netherlands - Business value of sovereign data sharing**
       * 21–23 March 2023
       * Pre-registration link: [https://internationaldataspaces.org/data-spaces-discovery-days-netherlands](https://internationaldataspaces.org/data-spaces-discovery-days-netherlands) 
   * Planning with acatech / German Hub and others to have Self-Description "get started" events where participant SDs are getting created to start the process to the many many projects outside the Trust Framework right now.


### Hackathon No. 6

   * Recap: Talking about extending the hacking (technical presentations and workshops)
   * Also: couple of hundreds of people on site!
   * Date: around end of April, beginning of May (March is too busy already)
   * Cristina is currently working on Call for Participations - expected to be ready in January


### AOB

   * No meeting next week! Next meeting on Jan 12!
   * Slack is currently more popular than Element/Matrix













