# Meeting notes Gaia-X OSS community January 19, 2023

## Focus of the weekly

   * Tagus Release Backlog
   * Hackathon No. 6


## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. Tagus release backlog

6. Tech event \& Hackathon #6

7. Verifiable Credential validation tool

8. AOB



## General Notes

**Participants in the call:** 33

**Acceptance of Agenda:** Agenda point "VC checker" added, otherwise approved by the audience.

**Acceptance of last week's minutes:** Approved by the audience.

**Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-01-12.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-01-12.md)

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * Boris Baldassari (Eclipse Foundation) - reach out to him if you have any questions on getting started in the Eclipse Foundation or on open source in general.


**Useful resources for newcomers:**

   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)




### Interesting sessions, events and news



   * **Gaia-X Inside Gaia-X #7 - GXDCH**
       * Recording will be made available
       * January 18, 2023 with Roland Fadrany, COO Gaia-X, organized by Gaia-X Hub Austria


   * **International Tourism Fair (FITUR)**
       * [https://www.ifema.es/en/fitur](https://www.ifema.es/en/fitur) 
       * January 19, 2023. Session of the Tourism working group of the Spanish Gaia-X Hub (FITURTECH Y).


   * **Catena-X Tenders for Core Services have been published**
       * [https://catena-x.net/de/aktuelles-termine/news-display/first-request-for-tender-for-the-catena-x-core-services](https://catena-x.net/de/aktuelles-termine/news-display/first-request-for-tender-for-the-catena-x-core-services)
       * Includes all centralized services to be deployed only once, time to apply until January 27, end of next week.


   * **ALASCA  Tech-Talk No.1: Yaook: Using Kubernetes for deploying OpenStack, a non-cloud-native application**
       * January 26 (2 - 3 pm, online)
       * [https://alasca.cloud/alasca-tech-talk-1/](https://alasca.cloud/alasca-tech-talk-1/)
       * Organizer: **ALASCA - Verband für betriebsfähige, offene Cloud-Infrastrukturen e.V.**
           * Founded by Cloud\&Heat, Cyberus, D3TN, dNation, secunet, secustack, STACKIT
           * Focusproject: Yaook
           * Go Live was on January 10, 2023
           * Website: [https://alasca.cloud/](https://alasca.cloud/)


   * **Gaia-X Hub Germany - Participant Onboarding and Self-Description Workshop**
       * February 3, 09:00 to 12:00 CET
       * Organized by acatech and deltaDAO
       * Will be held in English due to the huge interest from the international community
       * Creation of Gaia-X compliant participant Self-Descriptions to reach the 50% adoption goal of 2023 and to prepare projects for the Gaia-X Clearing Houses and work with Self-Descriptions
       * Includes Onboarding for the Gaia-X Web3 ecosystem


   * **FOSSDEM**
       * 4-5 February, Brussels
       * [https://fosdem.org/2023/schedule/](https://fosdem.org/2023/schedule/)


   * **Gaia-X Roadshow Hamburg**
       * February 9, organized by eco
       * [https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/](https://www.eco.de/events/gaia-x-roadshow-powered-by-eco/)
       * [https://www.eco.de/event/gaia-x-roadshow-powered-by-eco-hamburg/#tickets](https://www.eco.de/event/gaia-x-roadshow-powered-by-eco-hamburg/#tickets)
       * Restaurant Vlet in der Speicherstadt


   * **FOSS Backstage 23**
       * March 14-15, Berlin (and online)
       * Theme: *Community, Management \& Compliance in Open Source Development*
       * [https://23.foss-backstage.de/](https://23.foss-backstage.de/)
       * Eduard \& Ariane will be there to present **Community2**
           * Community2 is our new community for Open Source Community Managers
           * More information: [https://www.linkedin.com/posts/eduard-itrich\_the-new-year-starts-right-away-with-some-activity-7021022954134110208-Jqwe?utm\_source=share\&utm\_medium=member\_desktop](https://www.linkedin.com/posts/eduard-itrich\_the-new-year-starts-right-away-with-some-activity-7021022954134110208-Jqwe?utm\_source=share\&utm\_medium=member\_desktop)


   * **Market-X, Vienna, Gaia-X Market Adoption Event**
       * March 14-15, Vienna, Aula der Wissenschaften, [https://www.aula-wien.at/](https://www.aula-wien.at/) 
       * Updated website: [https://gaia-x.eu/market-x/](https://gaia-x.eu/market-x/)
       * Market-X will bring together and connect industries and technology partners as well as showcase funding opportunities through the Gaia-X Hubs.
       * Final name will be decided and more information forthcoming about next week, newsletter and brochure


   * **Data Spaces Discovery Days Netherlands - Business value of sovereign data sharing**
       * March 21–23
       * Pre-registration link: [https://internationaldataspaces.org/data-spaces-discovery-days-netherlands](https://internationaldataspaces.org/data-spaces-discovery-days-netherlands) 
       * It seems the name has been changed and there's a new link (Not sure if it's the same event, but the days are the same): [https://internationaldataspaces.org/events/data-spaces-dicovery-days-the-hague-business-value-of-sovereign-data-sharing/](https://internationaldataspaces.org/events/data-spaces-dicovery-days-the-hague-business-value-of-sovereign-data-sharing/)


### Tagus release backlog

   * Tagus backlog is now public [https://gaia-x.atlassian.net/jira/software/c/projects/TAG/issues](https://gaia-x.atlassian.net/jira/software/c/projects/TAG/issues)
   * If you want to contribute by fixing the tickets, use the OSS-Community Mailing list or reach out to Cristina directly
   * **Resources:**
       * Where anyone (!) can comment on Tagus: [https://gitlab.com/groups/gaia-x/lab/compliance/-/issues](https://gitlab.com/groups/gaia-x/lab/compliance/-/issues) 
       * Where developers wok on Tagus: [https://gaia-x.atlassian.net/jira/software/c/projects/TAG/issues](https://gaia-x.atlassian.net/jira/software/c/projects/TAG/issues)


### Tech event \& Hackathon No. 6

   * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home)
   * **Location and date:** should be confirmed by tomorrow, three venues are available, 2 in Bilbao and 1 in Turin
   * **Option for hybrid event?** Does []not[] seem reasonable for the Hackathon as it would be challenging to have some people on site in one room and then connecting people virtually, creates a lot of noise, setting everything up is a lot more difficult,...
   * **Call for contributions:** If you have anything in mind for the Hackathon, please add to the proposals page (or align on suggestions that have already been added, preferably via the mailing list)
       * **Workshop:** This is one possible format in the Tech event; it's supposed to be like a class and take attendees through everything step by step
       * **Deadline for proposals:** roughly end of March
   * **Call for organizers:** If you would like to help organizing the Hackathon, please reach out!




### VC Checker

   * Verifiable Credential validation tool
   * [https://gaia-x.atlassian.net/browse/LAB-312](https://gaia-x.atlassian.net/browse/LAB-312)
   * [https://gitlab.com/gaia-x/lab/ecosystems/simple-vc-checker](https://gitlab.com/gaia-x/lab/ecosystems/simple-vc-checker)
   * This VC Checker is []not[] Gaia-X-specific; it's a general tool for checking VC (across different initiatives)
       * Therefore, no need for Gaia-X compliance (?)
   * Added the walt.id W3C VC/VP Auditor and created a first issue in the repo [https://gitlab.com/gaia-x/lab/ecosystems/simple-vc-checker/-/issues/1](https://gitlab.com/gaia-x/lab/ecosystems/simple-vc-checker/-/issues/1)
       * [https://docs.walt.id/v/ssikit/getting-started/rest-apis/auditor-api](https://docs.walt.id/v/ssikit/getting-started/rest-apis/auditor-api)
       * [https://auditor.ssikit.walt.id/v1/swagger](https://auditor.ssikit.walt.id/v1/swagger)


### AOB

   * Next week: Presentation on the Eclipse Foundation and how to join!





