## Agenda Gaia-X OSS community February 16, 2023

1. Competition & Antitrust Guidelines (1 min, Kai)
2. Acceptance of last week meeting notes and today's Agenda (1 min, Kai)
3. Introduction of new participants and regular contributors (1-10 mins, Kai & newcomers)
4. Interesting sessions, events and news (10 mins, all)
5. Tech event & Hackathon #6 (5 min, Cristina)
6. Update from Gaia-X 4 Future Mobility family (Kai)
7. Update from Gaia-X Web3 Ecosystem "Pontus-X" and GEN-X network (Kai)
8. Updates from other Lighthouses and Gaia-X projects / developers?
9. AOB (all)    
 
## Meeting notes Gaia-X OSS community February 16, 2023

### Focus of the weekly
Updates from the community
Hackathon No. 6

###General Notes
Participants in the call: 30
Acceptance of Agenda: yes
Acceptance of last week's minutes: Approved by the audience.
Link to last week's minutes: https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes_weekly_meeting/Minutes_OSS-Community-Call_2023-02-09.md
Link to all minutes: https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes_weekly_meeting


### Introduction of new participants and regular contributors
Stefan Hedtfeld, btc, Germany
Dennis Appelt, Gaia-X Hub Germany, acatech
Winnie Schöngut, Gaia-X Hub Germany, Domain and Stakeholder Management, acatech

### Useful resources for newcomers: 
Introduction to our Community: https://gitlab.com/gaia-x/gaia-x-community/open-source-community
Link to our mailing list: https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu
The Gaia-X Framework: https://docs.gaia-x.eu/framework/
Link to our mailing list: https://list.gaia-x.eu/postorius/lists/
Gaia-X Publications : https://gaia-x.eu/mediatech/publications/
Subscribe to Gaia-X Tech Newsletter: https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/

### Interesting sessions, events and news
ALASCA Tech-Talk No. 2
February 23, 2-3 pm (online)
Topic: Standardization in the SCS community (with Kurt Garloff)
More information & registration: https://alasca.cloud/alasca-tech-talk-2/
Recording of Tech-Talk No. 1 (Topic: Yaook): https://www.youtube.com/watch?v=aKZ6egM-Hd8&t=21s

FOSS Backstage 23
March 14-15, Berlin (and online)
Theme: Community, Management & Compliance in Open Source Development
https://23.foss-backstage.de/
Eduard & Ariane will be there to present Community2
Community2 is our new community for Open Source Community Managers
More information: https://www.linkedin.com/posts/eduard-itrich_the-new-year-starts-right-away-with-some-activity-7021022954134110208-Jqwe?utm_source=share&utm_medium=member_desktop

Market-X, Vienna, Gaia-X Market Adoption Event
March 14-15, Vienna, Aula der Wissenschaften, https://www.aula-wien.at/ 
Updated website: https://gaia-x.eu/market-x/

New event series: GXFS Tech Workshops Key contact vivien.witt@eco.de
Please save the date for 15 and 16 March 2023. 
The Identity & Trust Workshop and the Self-Description Workshop will take place on these consecutive days. Location tbd

Before planning your trip, please note that knowledge in the following technical areas and access to specific tools are mandatory. If you do not have experience with these tools, a participation in the workshops will be difficult: 
Postman, Curl, Docker, Kubernetes, Github Access necessary
Javascript, Java, Go or any other programming language
Development IDE on the notebook (e.g. Visual Studio Code)
Gitlab, Minikube, k3s or kubernetes cluster are good to have 
IOS/Android Smartphone, Downloaded PCM App (we will be sharing instructions for the PCM download ahead of the workshop) 
Livestream also planned for passive participation

Data Spaces Symposium & Deep-Dive Day
March 21–23, The Hague
Info and registration link: https://internationaldataspaces.org/data-spaces-symposium

Hannover Messe, Hannover, Germany
April 17-21, Hannover (five days is new in 23)
https://www.hannovermesse.de/en/
Contact point would be Vivien Witt, eco, vivien.witt@eco.de
Gaia-X Area

Cloud Expo Europe, Frankfurt, Germany
May 10-11, Frankfurt
https://www.cloudexpoeurope.de/themes2023
Gaia-X at the heart of the Expo, Gaia-X Arena 
Contact point would be Vivien Witt, eco, vivien.witt@eco.de

MyData conference
May 31 - June 01, Helsinki
https://www.mydata.org/event/mydata-2023/

Tech-X & Hackathon #6
May 3-4, Bilbao, Spain
On-Site event
https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home

### Tech event & Hackathon No. 6

Infos and Guideline
https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home

Location and date
Azkuna Zentroa in Bilbao, Spain on the 3&4th of May 2023

Tech-X on site event https://gaia-x.eu/tech-x/
The presentations and workshops will be on-site
For the Hackathon:
The event will by default not be hybrid, but Session Organizers can have people help remotely if they can organize it efficiently for their individual sessions
If you are contributing a session you should have at least one person on site to represent the team
There will be some prizes and ... there will be Gaia-X Hackathon T-Shirts again for the hacking contributors on-site
After the proposal phase end, there will be about two weeks to decide on participation in the hacking sessions & workshops to allow for planning and collaboration before the event.

Registration
https://share-eu1.hsforms.com/14yLtEDMxRUSGwEnVE9tUMgfjelv

Call for proposals
If you have anything in mind for the Hackathon and/or the tech event, please add to the proposals page (or align on suggestions that have already been added, preferably via the mailing list) https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Proposals
Deadline for proposals: March 31, 2023

Call for organizers
If you would like to help organizing the Hackathon, please reach out on the mailing list or to Cristina!

### Gaia-X4 Future Mobility, moveID Components Landscape
About: “GAIA-X moveID” develops a decentralized software infrastructure for communication and commercial transactions among cars, vehicles, traffic infrastructure, factories, and service providers. Decentralized principles are being adopted from Web3, with the aim of allowing peer-to-peer transactions (e.g., between cars or cars and service providers) while ensuring data sovereignty.
Components landscape released to help use case sponsors to identify the tools they need / describe the components they are using. https://moveid.org/2023/01/31/gaia-x-project-moveid-releases-component-landscape/
The “component landscape” will be validated, detailed, and iterated over the course of the project toward a full-fledged system architecture.
Project family continuously implements more use cases (autonomous driving, road damage detection, object detection, simple data sharing) and uses shared infrastructure. All OSS community members are welcome to engage with our public services and infrastructure.
Awaiting latest Trust Framework release to update Self-Descriptions for the infrastructure and services.
Examples: https://portal.moveid.eu/

### Pontus-X, Gaia-X Web3 Ecosystem Upgrade
Next larger release upcoming, transition to Ocean Protocol v4 nearly completed
Awaiting latest Trust Framework Release for integration and to update the ecosystem Self-Descriptions
Introduces Metadata NFTs based on ERC721 for service management
Enables usage of other ERC20 Tokens, i.e. EURO and USD stablecoins, up to the service providers choice
Preparing for Federated Analytics and Federated Learning with Compute-to-Data
Updates to latest Javascript and Python Libraries for easier integration
Improved handling of Metadata for the catalogue, REST-APIs and Lifecycle Management
Improved Documentation and Catalogue Performance (10x)
First demonstration at Market-X event in Vienna with different use cases and lighthouses
Baseline for Trusted Execution Environments / Confidential Computation in 2023 (data & algorithm protection)
Updates will become publicly available via: https://docs.genx.minimal-gaia-x.eu/docs/intro/

### Updates from other lighthouses / projects / developers?
Lighthouses and projects as well as individual developers are encouraged to share updates from their work and progress.
AOB



