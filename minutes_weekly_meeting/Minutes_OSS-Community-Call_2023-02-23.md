# Meeting notes Gaia-X OSS community February 23, 2023

## Focus of the weekly

   * Updates from the community
   * Hackathon No. 6


## General Notes

**Participants in the call:** -

**Acceptance of Agenda:** -

**Acceptance of last week's minutes:** Approved by the audience.

**Link to last week's minutes:**[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-02-16.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-02-16.md) 

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)

### Introduction of new participants and regular contributors

   * Ewann Gavard, working in the Lab Team and on the Clearing House
   * Ajay Jadhav, AyanWorks, developed OCM/PCM


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)


### Interesting sessions, events and news

   * **ALASCA Tech-Talk No. 2**
       * February 23, 2-3 pm (online)
       * Topic: *Standardization in the SCS community* (with Kurt Garloff)
       * More information \& registration: [https://alasca.cloud/alasca-tech-talk-2/](https://alasca.cloud/alasca-tech-talk-2/)
       * Recording of Tech-Talk No. 1 (Topic: Yaook): [https://www.youtube.com/watch?v=aKZ6egM-Hd8\&t=21s](https://www.youtube.com/watch?v=aKZ6egM-Hd8\&t=21s)

   * **FOSS Backstage 23**
       * March 14-15, Berlin (and online)
       * Theme: *Community, Management \& Compliance in Open Source Development*
       * [https://23.foss-backstage.de/](https://23.foss-backstage.de/)
       * Eduard \& Ariane will be there to present **Community2**
           * Community2 is our new community for Open Source Community Managers
           * More information: [https://www.linkedin.com/posts/eduard-itrich\_the-new-year-starts-right-away-with-some-activity-7021022954134110208-Jqwe?utm\_source=share\&utm\_medium=member\_desktop](https://www.linkedin.com/posts/eduard-itrich\_the-new-year-starts-right-away-with-some-activity-7021022954134110208-Jqwe?utm\_source=share\&utm\_medium=member\_desktop)

   * **Market-X, Vienna, Gaia-X Market Adoption Event**
       * March 14-15, Vienna, Aula der Wissenschaften, [https://www.aula-wien.at/](https://www.aula-wien.at/) 
       * Updated website: [https://gaia-x.eu/market-x/](https://gaia-x.eu/market-x/)


   * **New event series: GXFS Tech Workshops Key contact vivien.witt@eco.de**
       * [https://www.gxfs.eu/events/](https://www.gxfs.eu/events/)
       * Please save the date for **15 and 16 March 2023**. 
       * The Identity \& Trust Workshop and the Self-Description Workshop will take place on these consecutive days. Location tbd


           * Before planning your trip, please note that knowledge in the following technical areas and access to specific tools are **[]mandatory[]**. If you do not have experience with these tools, a participation in the workshops will be difficult: 
               * Postman, Curl, Docker, Kubernetes, Github Access necessary
               * Javascript, Java, Go or any other programming language
               * Development IDE on the notebook (e.g. Visual Studio Code)
               * Gitlab, Minikube, k3s or kubernetes cluster are good to have 
               * IOS/Android Smartphone, Downloaded PCM App (we will be sharing instructions for the PCM download ahead of the workshop) 
           * Livestream also planned for passive participation


   * **Data Spaces Symposium \& Deep-Dive Day**
       * March 21–23, The Hague
       * Info and registration link: [https://internationaldataspaces.org/data-spaces-symposium]([]https://internationaldataspaces.org/data-spaces-symposium[])


   * **Hannover Messe, Hannover, Germany**
       * April 17-21, Hannover (five days is new in 23)
       * [https://www.hannovermesse.de/en/](https://www.hannovermesse.de/en/)
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de
       * Gaia-X Area


   * **Cloud Expo Europe, Frankfurt, Germany**
       * May 10-11, Frankfurt
       * [https://www.cloudexpoeurope.de/themes2023](https://www.cloudexpoeurope.de/themes2023)
       * Gaia-X at the heart of the Expo, Gaia-X Arena 
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de


   * **MyData conference**
       * May 31 - June 01, Helsinki
       * [https://www.mydata.org/event/mydata-2023/](https://www.mydata.org/event/mydata-2023/)


   * **Tech-X \& Hackathon #6**
       * May 3-4, Bilbao, Spain
       * On-Site event
       * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home)


### Tech event \& Hackathon No. 6

### 

   * **Infos and Guideline**
       * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home)


   * **Location and date**
       * Azkuna Zentroa in Bilbao, Spain on the 3\&4th of May 2023


   * **Tech-X \& Hackathon #6 on site event **[https://gaia-x.eu/tech-x/](**https://gaia-x.eu/tech-x/**)
       * The onboarding of the hacking sessions will be done virtually, the week before the hackathon. The goal is to  make sure the dev environment is setup before the 2-days hacking and newcomers get familiar with the project and the challenge at-hand


   * **Registration**
       * [https://share-eu1.hsforms.com/14yLtEDMxRUSGwEnVE9tUMgfjelv](https://share-eu1.hsforms.com/14yLtEDMxRUSGwEnVE9tUMgfjelv)


   * **Call for proposals**
       * If you have anything in mind for the Hackathon and/or the tech event, please add to the proposals page (or align on suggestions that have already been added, preferably via the mailing list) [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Proposals](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Proposals)
       * Deadline for proposals: March 31, 2023


   * **Call for organizers**
       * If you would like to help organizing the Hackathon, please reach out on the mailing list or to Cristina!


## Spotlight Work on Self-Descriptions

   * Mario from Arsys has provided some great examples for Self-Descriptions through the mailing list.
   * [https://gitlab.com/arsys-internet/arsys-sd-examples](https://gitlab.com/arsys-internet/arsys-sd-examples)
   * Also featuring a comparison of the SD Wizard and the Descriptions according to the latest Trust Framework implementation (new release upcoming towards Market-X)
   * deltaDAO is currently providing Self-Descriptions here, waiting for the new release before continuing with the examples: [https://gitlab.com/web3-ecosystem/gen-x-network/-/tree/main/static/self-descriptions](https://gitlab.com/web3-ecosystem/gen-x-network/-/tree/main/static/self-descriptions) + [https://github.com/deltaDAO/files/tree/main/Hackathon\_5\_SDs](https://github.com/deltaDAO/files/tree/main/Hackathon\_5\_SDs)
   * Other examples by the community available?
   * Latest packages for Schemata, Ontologies, etc. [https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/packages](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/packages) for the 22.10. version
   * Update on the SD Wizard available? [https://sd-creation-wizard.gxfs.dev/](https://sd-creation-wizard.gxfs.dev/)
   * Update on the state of IDS and EDC descriptions available? [https://github.com/eclipse-edc/MinimumViableDataspace/tree/main/system-tests/resources/self-description](https://github.com/eclipse-edc/MinimumViableDataspace/tree/main/system-tests/resources/self-description)
   * No updates available yet

## Updates from other lighthouses / projects / developers?

   * Lighthouses and projects as well as individual developers are encouraged to share updates from their work and progress.


### AOB

   * 










