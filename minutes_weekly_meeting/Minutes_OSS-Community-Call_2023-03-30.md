# Meeting notes Gaia-X OSS community March 30, 2023

## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. Presentation of OKP4 protocol and smart contracts architecture, builders program, hackathon

6. Updates from other lighthouses / projects / developers 

7. AOB



## Focus of the weekly

   * Updates from the community
   * OKP4
   * Hackathon No. 6


## General Notes

**Participants in the call:** 25

**Acceptance of Agenda:** Accepted by the audience.

**Acceptance of last week's minutes:** Accepted by the audience.

**Link to last week's minutes:**[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-03-23.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-03-23.md) 

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * Bruno Gachie, Functional Analyst, Gaia-X CTO Team, hi!
   * Alexis Desprez, FullStack Developer, Gaia-X CTO Team
   * Gabriel Mengin, OKP4
   * Christophe Camel, OKP4

**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)


### Interesting sessions, events and news

   * **ALASCA Tech-Talk No. 3**
       * March 30, 2-3 pm (online)
       * Topic: *Let's Build a Maintainable Network* *Fabric* (with Christoph Glaubitz, SysEleven)
       * Join here: [https://us02web.zoom.us/j/89142262107?pwd=d0VXa05TQXpEa3crSFhHSk04ZWt0Zz09](https://us02web.zoom.us/j/89142262107?pwd=d0VXa05TQXpEa3crSFhHSk04ZWt0Zz09)
       * Recording of Tech-Talk No. 2 (Topic: SCS): [https://www.youtube.com/watch?v=w1Q7V3297MQ](https://www.youtube.com/watch?v=w1Q7V3297MQ)

   * **SCS Community Lightning Talk**
       * 2023-03-30, 15:45 -- 16:00 CEST
       * Topic: Community Health Analytics in Open Source Software (CHAOSS)
       * Presenter: Georg Link
       * Jitsi @ [https://conf.scs.koeln:8443/SCS-Tech](https://conf.scs.koeln:8443/SCS-Tech)

   * **Hannover Messe, Hannover, Germany**
       * April 17-21, Hannover (five days is new in 23)
       * [https://www.hannovermesse.de/en/](https://www.hannovermesse.de/en/)
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de
       * Gaia-X Area

   * **Tech-X \& Hackathon #6**
       * May 3-4, Bilbao, Spain
       * On-Site event
       * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home)

   * **Cloud Expo Europe, Frankfurt, Germany**
       * May 10-11, Frankfurt
       * [https://www.cloudexpoeurope.de/themes2023](https://www.cloudexpoeurope.de/themes2023)
       * Gaia-X at the heart of the Expo, Gaia-X Arena 
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de

   * **SCS Summit, Berlin, Germany**
       * Berlin, May 23+24
       * [https://scs.community/summit](https://scs.community/summit)
       * German and English language content

   * **MyData conference**
       * May 31 - June 01, Helsinki
       * [https://www.mydata.org/event/mydata-2023/](https://www.mydata.org/event/mydata-2023/)


### Decentralized Compliance Service Hacking

   * CTO Team offering knowledge with the Compliance Service
   * First alignment session on this topic yesterday, open to different stacks not just Hyperledger Firefly


### Presentation of OKP4 protocol and smart contracts architecture, builders program, hackathon

- Link to all OKP4 Resources: [linktr.ee/okp4](https://linktr.ee/okp4)

- Presented by Gabriel Mengin and Christophe Camel

- OKP4 Open Knowledge Protocol [https://okp4.network/](https://okp4.network/)

- L1 blockchain based on Cosmos SDK / Tendermint Consensus, COSM/WASM

- Public and open-source protocol

- Hackathons open to join and collaborate

- Is there the option to bootstrap the network for a consortium?



### Tech-X \& Hackathon No. 6



   * **Infos and Guideline**
       * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home)


   * **Location and date**
       * Azkuna Zentroa in Bilbao, Spain on the 3\&4th of May 2023


   * **Tech-X \& Hackathon #6 on site event:** [https://gaia-x.eu/tech-x/](**https://gaia-x.eu/tech-x/**)
       * Reminder to fill in the proposals in the wiki, if you intend to participate
   * **Registration**
       * [https://share-eu1.hsforms.com/14yLtEDMxRUSGwEnVE9tUMgfjelv](https://share-eu1.hsforms.com/14yLtEDMxRUSGwEnVE9tUMgfjelv)
       * Registration open until end of April (or until we reach capacity)

   * **Call for proposals**
       * If you have anything in mind for the Hackathon and/or the tech event, please add to the proposals page (or align on suggestions that have already been added, preferably via the mailing list)
       * Link to proposals: [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Proposals](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Proposals)
       * Deadline for proposals: **March 31, 2023**
       * You can still edit your proposal until 31st of March, so add it to the page to let us know you intend to come
       * The proposals will be evaluated during 3-7 April and results will be shared during that time-frame


   * **Want to promote the event?**
       * A promo package is now available
       * Link: [https://gaia-x.eu/wp-content/uploads/2023/03/Tech-X\_Promo\_Pack.pdf](https://gaia-x.eu/wp-content/uploads/2023/03/Tech-X\_Promo\_Pack.pdf)


   * **Next steps for March:** Work on the agenda!


### Updates from other lighthouses / projects / developers?

   * Lighthouses and projects as well as individual developers are encouraged to share updates from their work and progress.

### AOB

Nothing.






















