# Meeting notes Gaia-X OSS community April 06, 2023


## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. Tech event \& Hackathon #6

6. Updates from other lighthouses / projects / developers

7. AOB



## Focus of the weekly

   * Updates from the community
   * Tech-X and Hackathon No. 6


## General Notes

**Participants in the call:** 25

**Acceptance of Agenda:** Accepted by the audience.

**Acceptance of last week's minutes:** Accepted by the audience.

**Link to last week's minutes:**[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-03-30.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-03-30.md) 

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)



### Introduction of new participants and regular contributors

   * No new participants.


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)


### Interesting sessions, events and news

   * **Hannover Messe, Hannover, Germany**
       * April 17-21, Hannover (five days is new in 23)
       * [https://www.hannovermesse.de/en/](https://www.hannovermesse.de/en/)
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de
       * Gaia-X Area


   * **Tech-X \& Hackathon #6**
       * May 3-4, Bilbao, Spain
       * On-Site event
       * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home)


   * **Cloud Expo Europe, Frankfurt, Germany**
       * May 10-11, Frankfurt
       * [https://www.cloudexpoeurope.de/themes2023](https://www.cloudexpoeurope.de/themes2023)
       * Gaia-X at the heart of the Expo, Gaia-X Arena 
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de


   * **SCS Summit, Berlin, Germany**
       * Berlin, May 23+24
       * [https://scs.community/summit](https://scs.community/summit)
       * German and English language content


   * **MyData conference**
       * May 31 - June 01, Helsinki
       * [https://www.mydata.org/event/mydata-2023/](https://www.mydata.org/event/mydata-2023/)


### Tech-X \& Hackathon No. 6


   * **Tech-X \& Hackathon #6 on site event**
       * [https://gaia-x.eu/tech-x/](https://gaia-x.eu/tech-x/)


   * **Infos and Guideline**
       * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home)


   * **Location and date**
       * Azkuna Zentroa in Bilbao, Spain on the 3\&4th of May 2023


   * **Registration**
       * [https://share-eu1.hsforms.com/14yLtEDMxRUSGwEnVE9tUMgfjelv](https://share-eu1.hsforms.com/14yLtEDMxRUSGwEnVE9tUMgfjelv)
       * Registration open until end of April (or until we reach capacity)


   * **Call for proposals - closed!**
       * Over 40 proposals were submitted - thank you!
       * Link to proposals: [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Proposals](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Proposals)
       * The proposals will be evaluated during 3-7 April
           * First results are already being shared, rest will follow soon


   * **Agenda**
       * Agenda to be populated here: [https://gaia-x.eu/tech-x-agenda/](https://gaia-x.eu/tech-x-agenda/)
       * Will be built after Easter (page is currently empty)
       * There will be four 90-minute presentation slots (instead of three)


   * **Want to promote the event?**
       * A promo package is now available
       * Link: [https://gaia-x.eu/wp-content/uploads/2023/03/Tech-X\_Promo\_Pack.pdf](https://gaia-x.eu/wp-content/uploads/2023/03/Tech-X\_Promo\_Pack.pdf)


### Updates from other lighthouses / projects / developers?

   * Lighthouses and projects as well as individual developers are encouraged to share updates from their work and progress.


   * **Certificate trust enforcement:** [https://gitlab.com/gaia-x/lab/compliance/gx-registry/-/merge\_requests/85](https://gitlab.com/gaia-x/lab/compliance/gx-registry/-/merge\_requests/85) (Ewann)

### AOB

Nothing.





















