# Meeting notes Gaia-X OSS community April 27, 2023

## Agenda

1. Competition \& Antitrust Guidelines

2. Acceptance of last week meeting notes and today's Agenda

3. Introduction of new participants and regular contributors

4. Interesting sessions, events and news

5. Tech event \& Hackathon #6

6. Showcase of the Workflow Engine (WFE) and its Key Features

7. Updates from other lighthouses / projects / developers

8. AOB



## Focus of the weekly

   * Updates from the community
   * Workflow Engine (WFE) Showcase
   * Tech-X and Hackathon No. 6


## General Notes

**Participants in the call:** 38

**Acceptance of Agenda:** Accepted by the audience.

**Acceptance of last week's minutes:** Accepted by the audience.

**Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-04-13.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-04-13.md) 

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)



### Introduction of new participants and regular contributors

   * Gérôme Egron - WeScale/OVH - GXFS FR
   * Nitin Vavdiya - smartsensesolutions
   * Jan van Boesschoten - AMS CIX
   * Alireza Zangeneh - LEANEA (sys4it)
   * Hossein Rafieekhah - LEANEA (sys4it)


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Compliance Service [https://compliance.lab.gaia-x.eu/v1/docs/#/credential-offer/CommonController\_issueVC](https://compliance.lab.gaia-x.eu/v1/docs/#/credential-offer/CommonController\_issueVC)
   * Registry: [https://registry.lab.gaia-x.eu/v1/docs/#/](https://registry.lab.gaia-x.eu/v1/docs/#/)
   * Calendar: https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar


### Showcase of the Workflow Engine (WFE) and its Key Features

   * Presented by Hossein Rafieekhah
   * Can be found at [https://wfe.gxfs.dev/](https://wfe.gxfs.dev/)
   * Generates full workflows, GUIs, Forms and everything you need to quickly develop a web application 
   * Access will be available after Tech-X, Code is available in the GXFS repositories
   * Repo in GitLab for the WFE [https://gitlab.com/gaia-x/data-infrastructure-federation-services/GXFS\_OAW](https://gitlab.com/gaia-x/data-infrastructure-federation-services/GXFS\_OAW) 


### Interesting sessions, events and news

**Link to our calendar:** [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar) (Ariane)

   * All past events are now (hopefully) added, some links (if there are any) are missing
       * Maybe we can briefly look into the calendar together? :)


   * **ALASCA Tech-Talk #4**
       * April 27, 2-3 pm (online)
       * Topic: *How we transformed our Kubernetes-based infrastructure from terraform/ansible/manual interventions to the Cluster API approach* (with Martin Pilka \& Matej Feder from dNation)
       * More information here: [https://alasca.cloud/alasca-tech-talk-4/](https://alasca.cloud/alasca-tech-talk-4/)

   * **GXFS Monthly Webinar** 
       * April 28, 10-11 am (online)
       * Register here: [https://register.gotowebinar.com/register/1167815918626911317](https://register.gotowebinar.com/register/1167815918626911317)
       * More information GXFS Event site : [https://www.gxfs.eu/events/](https://www.gxfs.eu/events/) 


   * **Tech-X \& Hackathon #6**
       * May 3-4, Bilbao, Spain
       * On-Site event
       * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/home)


   * **Cloud Expo Europe, Frankfurt, Germany**
       * May 10-11, Frankfurt
       * [https://www.cloudexpoeurope.de/themes2023](https://www.cloudexpoeurope.de/themes2023)
       * Gaia-X at the heart of the Expo, Gaia-X Arena 
       * Contact point would be Vivien Witt, eco, vivien.witt@eco.de


   * **SCS Summit, Berlin, Germany**
       * Berlin, May 23+24
       * [https://scs.community/summit](https://scs.community/summit)
       * German and English language content


   * **MyData conference**
       * May 31 - June 01, Helsinki
       * [https://www.mydata.org/event/mydata-2023/](https://www.mydata.org/event/mydata-2023/)


### Tech-X \& Hackathon No. 6

   * **Tech-X \& Hackathon #6 on site event**
       * [https://gaia-x.eu/tech-x/](https://gaia-x.eu/tech-x/)


   * **Infos and Guideline**
       * [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/wikis/Home)


   * **Location and date**
       * Azkuna Zentroa in Bilbao, Spain on the 3\&4th of May 2023


   * **Registration**
       * [https://share-eu1.hsforms.com/14yLtEDMxRUSGwEnVE9tUMgfjelv](https://share-eu1.hsforms.com/14yLtEDMxRUSGwEnVE9tUMgfjelv)
       * Registration open until end of April (or until we reach capacity)


   * **Call for proposals - closed!**

   * **Workshops and Hacking Rooms - fully booked!**
       * All the hacking and the workshop rooms are fully booked. 
       * The main conference is still open for registration until 2nd of May, or when the full capacity is reached (~ 50 spots still available)
       * If you did not catch a place in the workshops but really want to join, you can come outside the room before the workshop starts and take no-show free slots
       * Hackathon onboarding meetings are either happening or are planned.
       * We are all set and excited to start!


   * **Agenda**
       * Agenda to be populated here: [https://gaia-x.eu/tech-x-agenda/](https://gaia-x.eu/tech-x-agenda/)


   * **Want to promote the event?**
       * A promo package is now available
       * Link: [https://gaia-x.eu/wp-content/uploads/2023/04/Tech-X\_Promo\_Pack.pdf](https://gaia-x.eu/wp-content/uploads/2023/04/Tech-X\_Promo\_Pack.pdf)




### Updates from other lighthouses / projects / developers?

   * [https://gitlab.com/gaia-x/policy-rules-committee/trust-framework/-/blob/compliance\_schema/docs/compliance\_schema.md](https://gitlab.com/gaia-x/policy-rules-committee/trust-framework/-/blob/compliance\_schema/docs/compliance\_schema.md) 
   * GX-Signing-Tool moved to lab FQD [https://wizard.lab.gaia-x.eu/master](https://wizard.lab.gaia-x.eu/master) [https://wizard.lab.gaia-x.eu/v1](https://wizard.lab.gaia-x.eu/v1)
   * Lighthouses and projects as well as individual developers are encouraged to share updates from their work and progress.
### 

### AOB

   * No call on May 04!
