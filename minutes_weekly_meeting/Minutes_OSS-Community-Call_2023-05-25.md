# Meeting notes Gaia-X OSS community May 25, 2023


## Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Tech-X event \& hackathon #6
   6. Gaia-X Wizard quick demo
   7. Updates from other lighthouses / projects / Gaia-X Lab/ developers
       1. Updates from Pontus-X and Gaia-X 4 Future Mobility
   8. AOB


##  Focus of the weekly

   * Updates from the community
   * Tech-X and Hackathon No. 6
   * Gaia-X Wizard quick demo
   * Updates from lighthouses

## General Notes

**Participants in the call:** 33

**Acceptance of Agenda:** Accepted by the audience.

**Acceptance of last week's minutes:** Accepted by the audience.

**Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-05-11.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-05-11.md)

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes)\\_weekly\\_meeting

### Introduction of new participants and regular contributors

   * Nabil GHANMI, GXFS-FR
   * Yassir Sellami, CTO Team developer, focus on EDC

**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Compliance Service Swagger UI [https://compliance.lab.gaia-x.eu/v1/docs/#/credential-offer/CommonController](https://compliance.lab.gaia-x.eu/v1/docs/#/credential-offer/CommonController)\\_issueVC
   * Gaia-X Registry Swagger UI: [https://registry.lab.gaia-x.eu/v1/docs/#/](https://registry.lab.gaia-x.eu/v1/docs/#/)
   * Gaia-X Registry: [https://registry.lab.gaia-x.eu/](https://registry.lab.gaia-x.eu/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

   * **SCS Summit, Berlin, Germany**
       * Berlin, May 23+24
       * [https://scs.community/summit](https://scs.community/summit)
       * German and English language content


   * **GXFS upcoming events**: [https://www.gxfs.eu/events/](https://www.gxfs.eu/events/) 
       * **GXFS Monthly Webinar** 26.05.2023 10 to 11 am (English)
       * Register here: 
       * [https://register.gotowebinar.com/register/4921337509621654366](https://register.gotowebinar.com/register/4921337509621654366)


   * **GXFS Tech Workshop**: 21 \& 22 June in Braunschweig
       * Register here: 
       * [https://www.eco.de/event/gxfs-tech-workshop-2/](https://www.eco.de/event/gxfs-tech-workshop-2/) 


   * **ALASCA Tech-Talk No. 5**
       * Today,  2 - 3pm (online)
       * Topic: „Nix and NixOS in Cloud IT“  (Jacek Galowicz (nixcademy))
       * More information here: [https://alasca.cloud/alasca-tech-talk-5/](https://alasca.cloud/alasca-tech-talk-5/)
       * Or join directly here: [https://us02web.zoom.us/j/89142262107?pwd=d0VXa05TQXpEa3crSFhHSk04ZWt0Zz09](https://us02web.zoom.us/j/89142262107?pwd=d0VXa05TQXpEa3crSFhHSk04ZWt0Zz09)


   * **Webinar DG Grow on Standardisation Digital Product Pass June 12th**


       * In order to present the draft standardisation request to a wide group of potentially interested stakeholders, we are organising a Webex webinar next **12 June 2023, from 14:00 until 17:00 CET**. The webinar will also give us the possibility to update stakeholders on the current status of negotiation in the Council and Parliament with regards to the DPP, to provide elements of reflection related to possible DPP design options, updates from the EU CIRPASS project, and future funding opportunities related to DPP testing.


       * Webinar []draft[] agenda:
 

       * **14:00 – 14:10**: Welcome and housekeeping info (Michele Galatola, DG GROW)
       * **14:10 – 14:30**: Update on the co-decision process (William Neale, DG ENV)
       * **14:30 – 15:00**: The draft standardisation request on DPP system (Michele Galatola, DG GROW)
       * **15:00 – 15:30**: Q\&A from stakeholders
       * **15:30 – 15:45**: Break
       * **15:45 – 16:15**: First considerations on possible technical solutions for the design of the DPP system (Joao Rodrigues Frade, DG DIGIT)
       * **16:15 – 16:35**: The EU CIRPASS project and its contribution to DPP system design (Carolynn Bernier, CIRPASS project coordinator)
       * **16:35 – 16:45**: Upcoming Digital Europe funding opportunities related to DPP (Ilias Iakovidis, DG CNECT)
       * **16:45 – 16:55**: Q\&A
       * **16:55 – 17:00**: Conclusions and next steps (Michele Galatola, DG GROW)
       *  
       * If you want to attend this webinar please use this link: [https://ecconf.webex.com/ecconf/j.php?MTID=meda817498ac42b65479e72db1a31f23f](https://ecconf.webex.com/ecconf/j.php?MTID=meda817498ac42b65479e72db1a31f23f)


   * **Gaia-X General Assembly 2023**
       * June 5, 2023
       * [https://membersplatform.gaia-x.eu/dashboard?locale=en#/public-event-details/11](https://membersplatform.gaia-x.eu/dashboard?locale=en#/public-event-details/11)
       * New Board of Directors (24+2) will be elected by members


   * **EclipseCon, Ludwigsburg, Germany**
       * October 16 - 19, 2023
       * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
       * Monday 16th is the community day and there is the possibility to have a dedicated space for Gaia-X to have a workshop on Gaia-X OSS projects
       * CFP is currently open until June 16. Early submission deadline is June 2 to have a better chance to have your proposition accepted.


### Tech-X \& Hackathon No. 6

   * Tech-X \& Hackathon #6 in Bilbao, Spain
       * [https://gaia-x.eu/tech-x/](https://gaia-x.eu/tech-x/)
       * Videos available via Membership Platform: [https://membersplatform.gaia-x.eu/dashboard?locale=en#/stories/post/10](https://membersplatform.gaia-x.eu/dashboard?locale=en#/stories/post/10)
       * Feedback form is closing on 26th May, reviews are mostly positive
       * Hackathon report is in review with the session organizers, and is planned to be published by end of May


### Gaia-X Wizard Quick Demo

   * Recommended to get familiar with: [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Presentation of example process from 0 to 1
       * Get your SSL Certificate \& check it
       * Expose SSL Certificate, [https://www.delta-dao.com/.well-known/certificate-chain.crt](https://www.delta-dao.com/.well-known/certificate-chain.crt)
       * Create your DID:WEB, [https://delta-dao.com/.well-known/did.json](https://delta-dao.com/.well-known/did.json) and expose
       * Create your participant credential payload
       * Sign with Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
       * Verify against Gaia-X Compliance Service in Wizard and [https://compliance.lab.gaia-x.eu/v1/docs/#/credential-offer/CommonController\_issueVC](https://compliance.lab.gaia-x.eu/v1/docs/#/credential-offer/CommonController\_issueVC)
       * Expose, [https://delta-dao.com/.well-known/participant.json](https://delta-dao.com/.well-known/participant.json)
       * Share your credentials and learn together with others! 
       * Arsys deployed the wizard on their infra as well, for testing purposes: [https://wizard.arlabdevelopments.com/](https://wizard.arlabdevelopments.com/)
       * Find the complete source code, developed by smartSense Solutions, on how to create and sign the verifiable credentials with one click here: [https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/tree/main/implementing\_Verifiable\_Credentials\_in\_a\_real\_live\_use\_case](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/gx-hackathon-6/-/tree/main/implementing\_Verifiable\_Credentials\_in\_a\_real\_live\_use\_case). It will also show architecture diagrams and UML diagrams


### Updates from other lighthouses / projects / developers?

   * **Pontus-X \& Gaia-X 4 Future Mobility**
       * Proposed integration in IDSA Connector Report and new participants onboarded
       * New Gaia-X 4 Future Mobility Services published
           * [https://v4.minimal-gaia-x.eu/asset/did:op:684af1448fd5c99d5e4839da4f6e206056be74defd206a29307cc68ea464557f](https://v4.minimal-gaia-x.eu/asset/did:op:684af1448fd5c99d5e4839da4f6e206056be74defd206a29307cc68ea464557f)
           * [https://v4.minimal-gaia-x.eu/asset/did:op:99fda160c29d15c4c9d2216033da9594f9932176229f77cfadf53dbf12cc4dc8](https://v4.minimal-gaia-x.eu/asset/did:op:99fda160c29d15c4c9d2216033da9594f9932176229f77cfadf53dbf12cc4dc8)
       * EUROe Production integration completed and onboarding completed
           * [https://dev.euroe.com/docs/Stablecoin/contract-addresses](https://dev.euroe.com/docs/Stablecoin/contract-addresses)
           * EUROe is qualified and regulated as European e-money, EURO equivalent
       * Transparent EV Supply Chain Use Case released
   * Support for additional networks enabled
   * Future Mobility Data Marketplace of Gaia-X Hub Netherlands upgraded to v4
       * [https://marketplace.future-mobility-alliance.org/](https://marketplace.future-mobility-alliance.org/)
   * Gaia-X 4 Future Mobility PLC-AAD upgraded to v4
   * Work on CSP, Sustainability and Compute Carbon Offsetting continues
       * New features available next
           * Transparent Pricing and Fee Split for all service providers (Data Provider, Software Provider, Market Provider, Cloud Service Provider, Consortia/Community Fee)
           * Cloud Service Provider selection for Computation Locations
           * Cloud Service Provider visibility enabled
   * Work on Infrastructure Composition Attributes and Credentials continues
   * Release planned for next week


   * **Planned tenders for GXFS-DE enhancements for June 2023 short summary**:
       * Requirements to achieve a timely alignment of the PCM/OCM/TSA stack to support DID:WEB and VC with
       * JsonWebKey2020 (PRIO 1)


       * Ineroperability:
           * AIP 2.0 (JSON\_LD)        
           * OpenID Connect for  VP        
           * OpenID for VC Issuance        
           * SIOP V2   
       * Functional:
           * PCM  DID Resolving        
           * Endpoint Spec. -> ICAM  (precondition for PCM  DID Resolving)        
           * OCM  E1 (upgrade)        
           * TRAIN integration in TSA  + Rego Rules        
           * SD  Input -> NOT  API        
           * Integration Layer ( SD,  DID, CLI Command Line Interface)        
           * AAS  Tests -> policy-based for AA.        
           * TRAIN: Implementation \& Create  Use Cases   


### AOB

#### **EU Cyber Resilience Act (CRA)**

Brief discussion on CRA's impact on the Open Source. For more information, please read the Eclipse Foundation publication on this matter

   * [https://eclipse-foundation.blog/2023/01/15/european-cyber-resiliency-act-potential-impact-on-the-eclipse-foundation/](https://eclipse-foundation.blog/2023/01/15/european-cyber-resiliency-act-potential-impact-on-the-eclipse-foundation/)
   * [https://newsroom.eclipse.org/news/announcements/open-letter-european-commission-cyber-resilience-act](https://newsroom.eclipse.org/news/announcements/open-letter-european-commission-cyber-resilience-act)
If you can, it is important to reach out to your government / local authorities to explain them how Open Source is important to you business and your ability to innovate. You may contact Gaël Blondelle <gael.blondelle\_@\_eclipse-foundation.org> for more details.

#### Migration to Eclipse of the GXFS-DE components

- project has been created [https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/issues/472](https://gitlab.eclipse.org/eclipsefdn/emo-team/emo/-/issues/472)

- the provisioning of the project is ongoing, as several procedures need to be fulfilled before the initial contribution

#### **Task for the Community**

**Express your expectations to the OSS-Community meeting (via mailing list, Slack/Matrix, here in the pad)**

   * What information are you looking for?
   * What do you expect?
   * How can we make this more useful?

