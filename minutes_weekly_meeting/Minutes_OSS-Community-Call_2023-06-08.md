# Meeting notes Gaia-X OSS community June 08, 2023

## Agenda

   1. Competition \& Antitrust guidelines
   1. Acceptance of last week meeting notes and today's agenda
   1. Introduction of new participants and regular contributors
   1. Interesting sessions, events and news
   1. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   1. AOB


##  Focus of the weekly

   * Updates from the community

## General Notes

**Participants in the call:** 25

**Acceptance of Agenda:** Accepted by the audience.

**Acceptance of last week's minutes:** Accepted by the audience.

**Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-06-01.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-06-01.md)

**Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)

### Introduction of new participants and regular contributors

   * Julien Vanwambeke, Gaia-X, Working on the Gaia-X Academy

**Useful resources for newcomers:**
   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Compliance Service Swagger UI [https://compliance.lab.gaia-x.eu/v1/docs/](https://compliance.lab.gaia-x.eu/v1/docs/)
   * Gaia-X Registry Swagger UI: [https://registry.lab.gaia-x.eu/v1/docs/#/](https://registry.lab.gaia-x.eu/v1/docs/#/)
   * Gaia-X Registry: [https://registry.lab.gaia-x.eu/](https://registry.lab.gaia-x.eu/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

   * **Gaia-X General Assembly 2023**
       * June 5, 2023
       * [https://membersplatform.gaia-x.eu/dashboard?locale=en#/public-event-details/11](https://membersplatform.gaia-x.eu/dashboard?locale=en#/public-event-details/11)
       * New Board of Directors elected for two years

   * **Webinar DG Grow on Standardisation Digital Product Pass June 12th**
       * In order to present the draft standardisation request to a wide group of potentially interested stakeholders, we are organising a Webex webinar next **12 June 2023, from 14:00 until 17:00 CET**. The webinar will also give us the possibility to update stakeholders on the current status of negotiation in the Council and Parliament with regards to the DPP, to provide elements of reflection related to possible DPP design options, updates from the EU CIRPASS project, and future funding opportunities related to DPP testing.


       * Webinar (draft) agenda:
 

       * **14:00 – 14:10**: Welcome and housekeeping info (Michele Galatola, DG GROW)
       * **14:10 – 14:30**: Update on the co-decision process (William Neale, DG ENV)
       * **14:30 – 15:00**: The draft standardisation request on DPP system (Michele Galatola, DG GROW)
       * **15:00 – 15:30**: Q\&A from stakeholders
       * **15:30 – 15:45**: Break
       * **15:45 – 16:15**: First considerations on possible technical solutions for the design of the DPP system (Joao Rodrigues Frade, DG DIGIT)
       * **16:15 – 16:35**: The EU CIRPASS project and its contribution to DPP system design (Carolynn Bernier, CIRPASS project coordinator)
       * **16:35 – 16:45**: Upcoming Digital Europe funding opportunities related to DPP (Ilias Iakovidis, DG CNECT)
       * **16:45 – 16:55**: Q\&A
       * **16:55 – 17:00**: Conclusions and next steps (Michele Galatola, DG GROW)
       * If you want to attend this webinar please use this link: [https://ecconf.webex.com/ecconf/j.php?MTID=meda817498ac42b65479e72db1a31f23f](https://ecconf.webex.com/ecconf/j.php?MTID=meda817498ac42b65479e72db1a31f23f)


   * **GXFS Tech Workshop**
       * 21 \& 22 June in Braunschweig - Physical only
       * Register here: [https://www.eco.de/event/gxfs-tech-workshop-2/](https://www.eco.de/event/gxfs-tech-workshop-2/) 
       * The workshop will be comprised of introductory sessions and exercises on day one focusing on the GXFS work packages identity and trust as well as catalogue and asset descriptions. We will also hear the latest update on EDC components. On day two, we will have hacking sessions focused on exploring how GXFS and EDC components can be used.


   * **Eclipse XFSC Into webinar**
       * Jun 26, 2023 3:00 PM - 4:30 PM CEST
       * [https://www.linkedin.com/posts/emma-wehrwein-eco\_gxfs-xfsc-xfsc-activity-7069670712877150210-5VA3?utm\_source=share\&utm\_medium=member\_desktop](https://www.linkedin.com/posts/emma-wehrwein-eco\_gxfs-xfsc-xfsc-activity-7069670712877150210-5VA3?utm\_source=share\&utm\_medium=member\_desktop)

   * **New GXFS Whitepaper:** [https://www.gxfs.eu/kritis-whitepaper/](https://www.gxfs.eu/kritis-whitepaper/)
       * Which rules operators of critical infrastructures have to follow in the context of Gaia-X, is discussed in the new Gaia-X Federation Services (GXFS-DE) whitepaper “Information Security of Critical Infrastructures – Law and Regulation for Gaia-X and the Gaia-X Federation Services”.

   * **5th HACKATHONAMRING** - Physical only
       * Date and time: June 23 · 4pm - June 25 · 4pm CEST
       * [https://www.eventbrite.de/e/5th-hackathonamring-tickets-190977106667?aff=ebdssbdestsearch](https://www.eventbrite.de/e/5th-hackathonamring-tickets-190977106667?aff=ebdssbdestsearch) 


   * **Gaia-X Public Data Space Event**
       * June 27, 2023
       * [https://gaia-x.eu/event/public-sector-data-space-event/](https://gaia-x.eu/event/public-sector-data-space-event/)


   * **EclipseCon, Ludwigsburg, Germany**
       * October 16 - 19, 2023
       * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
       * Monday 16th is the community day and there is the possibility to have a dedicated space for Gaia-X to have a workshop on Gaia-X OSS projects
       * CFP is currently open until June 16.

### Updates from other lighthouses / projects / developers?

   * **Gaia-X LAB and Gaia-X Digital Clearing Houses**
       * **GXDCH by Aruba ~~and T-Systems~~ active**[]~~and ~~[]is available behind the respective load balancers of the compliance service => Update from Ewann, Aruba IT at the moment. I have no info for TSystem DCH
       * Docs to deploy a DCH: [https://gitlab.com/gaia-x/lab/gxdch](https://gitlab.com/gaia-x/lab/gxdch) (please note we are in the licensed model at the moment [https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/)](https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/))


   * **Gaia-X in production, first Pontus-X production transactions completed** with a supply-chain and CO2-footprint use case. Demonstrated live to Federal Ministry of Economic Affairs and Climate Action on May 26, 2023.
       * Demonstrated all relevant business models for data space service providers, data providers, software providers, infrastructure providers and CO2-compensation of the compute job on demand. Total TX cost, 0,21 EUR. 
       * Compute location can be selected by consumer on demand and is based on the service offerings of the CSP / compute infrastructure provider.
       * Multichain support enabled
       * Software: [https://polygonscan.com/tx/0xd5b5c0579310f92626a8fb92846b3751faa1d19a4b0b15eb7844e4aef26a5c03](https://polygonscan.com/tx/0xd5b5c0579310f92626a8fb92846b3751faa1d19a4b0b15eb7844e4aef26a5c03)
       * Compute \& Data: [https://polygonscan.com/tx/0x9e33147f67fe0bfa8b01dfbcf0c1e30304dbed8edf28d783459ee291402fa494](https://polygonscan.com/tx/0x9e33147f67fe0bfa8b01dfbcf0c1e30304dbed8edf28d783459ee291402fa494)
       * optional CO2-Compensation: [https://polygonscan.com/tx/0xc862ae09edc67e58c8178f37415695aa08a9f723fbeb4d5a89db584b1c70d4fb](https://polygonscan.com/tx/0xc862ae09edc67e58c8178f37415695aa08a9f723fbeb4d5a89db584b1c70d4fb)
       * Example Service Credential: [https://delta-dao.com/.well-known/serviceBatteryPackProductionData\_main.json](https://delta-dao.com/.well-known/serviceBatteryPackProductionData\_main.json)
       * Enabling a wide variety of European Cloud Service Providers and computational locations / service levels from cloud to edge to be used on an ad hoc basis. 
   * **Beginning integration of additional mobility, manufacturing, health and edge/IoT use cases**
       * deltaDAO won Health-X dataLOFT Fellowship Price, [https://www.health-x.org/en/platform](https://www.health-x.org/en/platform). Integration of health use cases in the public Gaia-X sandbox underway.
       * Integration with WOBCOM Open Data Platform based on FiWare started to enable smart parking and EV-charging services and applications: [https://v4.minimal-gaia-x.eu/asset/did:op:99fda160c29d15c4c9d2216033da9594f9932176229f77cfadf53dbf12cc4dc8](https://v4.minimal-gaia-x.eu/asset/did:op:99fda160c29d15c4c9d2216033da9594f9932176229f77cfadf53dbf12cc4dc8)
   * **Pontus-X Public Testing Sandbox for Gaia-X participants will receive a major upgrade** over the next month
       * Upgrade to latest polygon edge software version v1.0.0-rc1 and migration of remaining Portals to v4
       * The Supernets v1.0.0 RC1 release focuses on delivering the MVP product and customer flows necessary for taking a Supernet to production. This release contains all MVP functionality and is intended to test all end-to-end workflows. See: [https://github.com/0xPolygon/polygon-edge/releases/tag/v1.0.0-rc1](https://github.com/0xPolygon/polygon-edge/releases/tag/v1.0.0-rc1) 
   * **Gitlab license renewal**: We will remove accounts that are inactive since January 1st. We "only" have 300+ seats offered


### AOB

#### **Task for the Community**

**Express your expectations to the OSS-Community meeting (via mailing list, Slack/Matrix, here in the pad)**

   * What information are you looking for?
   * What do you expect?
   * How can we make this more useful?
