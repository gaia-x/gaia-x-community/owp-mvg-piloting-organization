# Meeting notes Gaia-X OSS community June 15, 2023

## Agenda

1. Competition \& Antitrust guidelines
2. Acceptance of last week meeting notes and today's agenda
3. Introduction of new participants and regular contributors 
4. Interesting sessions, events and news
5. Gaia-X Academy
6. Updates from other lighthouses / projects / Gaia-X Lab/ developers
7. AOB


##  Focus of the weekly

   * Updates from the community
   * Gaia-X Academy

## General Notes

   * **Participants in the call:** 31
   * **Acceptance of Agenda:** Accepted by the audience.
   * **Acceptance of last week's minutes:** Accepted by the audience.
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-06-08.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-06-08.md)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)

### Introduction of new participants and regular contributors

   * Mukund Sonaiya, Smart Sense Solutions, SSI
   * Giuditta del Buono, Gaia-X AISBL
   * Markus Sabadello, Danube Tech, SSI

**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Compliance Service Swagger UI [https://compliance.lab.gaia-x.eu/v1/docs/](https://compliance.lab.gaia-x.eu/v1/docs/)
   * Gaia-X Registry Swagger UI: [https://registry.lab.gaia-x.eu/v1/docs/#/](https://registry.lab.gaia-x.eu/v1/docs/#/)
   * Gaia-X Registry: [https://registry.lab.gaia-x.eu/](https://registry.lab.gaia-x.eu/)
   * Gaia-X Registration Number Service: [https://registrationnumber.notary.gaia-x.eu/v1/docs/](https://registrationnumber.notary.gaia-x.eu/v1/docs/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

   * **GXFS Tech Workshop**
       * 21 \& 22 June in Braunschweig - Physical only
       * Register here: [https://www.eco.de/event/gxfs-tech-workshop-2/](https://www.eco.de/event/gxfs-tech-workshop-2/) 
       * The workshop will be comprised of introductory sessions and exercises on day one focusing on the GXFS work packages identity and trust as well as catalogue and asset descriptions. We will also hear the latest update on EDC components. On day two, we will have hacking sessions focused on exploring how GXFS and EDC components can be used.

   * **Eclipse XFSC Into webinar**
       * Jun 26, 2023 3:00 PM - 4:30 PM CEST
       * [https://www.linkedin.com/posts/emma-wehrwein-eco\_gxfs-xfsc-xfsc-activity-7069670712877150210-5VA3?utm\_source=share\&utm\_medium=member\_desktop](https://www.linkedin.com/posts/emma-wehrwein-eco\_gxfs-xfsc-xfsc-activity-7069670712877150210-5VA3?utm\_source=share\&utm\_medium=member\_desktop)

   * **New GXFS Whitepaper:** [https://www.gxfs.eu/kritis-whitepaper/](https://www.gxfs.eu/kritis-whitepaper/)
       * Which rules operators of critical infrastructures have to follow in the context of Gaia-X, is discussed in the new Gaia-X Federation Services (GXFS-DE) whitepaper “Information Security of Critical Infrastructures – Law and Regulation for Gaia-X and the Gaia-X Federation Services”.

   * **5th HACKATHONAMRING** - Physical only
       * Date and time: June 23 · 4pm - June 25 · 4pm CEST
       * [https://www.eventbrite.de/e/5th-hackathonamring-tickets-190977106667?aff=](https://www.eventbrite.de/e/5th-hackathonamring-tickets-190977106667?aff=)   

   * **Gaia-X Public Data Space Event**
       * June 27, 2023
       * [https://gaia-x.eu/event/public-sector-data-space-event/](https://gaia-x.eu/event/public-sector-data-space-event/)

   * **EclipseCon, Ludwigsburg, Germany**
       * October 16 - 19, 2023
       * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
       * Monday 16th is the community day and there is the possibility to have a dedicated space for Gaia-X to have a workshop on Gaia-X OSS projects
       * CFP is currently open until June 16.


### Gaia-X Academy

   * Work on the Gaia-X Academy has started.
       * While the first release is unknown the Gaia-X AISBL team will hopefully be able to share more news on the timeline, and scope soon.
       * A  growing number of Gaia-X OSS community members have expressed interest to support the content creation, testing and launch of the Gaia-X Academy to reach the target audience and support newcomers in their journey to Gaia-X and deepen the understand of Gaia-X in a scalable manner.
       * Proposal: Find out about the user needs in the OSS community to tailor Academy content to the needs expressed and validate product to market fit.
       * Proposal: Making the Gaia-X Academy publicly available and offer easy first chapter in a multiple choice manner to explain and learn the basic concepts and implementations of Gaia-X to enable scalable, fast and efficient onboarding. Offer some kind of credential, POAP, NFT or easy certificate as gratification.
       * Proposal: Use the concept of gamification and the value of social signalling. People can be very happy to show their successful completion of the Gaia-X Academy and level up. Imagine levels and leaderboard as well as onboarding of various project participants as knowledge about latest Gaia-X developments is often unknown.
       * What is the main content the OSS community is currently looking for? Please bring your proposals.
           * Kai: How to become a Gaia-X participant in easy steps?
           * Kai: How can I offer a service in the Gaia-X framework?
           * Kai: What does Gaia-X compliance mean and how is it reached?
           * Kai: What is the benefit of Gaia-X compliance?
           * Kai: What is the Gaia-X Clearing House?
           * Kai: What is Gaia-X and Gaia-X AISBL?
           * Kai: What are the Gaia-X Federation Services and how can I create my own set of federation services?
           * Kai: What does a Gaia-X lighthouse make a lighthouse?
           * Kai: How does governance work in Gaia-X AISBL?
           * What is the difference between Gaia-x compliant and Gaia-x compatible ?
           * How can I deploy a Gaia-X Clearing House? How can I certify that my Clearing House is compliant with Gaia-X? Can I develop my own Clearing House components and have an assessment?
           * How to onboard a new participant in a federation?


       * Any questions regarding the Gaia-X Academy?
           * How to contribute to the Gaia-X Academy?
           * Please reach out to Frederik for details and coordination, it is not on the OSS group to create the Academy
       * Point of contact for the Gaia-X Academy: frederik.tengg@gaia-x.eu


### Updates from other lighthouses / projects / developers?

   * **Gaia-X LAB and Gaia-X Digital Clearing Houses**
       * **GXDCH by Aruba active**
       * Docs to deploy a DCH: [https://gitlab.com/gaia-x/lab/gxdch](https://gitlab.com/gaia-x/lab/gxdch) (please note we are in the licensed model at the moment [https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/)](https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/))


   * **Gaia-X in production, first Pontus-X production transactions completed** 
       * Enabling a wide variety of European Cloud Service Providers and computational locations / service levels from cloud to edge to be used on an ad hoc basis. 

   * **Pontus-X Public Testing Sandbox for Gaia-X participants will receive a major upgrade** over the next month 

   * **Nautilus, a JS wrapper / API for Pontus-X and Ocean Protocol**, released to enable automatic publication and consumption in the public Pontus-X ecosystem for Gaia-X.
       * Enables easy integration in other applications
       * Repository: [https://github.com/deltaDAO/nautilus](https://github.com/deltaDAO/nautilus)
       * [https://deltadao.github.io/nautilus/docs/api/](https://deltadao.github.io/nautilus/docs/api/)

   * **Gitlab license renewal**: We will remove accounts that are inactive since January 1st. We "only" have 300+ seats offered. => Done
   * Wizard "step-by-step" release coming soon (with Lab hosted DIDs and derivated key pairs): [https://wizard.lab.gaia-x.eu/development](https://wizard.lab.gaia-x.eu/development) - [https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool)
   * Ontology visualization: [https://service.tib.eu/webvowl/#iri=https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/raw/feat/ontology-using-protege/ontology.ttl](https://service.tib.eu/webvowl/#iri=https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/raw/feat/ontology-using-protege/ontology.ttl)

### AOB

#### **Task for the Community**

**Express your expectations to the OSS-Community meeting (via mailing list, Slack/Matrix, here in the pad)**



   * What information are you looking for?
   * What do you expect?
   * How can we make this more useful?







