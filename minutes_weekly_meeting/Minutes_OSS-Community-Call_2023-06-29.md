# Meeting notes Gaia-X OSS community June 29, 2023

## Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. SSI Integration in Future Mobility Data Market and Sphereon Wallet
   6. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   7. AOB

##  Focus of the weekly

   * Updates from the community
   * SSI Integration in Future Mobility Data Market and Sphereon Wallet

## General Notes

   * **Participants in the call:** 37
   * **Acceptance of Agenda:** Accepted by the audience.
   * **Acceptance of last week's minutes:** Accepted by the audience.
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-06-21.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-06-21.md)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * Antonio Salvado, TICE.PT, Portugal
   * Niels Klomp, Co-Founder and CTO, Sphereon
   * Martin Batz, IONOS, BD


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Compliance Service Swagger UI [https://compliance.lab.gaia-x.eu/v1/docs/](https://compliance.lab.gaia-x.eu/v1/docs/)
   * Gaia-X Registry Swagger UI: [https://registry.lab.gaia-x.eu/v1/docs/#/](https://registry.lab.gaia-x.eu/v1/docs/#/)
   * Gaia-X Registry: [https://registry.lab.gaia-x.eu/](https://registry.lab.gaia-x.eu/)
   * Gaia-X Registration Number Service: [https://registrationnumber.notary.gaia-x.eu/v1/docs/](https://registrationnumber.notary.gaia-x.eu/v1/docs/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news


**ALASCA Tech-Talk No. 6**

   * Today,  2 - 3pm (online)
   * Topic: „Deploying Gardener: Going From ‘Lost in Abstraction’ to ‘GitOps All the Way’“ (Alexander Predeschly, STACKIT)
   * More information here: [https://alasca.cloud/alasca-tech-talk-6/](https://alasca.cloud/alasca-tech-talk-6/)
   * Or join directly here: [https://us02web.zoom.us/j/89142262107?pwd=d0VXa05TQXpEa3crSFhHSk04ZWt0Zz09](https://us02web.zoom.us/j/89142262107?pwd=d0VXa05TQXpEa3crSFhHSk04ZWt0Zz09)


**Gaia-X Tech Deep Dive**

   * Today at 12:00 with Pierre Gronlier
   * Focus: Gaia-X architecture fundamentals and dataspace interoperability
   * Invitation was sent via the oss mailing list


**Blockchance 2023**

   * June 28 - 30, Hamburg (Germany)
   * Link: [https://blockchance.eu](https://blockchance.eu)
   * Lot of Gaia-X participants active in the event, such as 
       * walt.id, Spherity, deltaDAO, peaq, datarella, mobix, ....


**GXFS Monthly Webinar**

   * June 30, online
   * Register here: [https://register.gotowebinar.com/register/57773169960194138](https://register.gotowebinar.com/register/57773169960194138)


**GXFS CONNECT 2023**

   * 05-06 September 2023, Berlin


**Next GXFS Tech Workshops (on site)**
- 5 \& 6 September, Berlin
- 25 \& 26 October, near Stuttgart
- Late November/Early December, Cologne

**SODA DATA Vision 2023**
   * September 18, Bilbao
   * Topics:
       * Data Catalog for Files and Object
       * Cloud Native Data Management
       * Hybrid Multi-Cloud Data Lifecycle \& Protection
       * ...


**EclipseCon**
   * October 16 - 19, Ludwigsburg (Germany)
   * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
   * Monday 16th is the community day and there is the possibility to have a dedicated space for Gaia-X to have a workshop on Gaia-X OSS projects
   * CFP is closed.


### Sphereon Wallet and Future Mobility Marketplace SSI Integration

   * Update by Niels Klomp, CEO of Sphereon
   * [https://marketplace.future-mobility-alliance.org/](https://marketplace.future-mobility-alliance.org/) part of Pontus-X ecosystem for Gaia-X
   * SSI Wallets available here: [https://ssi.future-mobility-alliance.org/demo/issuer/#/download](https://ssi.future-mobility-alliance.org/demo/issuer/#/download)
   * Based on OIDC4VP, OIDV4VCI, SIOPv2


### Updates from other lighthouses / projects / developers?

   * **Lab: Demo of the Wizard in beginner-friendly mode**
       * Features: hosted did, vc. Generated IDs. Easy-to-fill form.
       * See: [https://wizard.lab.gaia-x.eu/development](https://wizard.lab.gaia-x.eu/development)




### AOB

   * Planned time-frame for migrating the GXFS components: 3-14th of July
   * Calendar of upcoming meetings
       * New invites will come!


#### **Task for the Community**

**Express your expectations to the OSS-Community meeting (via mailing list, Slack/Matrix, here in the pad)**

   * What information are you looking for?
   * What do you expect?
   * How can we make this more useful?
