# Meeting notes Gaia-X OSS community August 3, 2023


## Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news 
   5. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   6. AOB


##  Focus of the weekly

   * Updates from the community


## General Notes

   * **Participants in the call:** 36
   * **Acceptance of Agenda:** Accepted by the audience.
   * **Acceptance of last week's minutes:** Accepted by the audience.
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-07-27.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-07-27.md)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * No new participants in today's meeting.


**Useful resources for newcomers:**



   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Compliance Service Swagger UI [https://compliance.lab.gaia-x.eu/v1/docs/](https://compliance.lab.gaia-x.eu/v1/docs/)
   * Gaia-X Registry Swagger UI: [https://registry.lab.gaia-x.eu/v1/docs/#/](https://registry.lab.gaia-x.eu/v1/docs/#/)
   * Gaia-X Registry: [https://registry.lab.gaia-x.eu/](https://registry.lab.gaia-x.eu/)
   * Gaia-X Registration Number Service: [https://registrationnumber.notary.gaia-x.eu/v1/docs/](https://registrationnumber.notary.gaia-x.eu/v1/docs/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news



**GXFS CONNECT 2023 (English speaking event)**

   * September 5-6, 2023, Berlin
   * [https://www.gxfs.eu/gxfs-connect-2023/](https://www.gxfs.eu/gxfs-connect-2023/)
   * Max. PAX 250
   * Agenda in the making
   * English-speaking event


**Next GXFS Workshops (on site - language English)**

   * 5 \& 6 September, Berlin, parallel to GXFS Connect 2023, also in Villa Elisabeth
   * 25 \& 26 October, near Stuttgart
   * Late November/Early December, Cologne 


**SODA DATA Vision 2023** (in parallel with Open Source Summit Europe | Linux Foundation Events 19-21 September)

   * September 18, Bilbao
   * Call for proposals - until July 31st. Hybrid.
   * [https://www.sodafoundation.io/events/sodadatavision2023/](https://www.sodafoundation.io/events/sodadatavision2023/)
   * Topics (among others):
       * Data Catalog for Files and Object
       * Cloud Native Data Management
       * Hybrid Multi-Cloud Data Lifecycle \& Protection


**Future Congress Digital** 

   * October 11, Wolfsburg (Germany)
   * Gaia-X and Digital Mobility Ecosystems
   * [https://www.futurecongress.digital/anmeldung](https://www.futurecongress.digital/anmeldung)


**EclipseCon**

   * October 16 - 19, Ludwigsburg (Germany)
   * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
   * Monday 16th is the community day and there is the possibility to have a dedicated space for Gaia-X to have a workshop on Gaia-X OSS projects


**Gaia-X Summit 2023**

   * November 9-10, 2023
   * Alicante, Spain
   * [https://gaia-x.eu/summit-2023/](https://gaia-x.eu/summit-2023/)




### Updates from other lighthouses / projects / developers / lab?

   * Request: Create video for the Gaia-X Wizard
       * There will be a Tech Deep Dive on this in September 12 and we can do a preview of that in the OSS call as well
       * Additionally: Creation of a user guide for the wizard is in the backlog [https://gaia-x.atlassian.net/browse/LAB-351](https://gaia-x.atlassian.net/browse/LAB-351)


### AOB

   * Nothing.





