# Meeting notes Gaia-X OSS community August 10, 2023


## Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   6. AOB


##  Focus of the weekly

   * Updates from the community


## General Notes

   * **Participants in the call:** 32
   * **Acceptance of Agenda:** Accepted by the audience.
   * **Acceptance of last week's minutes:** Accepted by the audience.
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-08-03.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-08-03.md)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors



   * No new participants in the call.


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * Gaia-X Compliance Service Swagger UI [https://compliance.lab.gaia-x.eu/v1/docs/](https://compliance.lab.gaia-x.eu/v1/docs/)
   * Gaia-X Registry Swagger UI: [https://registry.lab.gaia-x.eu/v1/docs/#/](https://registry.lab.gaia-x.eu/v1/docs/#/)
   * Gaia-X Registry: [https://registry.lab.gaia-x.eu/](https://registry.lab.gaia-x.eu/)
   * Gaia-X Registration Number Service: [https://registrationnumber.notary.gaia-x.eu/v1/docs/](https://registrationnumber.notary.gaia-x.eu/v1/docs/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

**GXFS CONNECT 2023 (English speaking event)**

   * September 5-6, 2023, Berlin
   * [https://www.gxfs.eu/gxfs-connect-2023/](https://www.gxfs.eu/gxfs-connect-2023/)
   * Max. PAX 250
   * Agenda in the making
   * English-speaking event


**Next GXFS Workshops (on site - language English)**

   * 5 \& 6 September, Berlin, parallel to GXFS Connect 2023, also in Villa Elisabeth
   * 25. Oktober, Reutlingen 
   * 12-13 December, Köln 


**Gaia-X Roadshow** 

   * 10.10.2023 18:00 - 23:00 Uhr, Wolfsburg
   * Registration: [https://www.eco.de/event/gaia-x-roadshow-wolfsburg](https://www.eco.de/event/gaia-x-roadshow-wolfsburg) 


**Best of Blockchain**

   * October 5-7, Berlin
   * [https://bestofblockchain.io/](https://bestofblockchain.io/)
   * Panel on European data economy and Gaia-X spearheading Web3 and Web2 integration for businesses


**SODA DATA Vision 2023** (in parallel with **Open Source Summit Europe | Linux Foundation Events** 19-21 September)

   * September 18, Bilbao
   * Call for proposals - until July 31st. Hybrid.
   * [https://www.sodafoundation.io/events/sodadatavision2023/](https://www.sodafoundation.io/events/sodadatavision2023/)
   * Topics (among others):
       * Data Catalog for Files and Object
       * Cloud Native Data Management
       * Hybrid Multi-Cloud Data Lifecycle \& Protection


**EuProGigant Open House Day** 

   * EuProGigant ecosystem of five projects, meeting in Berlin
   * October 10-11, 2023
   * Registration open, 250 PAX, physical participation only
   * [https://euprogigant-openhouseday2023.b2match.io/](https://euprogigant-openhouseday2023.b2match.io/)


**Future Congress Digital** 

   * October 11, Wolfsburg (Germany)
   * Gaia-X and Digital Mobility Ecosystems
   * [https://www.futurecongress.digital/anmeldung](https://www.futurecongress.digital/anmeldung)


**EclipseCon**

   * October 16 - 19, Ludwigsburg (Germany)
   * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
   * Monday 16th is the community day and there is the possibility to have a dedicated space for Gaia-X to have a workshop on Gaia-X OSS projects


**Gaia-X Summit 2023**

   * November 9-10, 2023
   * Alicante, Spain
   * [https://gaia-x.eu/summit-2023/](https://gaia-x.eu/summit-2023/)


### Updates from other lighthouses / projects / developers / lab?

   * Pontus-X Documentation being updated: [https://docs.pontus-x.eu/](https://docs.pontus-x.eu/)
   * Pontus-X Ecosystem transition to v4 and next feature alignment in progress to be completed over the next two weeks [https://portal.pontus-x.eu](https://portal.pontus-x.eu)
   * Currently mapping the available and live Gaia-X ecosystem across software stacks to prepare interoperability builds (XFSC, Pontus-X/Ocean Protocol, EDC, else)
   * Gaia-X Digital Clearing Houses (GXDCH)
       * GXDCH Operational Rules and Requirements v0.12 are available as draft and have been circulated within the prototype group
       * Eligibility Criteria v0.7 currently in discussion for approval by BoD
       * Contact points would be the Gaia-X AISBL COO Team, Roland Fadrany and Frederik Tengg
       * Documents are not being published right now, but shall be approved before the Gaia-X Summit



### AOB

Nothing.

   * [...]





