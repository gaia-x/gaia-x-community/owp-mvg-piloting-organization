# Meeting notes Gaia-X OSS community September 07, 2023

## Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda 
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. GXFS-FR presentation on Issuer Credential Protocol  (Yves-Marie Pondaven)
   6. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   7. AOB

##  Focus of the weekly

   * GXFS-FR presentation on Issuer Credential Protocol


## General Notes

   * **Participants in the call:** 39
   * **Acceptance of Agenda:** yes
   * **Acceptance of last week's minutes:** yes
   * **Link to last week's minutes:**[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-08-31.md](**https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-08-31.md**)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * No newcomers in the call.

**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

**GXFS CONNECT 2023 (English speaking event)**

   * September 5-6, 2023, Berlin
   * [https://www.gxfs.eu/gxfs-connect-2023/](https://www.gxfs.eu/gxfs-connect-2023/)
   * Max. PAX 250
   * Agenda in the making
   * English-speaking event


**Next GXFS Workshops (on site - language English)**

   * 5 \& 6 September, Berlin, parallel to GXFS Connect 2023, also in Villa Elisabeth
   * 25. Oktober, Reutlingen 
   * 12-13 December, Köln 


**Tech deep dive**

   * September 12th, 12h-13h CEST, a Tuesday
   * Online
   * Topic: Quick tour of the implementation of the TF 22.10
   * Speaker: Ewann (Gaia-X AISBL CTO Team)

**IAA Mobility**

   * September 6-8, Munich (Germany)
   * Gaia-X 4 Future Mobility Demonstrator incl. SSI and network usage available with vehicles and smart infrastructure
   * Registration for test drives [https://mobix.ai/iaa-moveid/](https://mobix.ai/iaa-moveid/)


**Gaia-X Roadshow** 

   * October 10, 6 - 11 pm, Wolfsburg (Germany)
   * Registration: [https://www.eco.de/event/gaia-x-roadshow-wolfsburg](https://www.eco.de/event/gaia-x-roadshow-wolfsburg) 


**SODA DATA Vision 2023** (in parallel with **Open Source Summit Europe** | Linux Foundation Events 19-21 September)

   * September 18, Bilbao
   * Call for proposals - until July 31st. Hybrid.
   * [https://www.sodafoundation.io/events/sodadatavision2023/](https://www.sodafoundation.io/events/sodadatavision2023/)
   * Topics (among others):
       * Data Catalog for Files and Object
       * Cloud Native Data Management
       * Hybrid Multi-Cloud Data Lifecycle \& Protection


**EuProGigant Open House Day** 

   * EuProGigant ecosystem of five projects, meeting in Berlin
   * October 10-11, 2023
   * Registration open, 250 PAX, physical participation only
   * [https://euprogigant-openhouseday2023.b2match.io/](https://euprogigant-openhouseday2023.b2match.io/)


**Future Congress Digital** 

   * October 11, Wolfsburg (Germany)
   * Gaia-X and Digital Mobility Ecosystems
   * [https://www.futurecongress.digital/anmeldung](https://www.futurecongress.digital/anmeldung)


**EclipseCon**

   * October 16 - 19, Ludwigsburg (Germany)
   * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
   * Workshop "Federation Services @ Eclipse" during the community day on Monday, October 16th 
       * [https://www.eclipsecon.org/2023/community-day](https://www.eclipsecon.org/2023/community-day)
   * Dedicated workshop "Federation Services" at the first day of the week. 


**Gaia-X Summit 2023**

   * November 9-10, 2023
   * Alicante, Spain
   * [https://gaia-x.eu/summit-2023/](https://gaia-x.eu/summit-2023/)

### GXFS-FR presentation on Issuer Credential Protocol

   * Presented by Yves-Marie Pondaven and the GXFS-FR team
       * Source code: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer)
       * The tool can create Verifiable Credentials, either through a form or already existing authentication solutions
       * It handles the communication with the Gaia-X Compliance Service to obtain compliant VCs and gives the possibility to also revoke VCs (manually by an admin)
   * Discussion on recording the demos: 
       * we want to have a relaxed space, where anyone can ask questions, so we don't record the demos; also the presentations are sometimes just proof of concept and not always polished
       * the suggestion is that for those who want to have recordings of their demo, they can do it separately and we can promote it as an outcome of the OSS group




### Updates from other lighthouses / projects / developers / lab?

   * No updates this week
 

### AOB

   * Community input/support for submitting a proposal for Next Generation Internet [https://trustchain.ngi.eu/open-call-2/](https://trustchain.ngi.eu/open-call-2/) (deadline 'sportlich' **20 September**). Idea: further develop the Unigrid Decentralized Internet ([https://unigrid.org)](https://unigrid.org)) while ensuring maximum interoperability / standardization, e.g. in line with Gaia-X, maybe EBSI, etc. (René Krikke, engage@mrprobot.com - not sure if I can make the meeting).
   * Since René was unable to join the call, we will move the discussion on the mailing list

























