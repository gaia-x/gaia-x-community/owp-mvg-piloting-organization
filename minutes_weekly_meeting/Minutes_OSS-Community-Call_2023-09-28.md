## Meeting notes Gaia-X OSS community September 28, 2023

### Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   6. AOB



###  Focus of the weekly

   * Updates from the community


### General Notes

   * **Participants in the call:** 45
   * **Acceptance of Agenda:** Yes
   * **Acceptance of last week's minutes:** Yes
   * **Link to last week's minutes:**[https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-09-21.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-09-21.md)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * No new participants in the call.


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu](https://list.gaia-x.eu/postorius/lists/oss-community.list.gaia-x.eu)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

**ALASCA Tech-Talk No. 9**

   * Fri, Sep 29, 11am - 12pm (new date and time!)
   * Topic: "Our way to make 23KE an open source project" (Christian Berendt, 23Technologies)
   * More on 23KE: [https://23technologies.cloud/en#23KE](https://23technologies.cloud/en#23KE)
   * More info on ALASCA Tech-Talks: [https://alasca.cloud/alasca-tech-talks/](https://alasca.cloud/alasca-tech-talks/)

**Gaia-X Roadshow** 

   * October 10, 6 - 11pm, Wolfsburg (Germany)
   * Registration: [https://www.eco.de/event/gaia-x-roadshow-wolfsburg](https://www.eco.de/event/gaia-x-roadshow-wolfsburg) 


**EuProGigant Open House Day**

   * October  10 - 11, Berlin (Germany)
   * EuProGigant ecosystem of five projects
   * Registration open, 250 PAX, physical participation only
   * [https://euprogigant-openhouseday2023.b2match.io/](https://euprogigant-openhouseday2023.b2match.io/)


**Future Congress Digital** 

   * October 11, Wolfsburg (Germany)
   * Gaia-X and Digital Mobility Ecosystems
   * [https://www.futurecongress.digital/anmeldung](https://www.futurecongress.digital/anmeldung)


**EclipseCon**

   * October 16 - 19, Ludwigsburg (Germany)
   * [https://www.eclipsecon.org/2023](https://www.eclipsecon.org/2023)
   * Workshop "Federation Services @ Eclipse" during the community day on Monday, October 16th 
       * [https://www.eclipsecon.org/2023/community-day](https://www.eclipsecon.org/2023/community-day)
   * Dedicated workshop "Federation Services" at the first day of the week. 


**GXFS Tech Workshop**

   * October 17, Reutlingen (Germany)
   * Registration and Agenda [https://www.eco.de/event/gxfs-tech-workshop-4/](https://www.eco.de/event/gxfs-tech-workshop-4/) 
   * Location: **INNOPORT Reutlingen /** Max-Planck-Straße 68/1/ 72766 Reutlingen
   * Topics:
       * **„Was tragen Initiativen wie Gaia-X und Manufacturing-X zur Digitalisierung der Industrie bei?** - In German
       * Hands on with the WFE - In English 


**GXFS Tech Workshop 5**

* Decemer 12-13, Cologne (Germany)
* Location: 25hours Hotel Koeln The Circle
* Registration [https://www.gxfs.eu/gxfs-tech-workshop-5/](https://www.gxfs.eu/gxfs-tech-workshop-5/) 
* Agenda coming 

**SCS Hackathon**

   * Nov 8 2023, Dresden (with Cloud\&Heat / ALASCA)
   * More info soon!


**Gaia-X Summit 2023**

   * November 9-10, Alicante (Spain)
   * [https://gaia-x.eu/summit-2023/](https://gaia-x.eu/summit-2023/)


### Updates from other lighthouses / projects / developers / lab?

   * Lab: Next release of the CES is on the rails:
       * [https://gitlab.com/gaia-x/lab/credentials-events-service/-/merge\_requests/5](https://gitlab.com/gaia-x/lab/credentials-events-service/-/merge\_requests/5)
       * No breaking changes, a new feature and an eventual fix on concurrent events ⁠
   * Lab: Working on final TAGUS (TF2210) impl: Resource/SoftwareResource etc. 
 

### AOB

Nothing.















