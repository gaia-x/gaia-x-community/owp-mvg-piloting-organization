## Meeting notes Gaia-X OSS community October 19, 2023

### Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   6. AOB


###  Focus of the weekly

   * Updates from the community


### General Notes

   * **Participants in the call:** 40
   * **Acceptance of last week's minutes:** Yes
   * **Acceptance of the agenda:** Yes
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-10-12.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-10-12.md)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * No new participants in the call.


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news


**Maybe: Open Source Meetup organized by the Eclipse Foundation**

   * November 8 - 16:00-19:00, Alicante (Spain)
   * Anybody interested? Ping gael.blondelle@eclipse-foundation.org before Friday Oct 20th.


**Gaia-X Summit 2023**

   * November 9-10, Alicante (Spain)
   * [https://gaia-x.eu/summit-2023/](https://gaia-x.eu/summit-2023/)


**Gaia-X Workshop Luxembourg (only for Gaia-X Hubs, Ecosystems, Lighthouse projects)**

   * December 5-6 (maybe also 4) - Note, this is not an open event
   * Location: Luxembourg
   * Register: [https://events.gaia-x.lu/data-summit-luxembourg-schengen-x](https://events.gaia-x.lu/data-summit-luxembourg-schengen-x)
   * Info: [https://www.linkedin.com/posts/luxembourg-national-data-service-lnds\_datasummitluxembourg-schengenx-lnds-activity-7113153624398028800-b\_Lh](https://www.linkedin.com/posts/luxembourg-national-data-service-lnds\_datasummitluxembourg-schengenx-lnds-activity-7113153624398028800-b\_Lh)


**GXFS Tech Workshop**

   * December 12-13, Cologne (Germany)
   * Location: 25hours Hotel Koeln The Circle
   * Registration [https://www.gxfs.eu/gxfs-tech-workshop-5/](https://www.gxfs.eu/gxfs-tech-workshop-5/) 
   * Agenda coming 
   * On site event - no remote participation 

### Updates from other lighthouses / projects / developers / lab?

Gaia-X Digital Clearing Houses

   * To raise issues on the Clearing house: [https://gitlab.com/gaia-x/lab/gxdch/-/issues](https://gitlab.com/gaia-x/lab/gxdch/-/issues)
   * To try out deploying a Clearing house node: [https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/README.md](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/README.md)
   * T-Systems Gaia-X Digital Clearing House (GXDCH) now available
       * [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
       * Compliance Service: [https://gx-compliance.gxdch.dih.telekom.com/v1/docs](https://gx-compliance.gxdch.dih.telekom.com/v1/docs)
       * Registry: [https://gx-registry.gxdch.dih.telekom.com/v1/docs](https://gx-registry.gxdch.dih.telekom.com/v1/docs)
       * Notary: [https://gx-notary.gxdch.dih.telekom.com/v1/docs](https://gx-notary.gxdch.dih.telekom.com/v1/docs)


   * Catena-X next phase launched
       * [https://portal.int.cofinity-x.com/](https://portal.int.cofinity-x.com/)
       * Please use the term "compliance" responsibly. Verify yourself.


   * Pontus-X new features 
       * Auto-Signer for UX and usage with SSI wallets
       * XFSC PCM/OCM integration proceeding nicely.


The Tagus release is completed with these features:

   * [https://wizard.lab.gaia-x.eu/development](https://wizard.lab.gaia-x.eu/development) to test EID signature of payloads
   * [https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/trustframework](https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/trustframework) to take a look at the shape used for all trustframework (including service offering and resources)
   * Example of a service offering with resources: [https://compliance.lab.gaia-x.eu/development/docs/#/credential-offer/CommonController\_issueVC](https://compliance.lab.gaia-x.eu/development/docs/#/credential-offer/CommonController\_issueVC) => Example "service offering with resources"


   * These features are available on the development endpoints for now and it's planned to be merged on main on Monday, 23rd of October
 

### AOB

   * Nothing.







