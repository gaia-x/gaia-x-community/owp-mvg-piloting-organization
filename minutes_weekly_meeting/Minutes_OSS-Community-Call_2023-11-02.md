## Meeting notes Gaia-X OSS community November 02, 2023

### Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Update on Gaia-X compliant ecosystems
   6. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   7. AOB


###  Focus of the weekly

   * Updates from the community


### General Notes

   * **Participants in the call:** 45
   * **Acceptance of last week's minutes:** Yes
   * **Acceptance of the agenda:** Yes
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-10-26.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-10-26.md)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)

### Introduction of new participants and regular contributors

   * Alexander Diab <diab@osb-alliance.com>, Sovereign Cloud Stack, Ecosystem Manager

**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)

### Interesting sessions, events and news

   * News: Gaia-X \& Open Source [https://medium.com/@jankammerath/open-source-is-struggling-and-its-not-big-tech-that-is-to-blame-cfba964219f8](https://medium.com/@jankammerath/open-source-is-struggling-and-its-not-big-tech-that-is-to-blame-cfba964219f8)

**Confirmed! Pre-GaiaX Summit Open Source Meetup! organized by the Eclipse Foundation**

   * When: November 8 - 16:00-18:30 - followed by a cocktail
   * Where: Eurostars Centrum Alicante - Alicante (Spain)
   * Register at [https://pregaiaxsummitmeetup.eventbrite.com/](https://pregaiaxsummitmeetup.eventbrite.com/)
   * Still working on the program - available slots for short presentations (10mn) about OSS projects related to Gaia-X whether hosted at Eclipse or not. Send gael.blondelle@eclipse-foundation.org a presentation title and speaker name.


**Gaia-X Summit 2023**

   * November 9-10, Alicante (Spain)
   * [https://gaia-x.eu/summit-2023/](https://gaia-x.eu/summit-2023/)


   * Message by Lauresha
      * Dear community,
      * We look forward to seeing many of you at the upcoming Gaia-X Summit in Alicante. Knowing that a significant number of us will be in attendance, we're excited to organize a social gathering after the first day of the Summit on November 9, 2023. The goal of this community meet-up is to bring together members of our community for an evening of networking, socializing, and fun in a relaxed and friendly atmosphere.
      * We are coordinating this gathering, and if you're interested in joining, please complete the provided sheet to confirm your participation here: [https://docs.google.com/spreadsheets/d/1ma08YscWVgi\_PABZDyiBGjFf3nGV8DOUR-OESeJFiRs/edit#gid=0](https://docs.google.com/spreadsheets/d/1ma08YscWVgi\_PABZDyiBGjFf3nGV8DOUR-OESeJFiRs/edit#gid=0)  .
      * We'll be sharing the location and timing details soon. Once we have a finalized list of participants, I will send out an email with all the necessary information.
      * Let's come together, have a free drink, and engage in discussions about current activities and more. :)


**Gaia-X Workshop Luxembourg (only for Gaia-X Hubs, Ecosystems, Lighthouse projects)**

   * December 5-6 (maybe also 4) - Note, this is not an open event
   * Location: Luxembourg
   * Register: [https://events.gaia-x.lu/data-summit-luxembourg-schengen-x](https://events.gaia-x.lu/data-summit-luxembourg-schengen-x)
   * Info: [https://www.linkedin.com/posts/luxembourg-national-data-service-lnds\_datasummitluxembourg-schengenx-lnds-activity-7113153624398028800-b\_Lh](https://www.linkedin.com/posts/luxembourg-national-data-service-lnds\_datasummitluxembourg-schengenx-lnds-activity-7113153624398028800-b\_Lh)


**GXFS Tech Workshop**

   * December 12-13, Cologne (Germany)
   * Location: 25hours Hotel Koeln The Circle
   * Registration [https://www.gxfs.eu/gxfs-tech-workshop-5/](https://www.gxfs.eu/gxfs-tech-workshop-5/) 
   * Agenda coming 
   * On site event - no remote participation 
       * Exercise 1 (draft)
         * Here we will guide our attendees to do the following on their systems:
           * Deploy all the applications of Smart-X POC 
           * Configure the relevant XFSC components like OCM and PCM
           * Create Legal participant
           * Create Resource
           * Create Service Offering
           * Check the service offering published to CES
       * Exercise 2 (draft)
         * Here we will guide our attendees to do the following on their systems:
           * Deploy the catalogue frontend and backend
           * Setup the database
           * Configure the CES API
           * Fetch the service offerings from CES and see in their UI


### Update on "Gaia-X compliant data spaces" and "Gaia-X Compliance"

   * There is no such thing as "Gaia-X Compliant Data space" for the simple reasons that:
      * there is no unique definition of a data space
      * there is no rule for a compliant data space
      * data space are optional (PierreGronlier 02/11/2023)
   * Be aware of the latest improvements regarding Gaia-X credential shapes:
      * Those rules apply to **all** Gaia-X Self-Descriptions and there is a Self-Description for **all** the entities defined as part of the Gaia-X Conceptual model described in the Gaia-X Architecture document. This list mainly comprises:
          * Participant including Consumer, Federator, Provider
          * Service Offering
       * Resource:
          * [https://compliance.lab.gaia-x.eu/#menu](https://compliance.lab.gaia-x.eu/#menu)
          * [https://registry.lab.gaia-x.eu/development/docs#/Trusted-Shape-registry/TrustedShapeRegistry\_getImplementedShapes](https://registry.lab.gaia-x.eu/development/docs#/Trusted-Shape-registry/TrustedShapeRegistry\_getImplementedShapes)
   * Recent claims by Gaia-X related projects and lighthouses needed a clarification on "Gaia-X compliant data spaces". This has now been answered by the Gaia-X executive team.
       * The Gaia-X framework is designed to present and validate credentials between business partners as part of their transactions. 
       * Any service with claims validated and signed by an accredited GXDCH is compliant.
       * Gaia-X is validating and accrediting providers to operate GXDCH Services. Gaia-X maintains an official list of GXDCH Providers and Services.
       * Gaia-X association doesn’t have an exhaustive list of all Gaia-X Compliant offerings.
       * Conclusion: If your data space provides services that are validated as Gaia-X compliant you can start building the claim of a Gaia-X compliant data space <- that's inacurate (Pierre)
       * Gaia-X believes that the community will self-regulate itself with the incentive for the pretenders to be as transparent as possible otherwise risking to damage their reputation
           * see [http://docs.gaia-x.eu/framework/?tab=clearing-house](http://docs.gaia-x.eu/framework/?tab=clearing-house)
           * see [https://gaia-x.eu/gxdch/](https://gaia-x.eu/gxdch/)
       * Current three data spaces and ecosystems this claim to be the first Gaia-X compliant data spaces
           * Pontus-X
           * Catena-X
           * Boot-X
           * If you would like to support this claim please present your credentials for verification against the GXDCH.


### Updates from other lighthouses / projects / developers / lab?

   * XFSC Integration into Service-Meister Portal
       * Personal Credential Manager useable for login with XFSC company credential
       * Quick demo of the current functionality to be released and available for ecosystem portals
 

### AOB

   * Aruba has shared with Gaia-X management team its position paper regarding the choice between eIDAS qualified certificates and SSL certificates (both technically based on X.509 standard, but with different scope and governance) for authentication and electronic signature for Gaia-X and in particular to identify the Participants to the Gaia-X Framework and allow them to safely share data and make transactions.
   * The position paper should be available to Gaia-X website and Membership Platform
   * [https://www.aruba.it/documents/aruba-position-paper-on-eidas-and-ssl-certificates-for-gaia-x.pdf](https://www.aruba.it/documents/aruba-position-paper-on-eidas-and-ssl-certificates-for-gaia-x.pdf)



