## Meeting notes Gaia-X OSS community November 16, 2023

### Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   6. AOB


### Focus of the weekly

   * Updates from the Summit, Catalogues and Gaia-X Document Releases


### General Notes

   * **Participants in the call:** 47
   * **Acceptance of last week's minutes:** Yes
   * **Acceptance of the agenda:** Yes
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-11-02.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-11-02.md)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * Vincent Kelleher, Gaia-X AISBL CTO Team \& Lab

**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

**Gaia-X Workshop Luxembourg (only for Gaia-X Hubs, Ecosystems, Lighthouse projects)**

   * December 5-6 (maybe also 4) - Note, this is not an open event
   * Location: Luxembourg
   * Register: [https://events.gaia-x.lu/data-summit-luxembourg-schengen-x](https://events.gaia-x.lu/data-summit-luxembourg-schengen-x)
   * Info: [https://www.linkedin.com/posts/luxembourg-national-data-service-lnds\_datasummitluxembourg-schengenx-lnds-activity-7113153624398028800-b\_Lh](https://www.linkedin.com/posts/luxembourg-national-data-service-lnds\_datasummitluxembourg-schengenx-lnds-activity-7113153624398028800-b\_Lh)

**GXFS Tech Workshop**

   * December 12-13, Cologne (Germany)
   * Location: 25hours Hotel Koeln The Circle
   * Registration [https://www.gxfs.eu/gxfs-tech-workshop-5/](https://www.gxfs.eu/gxfs-tech-workshop-5/) 
   * Agenda coming 
   * On site event - no remote participation 
       * Exercise 1 (draft)
       * Here we will guide our attendees to do the following on their systems:
           * Deploy all the applications of Smart-X POC 
           * Configure the relevant XFSC components like OCM and PCM
           * Create Legal participant
           * Create Resource
           * Create Service Offering
           * Check the service offering published to CES
       * Exercise 2 (draft)
       * Here we will guide our attendees to do the following on their systems:
           * Deploy the catalogue frontend and backend
           * Setup the database
           * Configure the CES API
           * Fetch the service offerings from CES and see in their UI

### Updates from other lighthouses / projects / developers / lab?

   * Gaia-X Documents Release
       * Data Exchange Document 23.11
       * [https://docs.gaia-x.eu/technical-committee/data-exchange/latest/](https://docs.gaia-x.eu/technical-committee/data-exchange/latest/)
       * Policy and Conformity Rules Document 23.10 
       * [https://docs.gaia-x.eu/policy-rules-committee/policy-rules-conformity-document/latest/](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-conformity-document/latest/)
       * Architecture Document 23.10
       * [https://docs.gaia-x.eu/technical-committee/architecture-document/latest/](https://docs.gaia-x.eu/technical-committee/architecture-document/latest/)
   * Gaia-X Academy of Gaia-X Hub France opening up
       * see also Links to GXFS-FR catalogue for Aster-X and the GXFS-FR identity stack
       * Link: [https://elearning.gxfs.fr/](https://elearning.gxfs.fr/)
       * Catalogue Link: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/web-catalog-v2](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/web-catalog-v2)
       * SSI Stack "Issuance Credential Protocol ( ICP)" Link: [https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/credentialissuer/-/tree/documentation?ref\_type=heads](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/credential-issuer/credentialissuer/-/tree/documentation?ref\_type=heads)
   * New catalogue and signer tools released for the OSS community by Gaia-X AISBL and Smart Sense
       * Link: [https://gitlab.com/gaia-x/gaia-x-community/gaia-x-catalogue](https://gitlab.com/gaia-x/gaia-x-community/gaia-x-catalogue)
       * [https://catalogue.lab.gaia-x.eu/development](https://catalogue.lab.gaia-x.eu/development)
       * Developed to build the catalogue of catalogues, based on the OSS community catalogues
       * When it comes to the requirements for the depth of search, you can formally bring forward requests, ideas and most important contributions through the working groups and Gaia-X team or the repo: [https://gitlab.com/gaia-x/gaia-x-community/gaia-x-catalogue](https://gitlab.com/gaia-x/gaia-x-community/gaia-x-catalogue)
       * What are the most relevant questions and queries for the community? Let us know.
   * Gaia-X catalogues introduced at the "Catalogues up and running" session
       * Gaia-X Credential Event Service (CES)
           * mandatory with next major release of GXDCH
           * [https://ces-v1.lab.gaia-x.eu/q/swagger-ui/#/Credential%20Event%20Resource/get\_credentials\_events](https://ces-v1.lab.gaia-x.eu/q/swagger-ui/#/Credential%20Event%20Resource/get\_credentials\_events)
       * CISPE
       * Link: [https://catalogue.cispe-cde.cloud/](https://catalogue.cispe-cde.cloud/)
       * Pontus-X
       * Link: [https://www.pontus-x.eu/](https://www.pontus-x.eu/)
       * Prometheus-X
       * Link: [https://catalog.visionstrust.com/](https://catalog.visionstrust.com/)
       * Aster-X
       * Link: [https://webcatalog.aster-x.demo23.gxfs.fr/catalog](https://webcatalog.aster-x.demo23.gxfs.fr/catalog)
       * EuProGigant
       * Link: [https://portal.euprogigant.io/](https://portal.euprogigant.io/)
       * agDataHub
       * Link: [https://platform.api-agro.eu/members/login](https://platform.api-agro.eu/members/login)
       * Gaia-X 4 Future Mobility
       * Link: [https://portal.pontus-x.eu/](https://portal.pontus-x.eu/)
       * COOPERANTS
       * Link: [https://cooperants.pontus-x.eu/](https://cooperants.pontus-x.eu/) \& [https://airbus.pontus-x.eu/](https://airbus.pontus-x.eu/)
       * Service-Meister
       * Link: [https://servicemeister.pontus-x.eu/](https://servicemeister.pontus-x.eu/)
       * University de Lleida
       * Link: [https://dataspace.angliru.udl.cat/](https://dataspace.angliru.udl.cat/)
       * Berlin State Library
       * Link: [https://sbb.pontus-x.eu/](https://sbb.pontus-x.eu/)
       * PLC-AAD Instance of the XFSC federated catalogue: 
           * Link: [https://fc-server.gxfs.gx4fm.org/](https://fc-server.gxfs.gx4fm.org/) and a simple frontend for this instance: [https://metadatasearch.gxfs.gx4fm.org/](https://metadatasearch.gxfs.gx4fm.org/) 
           * Access to  [https://fc-server.gxfs.gx4fm.org/](https://fc-server.gxfs.gx4fm.org/) is protected. Please contact robert.schubert@msg.group for getting credentials. Postman API also available.
 

### AOB

   * Nothing.



