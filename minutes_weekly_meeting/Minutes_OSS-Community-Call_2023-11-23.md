## Meeting notes Gaia-X OSS community November 23, 2023

### Agenda

   1. Competition \& Antitrust guidelines
   2. Acceptance of last week meeting notes and today's agenda
   3. Introduction of new participants and regular contributors
   4. Interesting sessions, events and news
   5. Updates from other lighthouses / projects / Gaia-X Lab/ developers
   6. AOB


### Focus of the weekly

   * Tagus Release - Demo of Label Level 1 based on PRLD 22.11
   * Loire release branch


### General Notes

   * **Participants in the call:** 49
   * **Acceptance of last week's minutes:** Yes
   * **Acceptance of the agenda:** Yes
   * **Link to last week's minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-11-16.md](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes\_weekly\_meeting/Minutes\_OSS-Community-Call\_2023-11-16.md)
   * **Link to all minutes:** [https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/tree/main/minutes\_weekly\_meeting)


### Introduction of new participants and regular contributors

   * Felix Kappert, Gaia-X Hub Germany 


**Useful resources for newcomers:**

   * Introduction to our Community: [https://gitlab.com/gaia-x/gaia-x-community/open-source-community](https://gitlab.com/gaia-x/gaia-x-community/open-source-community)
   * Link to our mailing list: [https://list.gaia-x.eu/postorius/lists/](https://list.gaia-x.eu/postorius/lists/)
   * The Gaia-X Framework: [https://docs.gaia-x.eu/framework/](https://docs.gaia-x.eu/framework/)
   * Gaia-X Publications : [https://gaia-x.eu/mediatech/publications/](https://gaia-x.eu/mediatech/publications/)
   * Subscribe to Gaia-X Tech Newsletter: [https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/](https://list.gaia-x.eu/postorius/lists/tech.list.gaia-x.eu/)
   * Gaia-X Wizard [https://wizard.lab.gaia-x.eu/](https://wizard.lab.gaia-x.eu/)
   * OSS Group Calendar: [https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar](https://gitlab.com/groups/gaia-x/gaia-x-community/-/wikis/Calendar)
   * Gaia-X Members Platform: [https://membersplatform.gaia-x.eu/](https://membersplatform.gaia-x.eu/)


### Interesting sessions, events and news

**Latest Gaia-X Publications**

   * [https://gaia-x.eu/media/publications/](https://gaia-x.eu/media/publications/)


**News in Contracts and Ontologies**

   * [https://gaia-x.eu/news-press/latest-news/](https://gaia-x.eu/news-press/latest-news/)


**Gaia-X Summit videos are on youtube**

   * [https://www.youtube.com/@gaia-x5188/videos](https://www.youtube.com/@gaia-x5188/videos)


**Data Space Protocol Update**

   * IDSA Tech Talk | Unveiling the Dataspace Protocol 
   * [https://register.gotowebinar.com/register/4601736972768854103](https://register.gotowebinar.com/register/4601736972768854103)


**Gaia-X Workshop Luxembourg (only for Gaia-X Hubs, Ecosystems, Lighthouse projects)**

   * December 5-6 (maybe also 4) - Note, this is not an open event
   * Location: Luxembourg
   * Register: [https://events.gaia-x.lu/data-summit-luxembourg-schengen-x](https://events.gaia-x.lu/data-summit-luxembourg-schengen-x)
   * Info: [https://www.linkedin.com/posts/luxembourg-national-data-service-lnds\_datasummitluxembourg-schengenx-lnds-activity-7113153624398028800-b\_Lh](https://www.linkedin.com/posts/luxembourg-national-data-service-lnds\_datasummitluxembourg-schengenx-lnds-activity-7113153624398028800-b\_Lh)


**GXFS Tech Workshop**

   * December 12-13, Cologne (Germany)
   * Location: 25hours Hotel Koeln The Circle
   * Registration [https://www.gxfs.eu/gxfs-tech-workshop-5/](https://www.gxfs.eu/gxfs-tech-workshop-5/) 
   * Download Agenda here : [https://www.gxfs.eu/gxfs-tech-workshop-5/](https://www.gxfs.eu/gxfs-tech-workshop-5/)
   * On site event - no remote participation 
       * Exercise 1 (draft)
       * Here we will guide our attendees to do the following on their systems:
           * Deploy all the applications of Smart-X POC 
           * Configure the relevant XFSC components like OCM and PCM
           * Create Legal participant
           * Create Resource
           * Create Service Offering
           * Check the service offering published to CES
       * Exercise 2 (draft)
       * Here we will guide our attendees to do the following on their systems:
           * Deploy the catalogue frontend and backend
           * Setup the database
           * Configure the CES API
           * Fetch the service offerings from CES and see in their UI


**GXFS Tech Workshop #6**

   * Jan 22-23, Frankfurt (Germany)
   * Agenda coming soon. Focus on TRAIN and DNS Sec
   * Registration: [https://www.gxfs.eu/gxfs-tech-workshop-6/](https://www.gxfs.eu/gxfs-tech-workshop-6/) 


### Updates from other lighthouses / projects / developers / lab

   * **Tagus Release - Demo of Label Level 1 based on PRLD 22.11**
   * **Loire release branch off, start of development of the next release**
       * In order to allow us breaking things without affecting everyone, we've put Tagus release on a separate v1 branch and started using main/development and a v2 release. We'll provide a v1-staging for "non-production" tests
   * Proposal to introduce a trust Compliance Credential Verifier to complete the implementation (as endpoint with the GXDCH)


   * Discussion around key and certificate rotation. How to deal with this as a DID controller within DID:WEB (and potentially other DID methods) needs to be worked out. The working group is tending to this. Also the transition period is very important to look at as part of lifecycle management in the trust environment and identity ecosystem. A specification and proposal for that is needed.
   * Has been touched on during the last hackathon.


   * **Blockchain-based CES Solution Proposal**
       * [https://docs.google.com/document/d/1qKt5TWqb3gZ8gvRipZ6GVRSqTHBCTjjHrRbUrrxzjfQ/edit?usp=sharing](https://docs.google.com/document/d/1qKt5TWqb3gZ8gvRipZ6GVRSqTHBCTjjHrRbUrrxzjfQ/edit?usp=sharing)


   * **Pontus-X Subcription Verifier**
       * [https://saas.demo.delta-dao.com/](https://saas.demo.delta-dao.com/)
       * Moving beyond data exchange, and orchestration of data+software+infrastructre
       * Supports *aaS access, like an SSI verifier, based on the contracting service
       * Ongoing check against the "Gaia-X and Contracts" Blog, see [https://gaia-x.eu/news/latest-news/gaia-x-and-contracts/](https://gaia-x.eu/news/latest-news/gaia-x-and-contracts/) 
           * The consumer calls the of the selected provider using its verifiable credentials and its usage intentions
           * The Contracting service will call the Policy Reasoning Engine in order to validate that the usage intentions are aligned with the declared policies and, if yes, will issue a Contract Verifiable Credential towards the consumer (currently not supported, because a log is created that is validated by federators, can not be tampered with and acts as the basis) - tbd
           * The consumer can now directly call the provider for the requested service
       * Example 1 "Gaia-X 4 Future Mobility moveID": 
           * CES v1: bb030b0e-c2db-43c4-b3eb-75bb015771b2
           * CES v1: [https://ces-v1.lab.gaia-x.eu/credentials-events/bb030b0e-c2db-43c4-b3eb-75bb015771b2](https://ces-v1.lab.gaia-x.eu/credentials-events/bb030b0e-c2db-43c4-b3eb-75bb015771b2)
           * moveID Portal: [https://portal.moveid.eu/asset/did:op:f480f165280428ae0a2e2762fc172ab0200539cae463dc0c06dba70417e48814](https://portal.moveid.eu/asset/did:op:f480f165280428ae0a2e2762fc172ab0200539cae463dc0c06dba70417e48814)
       * Example 2 "COOPERANTS":
           * CES v1: b3e0a068-4bf8-4796-932e-2fa83043e203
           * CES v1: [https://storage.gaia-x.eu/credential-offers/b3e0a068-4bf8-4796-932e-2fa83043e203](https://storage.gaia-x.eu/credential-offers/b3e0a068-4bf8-4796-932e-2fa83043e203)
           * COOPERANTS Portal: [https://cooperants.pontus-x.eu/asset/did:op:6fe6ae5f546adf88606a0da158389f44a0801c3fe87ddb6e1f63e9120e07989c](https://cooperants.pontus-x.eu/asset/did:op:6fe6ae5f546adf88606a0da158389f44a0801c3fe87ddb6e1f63e9120e07989c)
   * **Extention of the gx or trust-framework vocabulary**
       * A DataResource contains a field "exposed through" that we can perfectly use for storing the EDC url where the data asses can be found.
       * For initiating a transfer between two EDCs we would need a contract ID from the source EDC. In the current vocabulary I cannot find a suitable field for storing this information.
       * Is it conceivable to add an optional field "contract information" (or something similar) into at least DataResource in the next release? (maybe it makes sense to add it into one of the super classes)
 

### AOB

   * FYI The GNU Name System (Nov 2023) [https://www.rfc-editor.org/rfc/rfc9498.html](https://www.rfc-editor.org/rfc/rfc9498.html) - Is this relevant just now?



